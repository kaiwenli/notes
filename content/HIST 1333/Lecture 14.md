+++
title = "Political and Human Emancipation"
date = 2020-03-24T17:42:16-07:00
series = "HIST 1333"
+++

## Recapitulation: The Young Marx

### Marx and Hegelianism
- Marx awakened to philosophy through Hegel's student, Gans

### From the Critique of Religion to the Critique of Society
- Feuerbach’s The Essence of Christianity (1841) and Marx’s “Theses on Feuerbach” (1845)
  - Marx: change the world, not just interpret it
  - philosophy to plunge into the world just as spirit plunged itself into concrete reality

### Marx's Early Career in Journalism
- did not become an academic philosopher like Hegel

## Marx's essay, "On the Jewish Question," ('Zur Judenfrage'): *Deustche-Französicher Jahrbücher (February 1844)

### Some Preliminary Remarks on the Notoriety of Marx's essay
- notorious essay because it's tainted by anti-Semitic language
- why Marx uses such language: need to understand history of European Jewry
- true in general of Jews in modern Europe that they were allies of Enlightenment and universalist promise
  - excluded from most European trades, forbidden from owning land, not citizens but inhabitants
  - this exclusion led Jews to believe in universal inclusion and justice
- anti-Semitism in Germany from Wagner to Nietzsche
  - this is more to contextualize the complexities in Marx's rhetoric
- distinguish arguments, rhetoric, political aims

### Marx in Debate with the Left-Hegelians
- Marx's essay really a "book review"
- first part of essay: direct engaging with Bauer's pamphlet
- Bauer saw alienation in Christianity
  - Judaism was distinctively mistaken because Judaism not only subscribes to an other-worldly god and fails to appreciate nature of human agency, but it also abstains from general religion that Christianity has where everyone is included in experience
  - stage-wise process of human history and more peculiarly, for Jews to convert themselves to Christianity before then throwing aside that universal religion of Christianity and arriving at universal human freedom
  - very dialectical way of applying Hegelian stages

### Marx on Political Emancipation
- endorses freedom and rights of the bourgeois individual
- individuals' rights are sacred: no individual should be forbidden from enjoying such freedoms
  - essence of bourgeois society: sanctifies those political rights, rights of individual
- Marx insists that the achievements of the French revolution are to be accepted
  - approves of these political freedoms
  - approves of Jewish political emancipation
- political emancipation of the Jews a criterion for how rational a bourgeois state is
  - Marx not agreeing with Bauer's torsion that Jews need to cast aside their Judaism
  - everyone should have freedom of religious conscience
- Bauer: "eyesore" of the Jewish community in the modern world
  - Marx's response: subtle nod to his own identity
  - Marx is basically granting that the experience of oppression that his ancestors suffered is one that has enhanced his own understanding of the nature of oppression for all peoples in the modern world

### Marx on Human Emancipation
- draw distinction between political and human emancipation
- endorses political emancipation, the freedom of individuals as defined by modern bourgeois state
  - notes that political emancipation is a matter of formal freedom as defined by a constitution
  - what that kind of freedom ignores is the entire realm outside of formal, political law: the realm of civil society
- as state secularizes, religion becomes private
- modern bourgeois state perfects our formal freedoms as individuals and therefore, we are free as individuals to be religious
  - but, to be free as a religious individual does not mean full freedom as human being
  - free for an egoistic identity, separates us from one another
- move beyond the freedoms of bourgeois society: the emancipation of the human
  - revolution in civil society where we find ourselves to be egoistic individuals
- having introduced human emancipation, essay takes polemical turn
  - Marx: look at religion not as a set of doctrines but in its most concrete form
- *Judentum*: synonym to financial world
  - if we are concerned with human life and worldly existence, what is Judaism in its most worldly terms?
  - general polemical description of modern finance and exchange and the egoism in the economic realm
  - unfreedom = Judaism; dialectically twist Bauer's anti-Semitism
- uses Hegelian logic to say that: modern world is a Christian world, modern bourgeois society perfected the universalist gesture in Christianity and gave us universal political rights, so it's modern Christian society which has made the features of society completely uniform and universalized even the unfreedom that was ostensibly ascribed to Judaism
  - political emancipation is to be endorsed but is not yet human emancipation
  - in material life, a kind of egoism inhibits ourselves

### General Remarks on Marx and the Jewish Question
- using idea that Judaism is about egoism and enlists it into his own argument, which is really one about capitalism and unfreedom
- dismantle Bauer's objections to Jewish freedom: only once political emancipation solved can solving human unfreedom come into view
  - stage-wise argument that moves from political to human emancipation
  - first a precondition for the second
  - will reappear in Marx's theory of history
- but, Marx introduces a further twists
  - on one hand, looks like he accepts stadial argument
  - emergent theory of ideology: by fastening too much attention on bourgeois political freedoms that we lose sight of human freedom that is to be achieved
  - not that bourgeois society is wrong, but takes its freedoms as only kind, highest type of freedoms to be achieved
  - can obscure our unfreedom that exists in economic life

## Engels, *The Condition of the Working Class in England (Die Lage der arbeitenden Klasse in England)*, Leipzig (1845)
- not as sharp a turn as may appear
- what Marx has addressed in abstract terms, Marx and Engels witness in continental Europe and England: actual suffering of labor

### Friedrich Engels (1820-1895): Life and Thought
- textile industry
- lifetime friendship with Marx
- financial support for Marx
- promoter of Marx's work
- more inclined to scientific understanding of Marxism

### The Young Engels on the Working Class in Manchester
- seems like a Victorian genre: redemptive ethnographies, reporting and novels, written chiefly for bourgeois readers that describe the depredations of industrial life
- lead up to Sinclair's *The Jungle*
- not written for laborers (they knew what their conditions were) but for bourgeois readership who would respond and endorse acts of political reform
  - not uncommon that texts like this spawned political reform
- Factory Act (1833), Mines Act (1842), Ten Hours Act (1847)

### From Reform to Revolution
- Engels, like Marx, sees that political and legislative measures are admirable
  - embraces the above acts: modern bourgeois state commits itself to certain humanizing laws
  - but, also sees that political legislation under capitalism is insufficient
  - does not address root cause of working class suffering: broader structure of bourgeois economic life itself, which is premised on private property
  - presents problems as not fully redressable by legislation but through structural transformation

## Concluding Remarks

### From Moral Revulsion to Structural Critique
- bourgeois society has brought into being certain possibilities
  - brings about certain formal rights

### Bourgeois Society and its Limits: stadial theory and the theory of ideology
- the bourgeoisie who speak in moral register obscure key systemic features that cause misery
  - thus, reform will never be enough
  - moral complacency about problems
