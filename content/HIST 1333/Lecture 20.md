+++
title = "The Writing of Capital"
date = 2020-04-17T01:04:53-07:00
series = "HIST 1333"
+++

## Introduction

### From the *Grundrisse* to *Das Kapital*
- Marx's perfectionism: not a practical man
- learned Russian to incorporate Russian sources, statistics
- slow but enormous impact
  - Lenin
  - most cited work in all of the social sciences published before 1950

### A Total Critique of Capitalism: Economic System, Ideology, and Form of Life
- title has capital, not capitalism, in it
- three objects of inquiry
  - internal analysis of capitalism as an economic system
  - critique of bourgeois political economy as the formal ideology and beliefs that justify capitalism
    - literal sense: society exhibits a political economy; criticize material system
    - broader ideological formation in view: explanatory structures of political economics that bourgeois economists have developed to explain and justify the capitalist system
  - broad critique of capitalism as a socially and historically distinct form of life
    - anthropological and philosophical analysis
- capitalism was a mid-19th century neologism (along with other -isms)
  - imply abstraction and ideological status
- Marx uses capitalism only twice in *Capital*
- endows capital with a certain anthropomorphic quality

### Marx's *Capital* and Hegel's *Phenomenology of Spirit*: A Preliminary Comparison
- discern in this work a reprisal of certain themes with young Hegel
- work of spirit and work of capital
  - much like Hegel, Marx sees capitalism as a total system
  - if you thin about capitalism as a self-sustaining system, it is a self-affirming though self-undermining holistic structure
  - historical completion: Marx, like Hegel, seems to believe that history reaches an end of culmination; understand everything that has happened before
    - Marx: historical completion comes after capitalism through communism

## Marx's *Capital*: General Remarks on Method and Structure

### Marx's Comments on Hegel in the "Postface" to the Second Edition (1873)
- attacking the premise of economics as a quasi-naturalistic realm
- indulges in crude "mere inversion"
- loving acknowledgement of Hegel as his instructor

### Marx's Comments in the "Preface" to the First Edition (1867)
- focus on England: represented the future of the world
- anonymous functioning of the system irrespective of intentions of economic agents
  - not interested in targeting individual capitalists: "personifications of economic categories"
  - formed by social conditions
- system has a certain historicity
  - society of growth and transformation

### General Structure of the Book
- begins book with bourgeois assumptions: bartering
  - the "economic cell form" for bourgeois society
  - present a simplified picture of realm of exchange
- commodity does not enjoy independence it appears to have
  - independent life is an illusion
  - press skeptically against illusion, move from exchange to production
- capitalist mode of production requires the extraction of surplus value
  - extraction as exploitation
- dialectic is to move from exchange to production, to put skeptical pressure on commodity form, disenchant that form, undo fetish, pass beyond commodity form and exploitation

## Four Preliminary Definitions: Value, Labor, Exchange, Commodity

### The Distinction between Use-Value and Exchange-Value
- use-value: intrinsic or material utility of a thing
- exchange-value: enters into a circuit of exchange when it assumes the form of a commodity
  - object has exchange-value in so far as it takes the form of a commodity
- what determines exchange-value?

### Marx on the Labor Theory of Value and Value in General
- Marx's subscribing to "labor theory of value": very controversial
  - most economists believe LTV to be patently false
- interestingly, Marx's argument rather subtle
- adapt Ricardo's LTV, but modifies it a great deal
- value does not derive from the particular amount of labor that goes into the production of the object
- need to look at given social context, understand what's considered socially necessary labor time
- do not look at one particular object, but at abstract labor, average socially necessary labor time
- commodity's value can be defined as "congealed quantities of homogeneous human labor"
- the result of this is to immediately think of labor itself as an abstracted commodity that can be aggregated

### The Principles of Capitalist Exchange
- two moments
- exchange: equilibrium
- capitalist profit: capitalist has money, uses it to buy certain structures, mixes that commodity with labor (a commodity), derives out of their products further money
  - how?
- look at commodity form, understand the mystery of the commodity form
  - laborer is a commodity
  - laborer can produce more value than he costs
  - difference between those two is "surplus labor value"
  - objective measure of exploitation

### The Analysis of the Commodity
- not enough to analyze exchange
  - will not tell us about dynamics about capital, why system of capitalism can generate value
- instead, analyze commodity form
  - understand nature of value in that form
  - scrutinize commodity form, begin to appreciate the socially average labor time that lends the commodity its value in exchange
- exchange not independent of capital
  - only exploitation in capitalist mode of production lends commodities the value they have
- Marx will begin with the mysterious appearance of the commodity
  - approach commodity in dialectic fashion
  - illusory independence
  - press beyond independence, strive to understand how commodity becomes a source of value
- commodity fetishism

## Concluding Remarks: Plunging into the Argument
- the mysterious appearance of the commodity and the drive beyond appearances
