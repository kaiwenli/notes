+++
title = "The French Revolution"
date = 2020-02-18T16:21:44-05:00
series = "HIST 1333"
+++

## Toward Modern Society: Hegel and the French Revolution

### The French Revolution as the Birth of Political Modernity

### German Thinkers and the Legacy of the French Revolution
- Kant's sympathy: revolution as indicative of human progress
- accusations of reductive forms of materialism
- Prussian "reform"
- German nationalism, romantic conservatism
  - moderate form of democracy, constitutional monarchy
- rise of German liberal nationalism

### Hegel's Dialectical Response
- Hegel: endorsed ideals, not implementation
- pivotal chapter in history of spirit
- philosophical and political
  - philosophical: French materialism, associated doctrines of utility
    - danger of abstract right: does not appreciate importance of institutional mediation
	- reminiscent of radical asymmetry in master-slave dialectic
	- Estates: mediated way of understanding power
	- not endorsing feudal order but advantageous in counter-forcing
- Rousseau: "general will"
  - any state built out of this will give over sense of confidence in citizens
  - to overcome, need more institutionalized state of freedom
- a dialectical assessment: both affirmation and criticism

## The Critique of Utility, Rousseau, and the "General Will"

### The Critique of Enlightenment Philosophy
- Hegel: French Revolution represents culmination in intellectual development
- utility: value of anything is due to only what it's good for as stipulated by foreign agent
  - dangerous: abstract will imposing its purpose on a wholly submissive object; danger of domination
  - attack British proto-utilitarian philosophy and French materialists

### The Critique of Rousseau's General Will
- troubled by Rousseauian general will
  - attempt to unify public without taking into account of differentiations between people
  - general will looms much larger than individual will
    - express not what they want, but what they think what the general will is, which may be wrong
  - Rousseauian general will came to power with Jacobins
- the reduction of religion to an other-worldly abstraction
  - Jacobin attempt of post-Christian, deist religion
- no mediation between state and individual
- domination of universal over particular

## The Critique of Jacobin Factionalism and the Terror
- when you have self-appointed leaders, who claim to have interest of people, no one can constrain them
- factionalism
  - any cult of virtue gives rise to this, attacking one another for not having the proper virtue
- universalized suspicion: mass execution
- death loses its meaning as end of individual life
  - guillotine, drowning
- spirit's attempt at freedom without mediation

## Lessons of the French Revolution: A Need for Mediation and the Turn to Morality

### The Turn to Morality
- no more wholly abstract opposition between general will and particular will
- flaw in Kant's moral theory
  - Kant: to be moral, obey universal law, which may conflict with individual inclination
  - Hegel: this clash visible in French Revolution
    - abstract universal right and individual desire
	- formalize a principle you are adhering to in your conduct conflicts with empirical self
	- unworkable, un-dialectical dualism

### The Need for Mediation
- Hegel: an understanding of political life that what we want for ourselves harmonizes with the general good
- must be mediation: overcome above sharp dualism
  - political philosophy that embodies this mediation individual freedom and notion of common good
- endorse spirit's stage
