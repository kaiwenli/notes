+++
title = "A Specter is Haunting Europe"
date = 2020-03-31T18:14:01-07:00
series = "HIST 1333"
+++

## Recapitulation: Marx on Emancipation, Political and Human

### Marx's Stadial Theory of History (as anticipated already in "On the Jewish Question,")
- history goes through epochal shifts
  - each stage generates problems it can't solve, overcoming through next stage, newer plateau
- bourgeois/"political" and communist/"human", freedom

### The Dialectical Relation of Historical Stages
- political freedom must precede human freedom
  - perfect bourgeois society, set in place all the rights of political individuals
  - then, perfect itself, prepare terrain for communist revolution

## Marx and Engels, "Manifesto of the Communist Party" (1848)

### Historical Background on the Origins of the Manifesto
- supposed to be foundational text stating principles of so called "Communist Party"
- The League of the Just -> "Communist League"
  - needed to draft a statement, a charter for itself as a party
- task first fell to Marx's Left-Hegelian colleague, Moses Hess
- Marx and Engels redraft manifesto
- Engels writes first draft in the former of a "catechism"
  - Marx discusses this further, abandons this form
- rewrite took quite a while: finished January 1848

### General Remarks on Reading the Manifesto and its Genre
- how shall we read the "Manifesto"?
- seems like a formal statement of theoretical principles
  - bring philosophical expectations, expect rational coherence
  - text in social theory
  - somewhat misleading
- also very much a specimen of political propaganda
- both of these at once
  - not unusual for Marx that he writes in both these modes simultaneously
  - read it both ways at once
- instruct working class and nourish revolutionary consciousness
  - motivational force

## The Argument of the "Manifesto"

### The Bourgeoisie as the Revolutionary Protagonist of the Modern World
- bourgeoisie is affecting the most changes in modernity 
- changes that are the prerequisite for a future revolution
- political changes: abolition of feudalism
- structural, gradual changes:
  - urbanization
  - industrialization
  - expanding commerce beyond Europe
    - global expansion of commerce unified people across vast distances
    - progressive movements of nationalism that removed traditionalist principalities
    - thinks of nationalism at this point as progressive movement in history
  - modernization of the family
- stepping back from all of this, ultimately, all can be measured in terms of economic success
  - all other values become unimportant
- crucial development
  - endorses this stage
  - clarifying life
  - see for the first time in history that our economic relation with one another is the deepest, material fact about life
  - thus, a demystification

### The Dialectical Work of the Bourgeoisie
- crediting the bourgeoisie with the most revolutionary role in history
  - clarifying nature of human existence
- preparatory work for communist revolution
- concentrate society into two opposing classes: bourgeoisie and proletariat
- remains work of human emancipation
- bourgeois freedom has perplexing quality that it advertises itself as only type of necessary freedom
  - obscures fact that there is any remaining un-freedom to worry about
  - thus, in this regard, bourgeoisie is mystifying and ideological

### The Revolutionary Role of the Proletariat
- conditions develop in which a modern revolutionary class emerge
  - bourgeois society generate a modern working class
- immanent negativity: generating in its own society a tension it cannot resolve
- as bourgeois society enriches itself it subjects working class to greater misery but also greater solidarity
- by revolutionizing society bourgeoisie digs own grave
- introduce into his picture of bourgeois society a dialectical understanding of history
  - closely resembles Hegel's model
  - certain epoch flourishes, but generates internal contradictions, resulting in epoch overcoming itself
- ascribes to working class a universal role in history
  - misery in working class represents final contradiction
  - anticipates a kind of final resolution of the highest problems of all of history
  - working class spreads across globe, knows no national identity
  - global consciousness: truly universal class
- extravagant claim; only makes sense as Marx's attempt to refurbish spirit's self-satisfaction

### The Role of the Communist Movement
- must develop revolutionary consciousness
- self-reflexivity: furnish proletariat with clear understanding of its mission
  - "my theory will play a role in the developments I'm describing"
- Marx's approval of nationalism was always conditional
  - create global reach but disapprove before it turns reactionary
- importance of internationalism
- political agitation is the ultimate purpose of the manifesto
  - in popular imagination, there is a crude misunderstanding of Marx's theory
    - bourgeoisie is destined to come to power, proletariat revolution inevitable
  - Marx thinks that proletariat must know that it shouldn't be merely satisfied with piecemeal reforms
    - if working class does not understand gravity of situation and mission to which it's charged, it might lapse into reformism
    - modify bourgeois society and not overthrow it
  - organized communist party is necessary

## Concluding Remarks

### Marx as Theoretician of the Communist Movement
- contribution to Marx's theory but also a motivational document to working class
- distinguish communist party from other competitors
  - goes after other parties

### Marx as Post-Hegelian Philosopher: The Manifesto as a Statement of Dialectical History
- inheritor of Hegelian themes
- a statement of the dialectical structure of history
- determinate, not abstract, negation
  - bourgeois society, in overcoming itself it fulfills what it has always promised, a kind of higher freedom
- Marx remains faithful to Hegel's universalism and a closure of history
  - the universal class, resolve its own history, resolve problem of history as such
  - arguably prophetic ideal, as Hegel an inheritor of religious longings
  - source of philosophical embarrassment

### Marx's Historical Optimism in 1848
- implies that revolution is immanent
- history to be resolved soon
- within a year or two, Marx's optimism destroyed
