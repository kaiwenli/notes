+++
title = "The Idea of a Phenomenology of Spirit"
date = 2020-02-08T23:29:47-05:00
series = "HIST 1333"
+++

## Hegel's Phenomenology of Spirit (1807): A Recapitulation

### What is "Spirit"?
- *Geist*: "social space" v. metaphysical principle
- sculptor v. marble: misleading
  - just as how theist is misleading
  - creator pre-exists; metaphysical presence before creation
  - think of sculptor and marble as originally one
  - *self*-creation
  - she is not yet an artist before engaging in work of art
- *Geist* is not *Geist* until go through process of self-externalization
  - does not pre-exist its self-expression to the world
- both readings are responsible, but in different ways
  - charitable intent of text such that you think it's defensible

### What is a "Phenomenology"?
- philosophy of history was a major genre in the 18-19th centuries
- for *Geist*, negativity is necessary, moments of suffering

### What is "Negativity"?
- the path of education is marked by struggle and resistance
  - whole of world is manifestation of *Geist*, but also as *Geist* in process of development
- sculptor is not a sculptor until she undergoes process of sculpting
- God is not yet God until he undergoes process of being God
  - does not pre-exit cosmos

## The Significance of History

### The History of *Geist* as a Narrative of Successive Efforts to Arrive at Full Satisfaction
- sculptor: Geist needs to be understood as having an idea, freedom, that it wants to manifest
  - arrive at fullest, most satisfactory
- abstractness to concreteness
- this may sound suspiciously theological
- final stage: reconciliation, highly Christian
  - eschatology, description of the end of the drama of God's self-coming to be
  - but a secularized one, a rational understanding
  - religion contained certain content, to give a secular spin on that rich content
  - philosophical recapitulation of Christianity

### A Philosophical Recapitulation of Christianity
- philosophical reading on life and suffering and resurrection of Christ
  - *Geist* undergoes trials
  - *Geist* needing to manifest itself in concrete form
    - reincarnation
- Hegel's rational/philosophical reading captures the essential truths in Christianity from more imaginative guises
- Hegel: other religions don't see overcoming
  - world and God separate
  - but separation needs to be overcome
  - in the world, redeem the world
- seeming "irrationality" has role in understanding higher rationality: a "theodicy"
  - "theodicy" is a justification

### A Philosophical Affirmation of Modernity
- philosophical justification for our own, modern life
- what modernity needs to be to realize human freedom
  - final and complete justification for what freedom should be in its most complete form
  - high self-confidence
- "absolute knowing"
- primitive experience had to disintegrate due to inadequacies
  - these brute origins are interesting because they contain within them an anticipation of what will come later
  - acorn to oak tree
  - implicit meaning of brute experience is to become this absolute knowing
  - workings of the dialectic even in the most primitive experience
  - "sensuous certainty"
    - sense doesn't need anywhere else
	- brute, simple, incorrigible experience
	- puts pressure on it: very terms of *Geist*'s self-understanding turn out not to be adequate
	  - point to something sophisticated

## The Significance of Negativity

### Reason and its Claims
- method of skepticism
- raising skeptical questions or "tests" that reveal inconsistencies or "internal" dissatisfaction
  - skeptical pressure when brought to sense data can reveal internal inconsistencies
  - by its very own criteria it fails
  - determinate negation annuls and preserves truth

### The Method of Immanent Critique

### The Method of Determinate Negation

## Some Anticipatory Remarks on Marx's Hegelian Inheritance

### Marx's Practice of Immanent Critique
- test bourgeois society by its own norms
  - fulfills its own promises?
  - if not, annihilate it, but fulfill its purported goals

### Marx's Method of Determinate Negation
- promises freedom but generates un-freedom
- will fulfill goals by overcoming itself
- suffering of working class necessary?
  - secularized theodicy
  - suffering in the narrative of freedom
  - don't want to overstate this however

### Marx's Affirmation of Modernity
- what the modern world could be
- could be a final manifestation of human freedom
