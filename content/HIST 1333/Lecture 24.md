+++
title = "Concluding Thoughts on Hegel, Marx, and History"
date = 2020-05-01T14:56:54-07:00
series = "HIST 1333"
+++

## Introduction: Historicity and Inevitability

### Hegel, Marx, and Historicity
- "Continental" tradition
  - tradition of philosophy that developed around Hegel
  - self-conscious of its own historical condition
  - ruminated extensively on the self-reflexivity problem of philosophy
    - philosophy is something that emerges in a particular time
    - understand the time in which his/her own philosophy arose
  - thought is plunged into time and into history
  - Hegel: thought and history reach a kind of fulfillment

### Hegel, Marx, and Inevitability
- direction and fulfillment
- history pre-ordained? inevitability?
- Hegel's *Weltgeist*: sort of inevitability
  - forward looking telos or reconstruction?
- Marx's *Capital*: crisis theory, immanent features of capitalist system that culminate in stark opposition between capitalist and proletariat

### The Enduring Question: How Hegelian was Marx?
- confidence about capitalism's impending crisis?
- if this was likely, what are the conditions for this?

## The Necessity of Fully Developed Capitalism

### Capitalism and Imperialism
- Marx: capitalism much reach full development for crisis to come about
- necessity of capitalism's world expansion
- endorse bourgeois colonization of the world
- "The British Rule in India": despite brutality, colonialism breaks India out of its traditionalism, "oriental despotism"
  - repeating same kind of logic as in "Communist Manifesto"
  - imperialism has a role to play in global transformation
- acknowledgment of brutality but also recognizes its necessity
  - only through that modernization that bourgeois society, capital, can fully develop

### The West-European Model and its Alternatives
- the necessity of establishing a world market
- what haunted Marx: the imminent crisis of European capitalism could be delayed by exporting exploitation abroad
  - northern flourishing by making use of cheap labor
  - export exploitation abroad, raise wages in north while suppressing them in the south
  - socialist revolution in Europe would be somehow disabled
- might be different paths to modernity
  - was it the case that every society had to follow the path Marx described?
  - greater variety than he previously imagined?
- Marx awakened to a certain kind of romanticism
  - romanticism: celebrated national variety
  - Marx himself became alive to the teaching of romanticism as he began to consider the possibility that there was greater plurality in human experience than previously theorized 
- began to study medieval German economic history
  - Marx's late romantic concession to a kind of historic plurality
- especially poignant with respect to Russia: a "backward" nation
  - millions of serfs bound to the land
  - changed modestly over centuries
  - for Russian intelligentsia, this was a painful matter: could Russian backwardness be overcome?
    - if so, could not conform to Marx's monolithic narrative of development
    - something different would have to happen
- Marx on the Russian commune: the letter to Vera Zasulich
  - anxious about letter: at stake was whole possibility of socialist revolution in Russia
  - make room for revolution in Russia
  - although Russian communes look extremely primitive, if turned in proper direction, could be the basis for social regeneration
    - "fulcrum"
  - crucial for Bolsheviks

## Marxism After Marx

### The Soviet Union
- realization or distortion?
- Stalinism, "collectivization," and the Gulag
  - countryside needed to be revolutionized, great misery to hundreds of thousands (if not millions)
  - brutal dictatorship
- socialism with a "human face"
  - seemed a painful mockery of socialism with its most cruel reality

### How Far is a Philosophy Responsible for its own Historical Distortions?
- urgent question for philosophies with a claim to historicity and worldly application
  - bear in themselves a message for the future
- Marx and USSR; Heidegger and fascism
  - entanglements
  - of course there are differences
    - Heidegger: once Nazis came to power, endorsed Nazi rule
      - private notebooks expressed sympathies for anti-Semitism, transforming the globe
  - how much intention is there in a philosophy that we see realized in a moment later in time?
    - Heidegger: knew of Nazi brutality and did not offer apology
    - Marx: never intended anything like what the Soviet state did to its people
- move beyond authorial intention: do ideas themselves bear some responsibility?
  - contain susceptibility to future misunderstandings?

### Philosophy as Ideology
- Marxism became an ideology
- Marx's famous remark: "All I know is that I am not a Marxist."
  - aware his own thinking can be turned into something sclerotic, unresponsive to historical conditions
  - Marxist != Marxian
- theory vs. ideology
  - philosophy: window upon the world, understand its contingencies
    - should not block out inconvenient facts
  - irony of Marxism: became an ideology, weapon against the world
    - justification for human violence, cruelty, domination

## Philosophy as Theodicy

### Theodicy in Hegel's Philosophy of History
- ascribed suffering to negativity
  - "cunning of reason"
  - explain higher and benevolent purposes of world spirit
  - criticism and affirmation
- risk: theodicy immediately becomes ideology
  - when critique reverts to affirmation, begin to justify whatever begins to be the case regardless of brutality

### Theodicy in Marx's Philosophy of History
- suffering as negativity
  - in *Capital*'s crisis theory a materialist theodicy
- risk: political ideology
  - stamping suffering with allure of higher rationality
  - imperialism, coercive transformations of globe necessary to bring about communist future

### From the Workings of Spirit to the Workings of Capital
- the actual is the rational
  - Marx never reaches this degree
- the actual is the irrational
  - capitalism destined to fall apart because of its own irrationality
  - therefore, Marx more critical than Hegel (although debatable)

## Concluding Remarks: From Hegel to Marx and Beyond

### From Hegel to Marx: Holism as an Enduring Bond
- systems holism: "the true is the whole" (Hegel)
  - scope all of reality
  - world as a system
- world system, integrating into it everything resistant or outside capital
  - only through global work of capitalism could it be overcome
  - enduring debt to Hegel
- historical or longitudinal holism: history must reach closure or fulfillment (Marx)
  - theologian
  - all human strife disappears, class conflict is overcome
  - another enduring bond between Hegel and Marx
  - but, possibility of blindness to other kinds of human suffering?
    - does not debunk Marxism, but is a reason to not become full ideologues

### Beyond Marx
- (mostly prof's views)
- sustain negativity without moving to affirmation
- best dialectic tarries with the negative
- "the whole is the false" (Adorno, *Minima Moralia*)
  - already there in both Hegel and Marx
  - liberate the negative from the affirmative
- Marx against Marx, and by extension, Hegel against Hegel
  - alerting us to negativity rather than moving us too swiftly toward the holism that overcomes negativity
  - retain critical thrust of negation without reconciliation that is supposed to bring all negativities to an end

### Philosophy, History, and Human Responsibility
- (mostly prof's views)
- human suffering as an affront to reason
- if history is record of human suffering, then no rationality can provide quasi-rational justification
- higher obligation of philosophy
  - not Being, not spirit
  - remain open and responsive to suffering in the world, just as we must remain responsive to fragility and need
