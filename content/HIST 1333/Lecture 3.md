+++
title = "The Dialectical Structure of Reason"
date = 2020-02-04T20:25:53-05:00
series = "HIST 1333"
+++

## Hegel and the Enlightenment: A Recapitulation

### Affirming the Enlightenment but Overcoming its Dualisms
- Hegel not too favorable to Kant
- Kant's transcendent idealism is a world of appearances and a world in itself beyond possible experience
  - dialectic: making knowledge-claim about this latter world
- Hegel gives dialectic a new meaning

### Hegel's Requirements
- reason imposes limits; both differentiation and reconciliation
- what model of reason could achieve such a higher reconciliation *without* surrendering reason's gains?
- for Herder, every nation has a national spirit
  - unfolding intentions, contribute to larger human progress
  - a genius that belongs to every nationality
  - self-expressing externalization
  - "expressivism"
  - subject not contemplative, but active, an agent
  - strives to realize its purposes
  - highly instructive to Hegel, best moment of what *Geist* is, achieving both differentiation and reconciliation
  - dialectic had different definitions in Hegel's own work; paradigms

## Hegel's Dialectic: A Preliminary Account

### The Paradigm of Creation (Sculptor and Marble)
- initial: she sees inert marble block
  - sees idea, but remains in head, abstract
  - marble seems inert and questions if marble can become her idea
  - negativity, obstruct her purpose
- intermediary: chip away at, negates negativity
- final: steps back from block
  - she experiences sense of expanded agency
  - although block of marble separate, reflects her purposes
  - therefore, both represents her and other than her
- formal description: from a) disunity through b) negation to c) a higher unity that also preserves difference; thus achieving, in Hegel's infamous phrase, "the identity of identity and non-identity"
- capture something an artist might experience
- subject tries to manifest idea in world

### The Paradigm of Analysis (Subject and Object)
- wandering in forest, see a Buick
  - have no idea what it is, just a big inert thing
  - take it apart, differentiate it, analyze it through separation, dismembering the Buick
  - now bits of car are strewn through forest
  - had group thing you didn't know, but you know how the bits work by themselves
  - equipped with this knowledge, reassemble
  - reassemble holistic object unknown in the beginning
  - you've learned something
    - analytical knowledge
- process of rational analysis
- to know anything is to know through its dismemberment, gain through reassembly
- deeper rational understanding of the whole
- dialectic another name for how reason works
- a sense of differentiated unity, mediated whole

- pays tribute to Kabbalah
  - worked its way into German philosophy
- Schelling, Hegel's roommate
  - gave him picture of world
  - divine withdrawal
  - influenced Hegel in which spirit must undergo self-negation and differentiation to result in self-creation

### The Paradigm of Agency
- initial: confront world as other, obstruct purposes
  - deep dissatisfaction
  - to be an agent, world theatre for self-realization
- intermediary: subject tries to overcome obstruction of the world
  - self-externalization, express itself
- final: achieves agency as free subject

### Summary: the Dialectic as the Model of Reason's Self-Actualization as Freedom
- dialectic: shape of reason's activity
  - technical name for the form that reason takes as it tries to realize itself in the world
- reason cannot remain abstract but most also be a concrete universal
- reason must grasp itself simultaneously in unity and in separation, achieve differentiated wholeness
- reason, as subject, strives toward freedom
- in summary: "The actual is the rational, and the rational is the actual."
  - in final stage of dialectic, reality itself manifests in satisfactory way the aims of reason(?)

## Hegel's "Preface" to *The Phenomenology of Spirit* (1807): Preliminary Remarks

### What is "Spirit"?
- *Geist* as deflationary: human mindedness
- *Geist* as metaphysics: works out purposes through humanity

### What is a "Phenomenology"?
- study of spirit in its various "appearances" (philosophical, cultural, historical, theological) over time
- narrative of spirit
  - traces out the path of spirit's education, *Bildung*
- a novel of spirit's education, an attempt to realize itself in more sophisticated and adequate ways
- story of philosophy that unfolds in time

### The Importance of Negativity
- just as in any story of worldly education, struggle, difficulty, disappointment
  - in other words, negativity
- spirit moves forward by encountering otherness
  - recuperate that otherness into own higher sense of identity
- only by recognizing yourself in others that you can achieve higher sense of self-knowledge
- move out into rational, differentiated unity
- negativity, becoming other to oneself, is important to spirit's education
- rational understanding must include negativity
- something irrational is working in service of reason
- Mephistopheles in *Faust*
- does not cover brute irrationality (e.g. stubbing toe)
- but, a great deal in human expression which seems irrational works in the service of our collective benefit
- this is a philosophical principle
- even stuff that seems false must be worked with
  - spirit does not mean you avoid looking at the negative; this is not a philosophical attitude
- even the most partial philosophical doctrines have something to teach us
- enlist them in project, higher perfection
- example: professor's book reviews
  - occasionally, write scathing review
  - but that is a bad practice: you only sweep it aside, but no one learns anything
  - find even in the flawed, turn that into better purposes
- flaws are themselves instructive
- tarry with the negative as it can be educative

### Knowledge as both Process and Result
- culminates in absolute knowing
- only from position of spirit's satisfied condition and see how each of these steps are necessary
- only in completion can we understand negativity's role
- once stable, why volatility was important
- peace contains the volatile movements it underwent
- capture rational unity and rational differentiation
  - movement and structure
- "The True is the Whole"

## Concluding Remarks and a Question

### Dialectics and the Significance of Negativity
- negativity is not to be confused with "contradiction," and dialectics is not a mere "clashing" of mere opposites
- abstract negation versus determinate negation
  - abstract negation: kill or annihilate something
  - determinate negation: both overcoming and preserving

### The Phenomenology and the Primacy of Holism
- how compatible is this negativity with Hegel's holism?
  - if negativity is always encountered by spirit, final satisfaction seems to be ruled out
- Hegel's holism as metaphysics: a challenge to the deflationary reading?

