+++
title = "Exploitation and the Formula of Capital"
date = 2020-04-21T19:29:34-07:00
series = "HIST 1333"
+++

## Introduction: Marx's Concept of Exploitation

### From Alienation to Exploitation
- move away from philosophically rich category of alienation and toward economic analysis
- Hegelian inheritance does not seem to be present
- extraction of surplus value
- what happened to alienation? struggle for recognition?

### Marx's Mature Critique of Capitalism
- focus on specific economic feature of capitalism: exploitation
- might seem to be purely objective feature of capitalism
- term which implies moral condemnation?
  - is exploitation wrong from a moral point of view
- morally loaded term, but Marx tries to capture an objective feature of the capitalist system
  - seen this way, category without normativity
  - recall *Critique of Gotha Programme*: ideals can't be higher than given social arrangements of the time
  - capitalism can be evaluated purely in terms of its function
  - pick out exploitation, which shows us why capitalism in the long term is destined to fail
- function vs normative question
  - just because something doesn't function doesn't mean that it's morally wrong
  - just because something does function doesn't mean it's morally right
  - thus, dysfunction itself does not count against something morally
  - exploitation does not permit us to say that capitalism is unjust

## Marx and the Labor Theory of Value

### On the Distinction between Use Value and Exchange Value
- Smith: utility vs power of purchasing other goods
- Marx: there is a particular commodity which can generate more value than it costs
  - this commodity is labor
  - focus on the exchange value of labor
  - realm of commodities into realm of labor
- where does value truly come from?

### Where Does Value Come From?
- inheritance from Smith, Ricardo: "labor theory of value"
  - Ricardo: amount of labor that goes into producing commodity is value
  - *not* on compensation for labor
  - opens up a space where Marx can rethink labor theory of value
- Marx moves past production of commodity
  - understand how labor is a peculiar commodity
  - one that can generate more value than it costs
- once labor is treated as commodity, it becomes socially average, abstract, universal
- exchange is revealing something about the way labor is treated in our society
- if we understand the commodity form correctly, reveal that it mystifies labor behind it
- value of a commodity is determined by average socially necessary labor power, not the value of the commodity in its individuality
- laborer generates value, but is simultaneously a commodity

### The Laborer as Yet Another Commodity
- laborer does not sell discrete acts of labor
  - on the contrary, sells labor power
- labor power: the minimum social cost necessary for the reproduction of the laborer
  - what you buy: food, rent, reproduce them
- value of labor power: you buy the laborer's capacity to produce value
  - reproduce the laborer
- crucial to Marx's analysis of capital

## The Formula of Capital

### How Does Capitalism Work?
- formula of exchange: C^1 - M - C^2, where C^1 = C^2
- formula of capital: M - C - M^1, where M^1 > M
- how is this the case?

### Labor and the Extraction of Surplus Value
- laborer, as a commodity, sells his commodity, but his commodity is labor power
- value laborer can produce is greater than cost of reproduction of labor power
- this difference is surplus value

### The Extraction of Surplus Value as an Objective Measure of Exploitation
- exploitation is the name we give to generation of surplus value
  - simple, objective feature
  - if there were no surplus value, capitalist would get no profit
  - nothing would be set into motion without it

### The Composition of Capital
- mix together commodities, derive from it profit
- constant capital and variable capital
- constant capital: e.g. factory, raw materials
  - relatively fixed sums
  - e.g. knitting machine and cotton cost certain amount
  - average across an industry
- variable capital: social average
  - you can pay the laborer a lot or much less
  - changeable sum, starts being suppressed
- extract as much of surplus value by reducing expense of laborer
  - can't reduce constant capital as much

## Capital in History: the Declining Rate of Profit and the Final Crisis

### Concentration of Industry and its Consequences
- over time, capitalist system transforms
- laborer experiences decline with what they are rewarded with
- capitalist enterprises expand, there are less of them, more and more competition, reduce costs through compensation
- industry itself becomes efficient
  - laborer can produce more for the same price
  - unemployment
  - this compels people to accept low wages

### Capitalism Against Itself: Marx's Crisis Theory
- exploitation is objectively part of capitalism
- undermines its own functionality
- socialization of labor
  - labor population evolves too
  - abandons small trade
  - rise of the homogeneous and international

## Concluding Remarks
- analysis of proper functioning of system
  - not intentions of capitalists as individuals
  - not maligning individuals
- exploitation: built-in way of generating profit
  - system-functional, immanent rationality
  - system-dysfunctional, irrational
    - seen in crisis theory
    - leaves swaths of population destitute
    - any social order which requires such a dramatic degree of human suffering cannot qualify as rational
    - actual is the irrational, irrational now actual
    - massive inequality that justifies capitalist world
