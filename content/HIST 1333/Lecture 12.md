+++
title = "From Hegel to Marx"
date = 2020-03-16T22:35:52-07:00
series = "HIST 1333"
+++

## Introductory Remarks: Karl Marx, Life and Thought

### Reading Marx Today: Contextualism and Obsolescence, Social Theory and Philosophy
- experience of past as hermeneutic
  - not confined to the past
- theodicy
  - Hegel's philosophy of history justifies human suffering
  - mind's need to understand human suffering
    - spirit's interests ruled supreme, but it's human spirit, so it's less troubling
  - Marx: offer explanation and remedy that offers dramatic transformation of human life
- Marx trying to understand society
  - situated thinking, much like Hegel; self-reflexivity
  - strives to comprehend, then transcend
  - exemplar, and resisting, his time

### Biographical
- born into family of German Jews
  - troubled history: ruled by princes etc
  - suffered discrimination, no citizenship
  - Napoleoni liberation
  - reactionary Prussian principality revoked emancipation of German Jews
    - Karl's father wanted exception in order to practice law
    - conversion to Christianity without conviction

## The Young Marx in Berlin and the Discovery of Hegelianism

### Hegel's Students in the Berlin *Vormärz*
- Young Hegelians: dismantling religion, dialectic techniques against Christianity
  - ruthless critique of religion
    - religion rules as a despot
    - criticize reaction, monarchy

### Marx's Discovery of Hegel's Philosophy
- Gans' lectures transformative experience for Marx
- "conversion experience"

## Marx's Dissertation, "The ifference between Democritean and Epicurean Philosophies of Nature,” (1839-41)

### The Birth of Marx's Materialism
- anti-Platonist: resolutely materialist
  - rejected other-worldly, Forms

### Materialism as Rebellion
- discern analogy between anti-idealism and democracy
- likens himself to Prometheus

## Marx's Retrospective Remarks on His Intellectual Development

### Marx's Debt to Hegel: The Inversion Thesis
- simple inversion: social being controls consciousness

### Marx, *"Preface” to Contribution to the Critique of Political Economy* (1859)
- misleading? crude caricature of his journey with Hegel

### Marx, Afterward to the Second German Edition of *Das Kapital* (January, 1873)
- don't take Marx's own reflection too seriously
- Hegel's dialectic is "mystified," or "standing on its head"
- inversion thesis the embarrassment of mature Marx Hegel influenced
- something mere that is retained in Hegel's philosophy when Marx transforms it

## Concluding Remarks: the Passage from Hegel to Marx

### The Truth of the Inversion Thesis
- Hegel's idealism; Marx's materialism
- the aims of spirit and the justification of human suffering

### The Falsehood of the Inversion Thesis
- dialectic a rational model to understand mediation of all things
  - things don't operate in crude causation from idea to matter, but rather complicated mediation
- Hegel: highly complicated jargon, labor of concept
- challenge of reading Marx: polemical intention vs. philosophical insight
  - e.g. inversion thesis; if this was the case, creatures of pure reflex; too dialectical for that

### The Importance of the Critique of Religion and Left-Hegelianism
- turn from Spirit, affirm humanity
  - *Geist* as mere spectre
  - human bid to be free
- replace theology with anthropology
