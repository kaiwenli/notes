+++
title = "The Economic and Philosophic Manuscripts of 1844"
date = 2020-03-26T23:30:51-07:00
series = "HIST 1333"
+++

## Recapitulation: The Young Marx

### Marx's Dialectical Response to the Left-Hegelians
- Marx still thinking explicitly in Hegelian terms
  - true in *Theses on Feuerbach*
  - true in *The Holy Family*, written with Engels
- for Marx, Left-Hegelians think dismantling religion through philosophical critique result in freedom
  - philosophically criticism sufficient to liberate humanity
- for Marx, critique of religion is insufficient
  - move from interpretation to change
  - critique of religion to critique of society
  - focus on alienation in material life, specifically in labor

### Karl and Jenny in Paris
- married then moved to Paris in 1843
- Marx banished by French for left political activities by 1845
  - moved to Brussels, then London
  - (birth of daughter in Brussels = Brussels sprouts?)
- Marcuse: what these manuscripts show is that Marx's critique of capitalism were always philosophical
  - not just economic, but "into a dimension in which the subject of the investigation is human existence in its totality"
  - Heideggerian way of phrasing it
  - Marcuse shifts away from Heideggerian vocabulary to Marxism due to Heidegger's affiliation with Nazi party; his thinking was infused with Heideggerian thought nevertheless
- Frankfurt School draws upon work of Lukács, birthplace of 20th century revival of Hegelian Marxism
- by 1932, many scholars becoming aware that Marx never abandoned his Hegelianism
  - philosophical vocabulary explicit and complex
  - not merely economic thinker

## The "Economic and Philosophic Manuscripts" (1844)

### Marx's Hegelian Premise
- labor is not merely the metabolic reproduction of the human organism
  - labor is the objectification of human being itself
  - borrows heavily from Left-Hegelian terminology
  - not mere metabolism but far richer: externalize our very consciousness
- homage to Hegelian dialectic
  - image of the sculptor and block of marble: dialectic reaches its conclusion when the artist can step back from the completed sculpture and experience a sense of broadened self-hood and satisfaction, manifest himself in what he has created
  - duplicates himself, objectifies himself, externalizes himself in otherness
- Marx extols humanity in its Promethean capacity to create the world, to impress on the world his own purposes
- more philosophically, what Marx is doing here is developing human-centered dialectic
  - anthropogenesis: coming to be ourselves as human beings
- Marx's attempt to apply to human labor the work of Hegel's *Geist*
  - for Hegel, *Geist* was trying to realize itself
  - for Marx, turn that to self-realization of humanity

### The Diagnosis of Alienated Labor
- labor under unfree conditions cannot fulfill project of self-realization
  - disfigures anthropogenesis, prevention from reaching fullest satisfactions
  - robs the human being of that final satisfaction where we see ourselves in the world that we have created
- diagnosis of alienated labor
  - image of seeing God as our self-externalization
  - Feuerbach: God is an alienated anthropogenesis
    - all of our concepts are our own species-being objectified
    - in God, that objectification is torn away from us
  - similar language in Marx
    - his own account of alienation is built upon a religious model, the Left-Hegelian critique of religion

### The Manifold Structure of Alienation
- experience of alienation not limited to single human being on a certain occasion, but applies to all of humanity
- identifies four ways
- laborer's alienation from product of labor
  - laboring under condition that the thing you make is not yours
  - literal understanding
- laborer's alienation from laboring activity itself
  - laboring experience itself then becomes destroyed
  - a worker is alienated not only from the product, but from the activity of laboring
- laborer's alienation from him/her-self
  - Marx's theory of anthropogenesis underlies this whole argument
  - laboring activity is human self-expression
  - if we are alienated from laboring activity, we are alienated from ourselves
- laborer's alienation from "species being" (i.e., from those broad features that signify one's membership in humankind)
  - what we are as individuals is tethered to higher sense of ourselves as members of human species
  - "species being"
  - if we are alienated from ourselves, it follows we are alienated from our own universal perfections, our "species being"
  - follows that we are alienated from one another

### Broader Implications of Marx's Theory of Alienation
- abolish unfree conditions that give rise to alienation in labor
- private property where laborer does not experience object of labor as belonging to him
- communism not only as abolition of private property, but conditions that give rise to alienation
  - transcendence from conditions of human self-estrangement
  - not merely adjustment in economic conditions: has economic component, but requires a restructuring of entire process of human self-realization, such that we can finally be free, experience ourselves in labor
- communism becomes a human utopia that resolves all problems of human history from the very beginning
  - the "riddle of history solved" that "*knows itself to be*" this solution
  - extraordinarily rhetorical phrase
  - resonates with Hegel's concluding lines in his *Phenomenology* when *Geist* reaches its final stage of knowing
  - just as spirit has passed through all stages of history
- to be fully natural is to be fully human
  - to be fully human is to have itself realized through concrete desires and requirements, or material existence
- how will communism transform us?
  - in the narrow sense premised on the abolition of private property
  - Marx understands this to result in far-reaching changes
    - *Aufhebung* of private property
  - remember that Marx is writing this as Engels is developing his critique in life in working class Manchester
    - as such, Marx very aware and concerned about degradation of working class life
- affirms humanism
- Marx's conception of communism is not that it merely abolishes property as such
  - transforms our property such that we come to truly own things for the first time
  - note Marx's language: "real appropriation" of the "human essence"
  - the word appropriation contains property
  - communism is what first permits us in human labor to affirm that the object of our labor are indeed ours
  - while bourgeois rules of ownership discarded, retains deeper sense of ownership in a much richer, philosophical sense

## Marx, *The German Ideology* (1845-46)

### An Unpublished Work (First Published 1932, Marx-Engels Institute, Moscow)
- quarrel with Hegel and the Young Hegelians
- "materialist" theory of history

### Beyond the Young Hegelians
- Young Hegelians: real chains of man are based in consciousness
- Left-Hegelian view insufficient
  - it alone will not set us free
- freedom not a matter of consciousness, but in material conditions

### Toward a Materialist Theory of History

### An Emergent Concept of Ideology and its Susceptibility to Reductive Interpretation
- tempted by crude reductionism
- susceptibility, or vulnerability, of Marx to reductionism reading
- misreading arises from crude understanding of Marx's theory of history
- of course it's true that for Marx, ideas are part of broader concatenation that make up our existence as social beings in material life
  - this does not mean that ideas should be dismissed as mere "reflexes" or "phantoms"
  - however, readers are tempted by this reductive claim
- nothing in Marx's 1844 manuscripts that promote such a reductive view
  - makes Marxism sound like objective science like that of nature
  - reflects Engels' penchant to think of Marxism in those terms
    - by his grave, termed him the Darwin of the social world
  - quasi-scientific view present in official Soviet ideology

## Concluding Remarks
- Marx, at his best, retains more humanist premises inherited from Hegel
- his revision of Hegelian history moved from history of spirit to history of humanity
  - from heavens to earth, from *Geist* to laboring individual
- incarnation of *Geist*, but not a reduction
- we are embodied creatures, but we are also embodied consciousnesses
- under labor, we realize deepest aspects of our consciousness too
- Marx's reprisal of a deeply Hegelian theme
- communism too, an event in the self-realization of human spirit
  - result in a state of self-satisfaction, higher kind of freedom that Hegel had anticipated at the end of the *Phenomenology*
  - acknowledgement of a consciousness of what we had achieved
  - this premise therefore makes it clear that to become fully realized as human beings we must know ourselves to be fully realized
  - in labor, be conscious of our laboring
  - the working class needs to know itself to be the agent of history
  - know what it is that it is setting out to achieve, and once it has achieved it, know what it has achieved
- in other words, Marx's founding premise in the 1844 manuscripts makes room for the human being to recognize itself, to be conscious of its own agency
  - in more practical terms, working class needs to reach a stage of popular self-understanding
  - cannot just non-reflectively go about its business
  - must come to understand own agency
  - therefore, must have revolutionary consciousness
  - helps explain Marx's participation in organized communism
  - deepest and philosophical reason to why he drafted *The Communist Manifesto*
