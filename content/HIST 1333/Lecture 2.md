+++
title = "Dissatisfactions of the Enlightenment"
date = 2020-02-02T18:09:26-05:00
series = "HIST 1333"
+++

## Introducing Hegel

### Philosophy at the Inflection Point
- not a romanticist, but rationalist with a post-Enlightenment spin
  - culminating figure of early modern philosophy

### Hegel's Life
- an academic philosopher to his bones
- 1806: Napoleon invades Jena, bringing upon Enlightenment values from above
  - Hegel flees
- he believed his phil was solution to the world's problems

## Critique of the Enlightenment

### Hegel's Metaphysical Attempt to Overcome the "Tornness" of the Cosmos
- Enlightenment had promised reason would rule the world
  - but this introduced a certain tornness
  - one understanding is theological
    - use certain Christian insights to make sense of tornness of being modern

### Hegel's Philosophical Attempt to Overcome the "Dualism" of the Enlightenment
- offering a solution to unsatisfactory dualisms that have been systematically impounded by Kant

### Hegel's Political Attempt to Overcome the "Division" in Human History
- a response to the French Revolution, namely, overcoming social antagonism, and the "Terror" of abstract right

## The Antinomies of Kant's Philosophy

### Reason's Right and Limits
- Kant studied not only phil but natural sciences
  - Newtonian physics as most respectable of modern science
  - Hume doubted it could be justified
  - Newtonian physics describes everything with necessity and absoluteness
    - if not, necessary connection falls apart
- so, Kant needed to justify the sciences
- (pen died)
- transcendental idealism
- when we think of non-temporal things, we are thinking about things that transcend the bounds of human reason
  - "God shaped the world" - we are committing an error
  - we can only reason about things within bounds of experience
  - the dialectic: the error when reason is used to reason about something not reasonable
  - dualism: possible experience with things in themselves
    - the latter can be thought but cannot be known; must be left to faith
- cannot pick out free will; the unknown world has(?) freedom
- this dialectic was unacceptable to Hegel

### The Ironies of Human Sociability
- how is it possible that we are all creatures pursuing our own desires which may clash with others and yet nonetheless we might be able to fathom progress in history, which requires some sort of unity?
- in the sheer fact of our individual desire, there is an almost providential level, that self-pursuit ends in greater good
- fortunate, but unintentional, out come when pursuing self-interest
  - unsocial sociability
- echoes Adam Smith
- humanity's progress conditioned upon us, left alone to self-interest, these "antagonistic" things, something negative can have beneficial effect
- must look at the negative to understand the world

### The Question of Human Progress
- different understandings of the news of history
- varieties
  - eudemonistic: too naturalistic
  - terroristic: no more convincing than former
  - aberitic: random flux, named after city where Democritus is from
- if we have free will, we can't conform to any of these
- how can one affirm progress in history without violating the spontaneity of human freedom?
- find out if our conduct has an effect, a sign
- Kant: look at Europe today
  - universal sympathy to France symbolizes a sign of affirmation for freedom

## Hegel's Dialectic as a Response to the Enlightenment
- Hegel did not oppose the achievements of the Enlightenment
- rejected Kant's philosophy as unsatisfactory
  - unworkable dualisms
  - the sense of dissatisfaction: recommends freedom but cannot promote action
- reaffirms reason but want to move beyond the effects of the Enlightenment since they are divisive
- rational: draw back from world with critical scrutiny and offer skeptical reflection
  - belief rational capacities
  - reason can't just leave us with sense of tornness
  - withdrawal and higher affirmation
- reason then is something higher than what Kant suggested
- Hegel's dialectic does not forgo reason's gains
- Herder: all peoples of the world may be gifted with their own idiosyncratic spirits, all represent tributaries flowing into universal happiness in the end
  - behind apparent disorder of human history that there is a basic human spirit that unites all of us
  - his conception of his national spirit coined as "expressivism"
  - has to blossom over time
  - earlier form of cultural nationalism
  - exp. model of reason inspired Hegel
  - used this model as a model for dialectical unfolding of reason in human experience
  - reason can be thought of as subject that can become what it is through development and self-externalization
