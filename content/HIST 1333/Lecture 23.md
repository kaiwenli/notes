+++
title = "The Reality of Social Suffering"
date = 2020-04-23T22:46:45-07:00
series = "HIST 1333"
+++

## Recapitulation: Exploitation as a Systemic Problem

### Objectivity or Normativity?
- exploitation: simply objective feature, or morally objectionable?
- necessary for capitalism
- did Marx think the extraction of surplus value was wrong?

### A Critique of Economic Reason
- in natural sciences, facts and values are separate
  - you wouldn't say gravity is praiseworthy or objectionable
- so to, science of society has same kind of objectivity while disregarding moral evaluation
  - present in neoclassical economics: externalities
- Marx understood this, and tried to understand capitalism objectivity
  - Marx understood economics' mode of objectivity because permitted economists to eternalize, naturalize, justify economic exploitation
  - objectivity serving ideological purposes
- *Capital* looks at exploitation as an objective fact apart from moral evaluation
  - but, Marx seems to be doing so in a strategic fashion
  - serves the purposes of immanent critique
  - takes up bourgeois point of view: replicates strategy of objective social science

### The Reality of Human Suffering in *Capital*
- emotional language, strong moral disapproval
- resorts to irony: moves between rhetoric of bourgeois objectivity, stern disapproval 
  - objective voice to convey disapproval
- especially present in "The Working Day"
  - description of exploitation, extraction of surplus value, as measured by the length of the working day

## The Reprisal of Moral Ethnography

### Marx Returns to Engels
- Engels, *The Condition of the Working Class in England* (1845)
- between documentation and moral outrage

### The Materialist Meaning of Morality
- biblical reference: sweat (and other impurities) dripping into bread
- Left-Hegelian gesture: materialist reading of theology
- irony: documenting actual working class conditions but also conveying moral outrage
  - reinforce sense of moral scandal
- also reinforcing need for change

### Suffering as Moral Scandal and Political Motive
- length of working day = measure of exploitation
- not only interpret the world, but change it
  - working class solidarity and agitation
- not just venting, but presenting a case in which moral outrage became political motivation in transforming working class conditions

## Capitalism Against Itself: Marx's Crisis Theory

### Exploitation is Systemic
- moral objection to capitalism irrelevant
- sentimentality irrelevant
- capitalist (person) intention irrelevant
- system functions objectively only through exploitation

### The Concentration of Capital and its Dialectic
- concentration of capital
- capitalists grow in wealth
- capitalists diminish in number
- competition among capitalist firms diminishes
  - less choice for laborers as to where they work

### The Socialization of Labor and its Dialectic
- industrialization: dissolution of local identities
  - skilled artisanal production fades away
- workers increasingly proletarianized
  - internationalization of labor
- conditions become more difficult, though creates common sense of identity
  - possibility of increased solidarity

### The Final Crisis
- dramatic difference between small number of wealthy and mass of poor
- think about exploitation as the initial thing that generates private property in the first place
- system of private property ultimately works to undermine itself
- from private property to socialized property
  - appropriation of the means of production by the proletariat
- the "negation of negation"

## Marx's *Capital*: Some Concluding Remarks

### Marx's Final Synthesis
- can one actually get away from normative evaluation?
  - relevance of "Critique of the Gotha Program"
- prof's view: Marx does think exploitation is wrong
  - not just evaluation
  - use of irony
  - convey his moral judgment

### The Persistence of Negativity
- show irrationality of capitalism on its own terms
  - demonstrate how it generates its own negativities
- witness a negation of negation
- modes of criticism: moral and systems-functional

### The Actual and the Irrational
- how much did Marx break with Hegel?
  - prof's view: Althusser's representation of Marx's break with Hegel too extravagant
- Hegel: idealist science
  - absolute knowing, spirit gazes upon the world with self-satisfaction
- Marx: materialist science
  - dissatisfied world, material conditions of the world are ones that are not in harmony
  - remains good Left-Hegelian: not the rationality of actual, but the irrationality of actual
- nonetheless, faithful to Hegel: final negation
  - could be a final *Aufhebung*: "negation of the negation" bring the world into harmony

### What Could Defer the Final Crisis
- the Hegelian premise: the true is the "whole"
  - humanity can reach self-satisfaction
- Hegelian premise deferred for Marx into a communist future when the whole world will be at peace
- Marx is not naive about when final synthesis will emerge
  - Marx's crisis theory took seriously the possibility that crisis could be deferred
  - e.g. imperialism, outsourcing labor and exploitation
- Marx committed to thought that capitalism was a global system
  - could defer crises as long as global system has not developed fully
  - controversial reflections about the necessity of imperialism
