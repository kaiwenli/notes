+++
title = "Introduction — the Path from Hegel to Marx"
date = 2020-02-02T18:08:59-05:00
series = "HIST 1333"
+++

## From Hegel to Marx

### The Idea of Freedom in German Idealism: An Emancipatory Philosophy
- Hegel: freedom <-> phil
  - culmination of German idealism beginning with Kant
- Kant: primacy of practical reason
  - sense that we as humans are agents, unihibited own moral ideals
  - exemplify autonomy
- Hegel: highest kind of freedom is larger, *Geist*
- beginning of new tradition
- right and left-Hegelians: Hegel's ideals (realization of freedom) realized/not, respectively

### Continuity or Discontinuity?
- young Marx: conversion experience
  - sober critic of capital? abandoned Hegel?
- Althusser: epistemological abrupt departure by mature Marx
  - probelms of society are economic, deeper than humanagency
  - prof's view: this is not possible; Hegel remained important to Marx

- continuity
  - historical thinker: communism as the "riddle of history solved"
  - one deep central knot in human suffering: class conflict

### Continuity and Controversies
- Lukacs: Hegelian themes in Marx's work
  - focused on "reification"
  - what happens to human consciousness under capitalism
    - cease to think of ourselves as agents, but rather objects
- Marx: Paris Manuscripts
  - early enthusiasm for Hegel
  - prescribe working class with "alienation"

- "Diamat"
  - science of society
  - ideological warrant for authoritarian character of USSR

- Marxist "humanism"
  - "Western Marxism"

## Concluding Remarks
- Hegel had confidence in reationality
- human experience: growing rationality
- reason the defining characteristic of Spirit
- what's *Geist*? deflationary/inflationary views
