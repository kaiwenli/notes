+++
title = "Overcoming Immediacy: Hegel's Argument against Brute Empiricism"
date = 2020-02-11T14:12:33-05:00
series = "HIST 1333"
+++

## Hegel's Phenomenology of Spirit (1807):

### Spirit's Path of Education, or, Spirit's *Bildungsroman*
- not only a history of philosophy; an argument of how each stage leads to the next
- each stage linked necessarily to the next
- brute empiricism, then other stages crowd in
  - not clear how all these episodes are linked together
- paradigmatic episodes that tell us something about spirit and its progress
  - weak affinity: "transitions weren't Hegel's strong suit"
- conclusion: spirit having reached self-confidence, rationality justified
  - full "actuality," cannot collapse when subjected to rational scrutiny
  - consciousness will not feel like it has things pushing back at it, no obstruction to freedom

### Demonstrating Dialectical Reason
- Cartesian doubt: put aside traditions, build up edified knowledge in rational fashion
  - only something that withstands doubt can be justified
- begin with most rudimentary truths
- senses yield to skepticism (e.g. optical illusion, example of straight stick in water)
- Hegel: puts pressure on sensuous-certainty
  - intrinsically unstable
  - overcomes itself
  - something more sophisticated; weave individual sensual self to social world, arrive at modern satisfaction of public life
- not just old problem; dialectic writ large
  - model of Hegel's immanent critique, determinate negation

## The Examination of Sensuous Certainty

### The Nature of Sensuous Certainty
- immediate confrontation: simple
- starts with atomic sensuous-certainty
  - gets through "compositional development"
  - present/culminated in spirit, the whole "spirit"
- can I trust my senses? is it a reliable bedrock?
- can sensuous-certainty establish the kind of claims it makes?
- the "this", "here", "now": name something that seems indubitable
  - what is this "now"?
  - the minute you say now it slips away into the past
  - left to affirm something that is no longer now

### Putting Skeptical Pressure on Sensuous Immediacy
- what is the "now"?
  - seems like a little trick
  - sensuous-certainty doesn't have the naivete it supposes
  - now in the past requires a contrast: now and then
  - think about a higher standard that connects them
  - e.g. time, a universal
    - the particulars are subsumed
    - recourse to universal concept
    - identify the now even when it has slipped away
  - what claims to be immediate/brutely here, involves mediation/concepts
    - immediacy requires mediation
  - what happens to certainty when you turn away from tree to house?
    - certainty persists, why? still sure that tree is there
    - something conceptual

### Mediation is the Truth of Immediacy
- when we put pressure on sensuous-certainty, we are forced out to sophisticated concepts

## What Has This Thought Experiment Taught Us?

### Immediacy and its Dialectic
- certitude is not immediacy but involves mediation
- this thought experiment and its consequences anticipates the entire process of the dialectic

### Attempts to Fasten on Merely the Particular Must Yield to the Universal
- particularity is shot through with universality and conceptuality
- if you think empiricism is sufficient, you need to start again
- transformation of season: Elusinian mysteries
  - ceremonies: three fold structure of descent, search, ascent
  - the same structure
  - referring to the sacrament
  - things are not what they seem to be; things are not only what they seem
  - Christianity transformations of blood/wine in communion
  - sensuous-certainty does not tell you truth
  - wine not just wine, bred not just bred

### This Thought Experiment as Model in Miniature for the Entire Dialectic
- recapitulation of old cult requires concepts
- conventional epistemology -> humans as concept wielding beings
- follow clever gestures
- into the social
- if we have to have concepts to have certainty, in being certain, we have to be certain about consciousness
  - concepts exist in mind
  - certitude about our own mindedness
  - consciousness can only be indubitable if it knows itself to be conscious

## Toward the Social: Some Introductory Remarks on the Dialectic of "Master" and "Slave"

### Conceptuality and Consciousness
- to be certain of consciousness, self-consciousness
- when I see tree, refer to treeness
  - affirming something conceptual is not in tree but rather in housing of concepts; self-consciousness
  - possible only because of own conceptual activity

### What Does Self-Consciousness Demand?
- if external world demands sensuous-certainty...
- to be self-conscious, mind needs to examine mental activity and affirm it
  - can't do it in own head
  - self-affirmation is just solipsism
- the affirmation of consciousness must be affirmation *by another consciousness*
- gets Hegel into social world

### Thus The Attempt to Arrive at Self-Certitude Necessarily Involves the Other
- Hegel's turn from individual epistemology to social epistemology
- "what can I know" to "what can we know"
- primitive encounter between two consciousnesses
- mastery and servitude
  - each consciousness trying to do same thing, affirm consciousness
  - each of these two consciousnesses merely trying to gain recognition for itself as consciousness
- involves Hobbesian state of nature
  - doesn't work quite the way Hobbes imagined
