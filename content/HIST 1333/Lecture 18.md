+++
title = "Marx's Role in the Communist Movement"
date = 2020-04-07T22:23:27-07:00
series = "HIST 1333"
+++

## Introductory Remarks: Marx as Theoretician and Strategist

### Marx in London, 1849-1883
- focused on scholarship: political economy

### The History of Communism in Europe
- what did communism mean to Marx?
- communism: elaborate tradition with many different strings
- French Utopian Socialism
  - Fourier: utopian communes; inspired them to pop up
  - Saint-Simon: alternative societies
  - Marx a strong critic; akin to summer camps that don't change the world 
- The League of the Just
  - shut down in Germany
- International Workingmen's Association
  - umbrella group with many strong personalities

### The IWA and Marx
- not a centralized organization
  - mostly in England
  - trade unions
- on continent, mutual benefit societies, educational groups, illegal clubs (since communism illegal)
- IWA holds "general congresses"
- Marx distant from organizational leadership
  - his opinion was that position should go to a worker

## Marx, "Inaugural Address of the Working Men's International Association" (1864)

### The First Meeting of the International Working Men's Association, retrospectively known as the "First International"
- responding to severe economic distress around him

### Growing Poverty and Economic Inequality in Great Britain, 1848-1864
- dramatic increase in industry and commerce
- decline in worker wealth and health
- degradation of working poor
- calling attention to scandalous contrast between capitalist class and working poor

### Salutary Gains and the Importance of Reform
- in favor of reforms: Ten Hours Act limited work to 10 hrs (weekdays) and 8 hrs (Saturdays) for women and children ages 13-18
  - rules weren't carefully enforced
- nevertheless grants importance of such bill
- other utopian socialist movements have also contributed
  - cooperative movement
  - uplifting rhetoric

### Internationalism against Nationalism
- Marx sees reactionary Russia as terrible serpent
  - fearful of imperialist ambitions
- use of moral language
  - simple laws of morals and justice that ought to govern humanity
  - as a matter of theory, Marx develops allergy to that sort of moral appeal

## Marx as a Critic of the Communist Movement: "Critique of the Gotha Program" (1875)

### Historical Background: The Debate with Ferdinand Lassalle (1825-1864)
- Lassalle wanted to get Bismarck on his program
  - in unifying Germany, Bismarck wanted support of nationalist liberals but also wanted to break their power
  - plan was to expand suffrage to workers, since bourgeoisie did not want them to enjoy political power
  - Lassalle recognized this, strategic alliance with Bismarck
- collusion with Bismarck was a scandal to Marx
  - opposed nationalist orientation
- Lassalle, according to Marx, misunderstood two things about communism
  - national framework: international solidarity, nationalism a bourgeois distraction
  - statism: state "independent entity"; state is an instrument of ruling class, must overcome bourgeois state

### Marx's Critique of the Program
- did not like Lassalle's influences
- detested moralistic language
  - big contrast with the "Inaugural Address"
- hostile to program's "fair" distribution
  - bourgeois notions
  - what do you mean by "fair"? capitalism operates the way it's supposed to operate
- "Right" can "*never be higher* than the economic structure of society and the cultural development conditioned thereby."
  - does Marx mean this?
- Gotha program relies on notions of fairness and morality that are irrelevant to communism

### Theoretical Implications of Marx's "Critique"
- epiphenomenalism, appearances are simply floating above deeper real forces of economic structure
- Marx's normative epiphenomenalism: moral or political concepts only expressions of dominant economic/cultural era?
  - difficult situation: can we secure any normative leverage against current order with concepts that belong to that order?
  - if Marx is right, then concepts of fairness, justice, morality, can't gain critical leverage against that order 
  - concepts only affirm that order and its proper function
- prone to write in those extreme terms
  - moments of negativity? if he is wholly committed to this kind of epiphenomenalism, then he has abandoned his Hegelian education
  - Hegel: tarry with negative, order turns over itself
  - epiphenomenalism denies room for self-generating negativity
  - how would critique be an immanent practice?
- reflexivity requirement: social theory must afford room for itself
- if Marx is committed to epiphenomenalism, then he can't account for his own theory
- on the other hand, epiphenomenalism as a piece of evidence for Hegelianism?
  - Hegel's philosophy was motivated by a metaphysics of expressivism, of the expressive self
  - Marx replaced spirit with materialist life
  - materialist life expresses certain values, norms, culture, as if spirit manifests itself in reality
  - this is an enduring Hegelianism then?
- though not in this text, can understand expression of negativities/tensions this way: all dominant notions of justice or right express the interests of the dominant class
  - subordinate concepts that resist or contest the values of the dominant class
  - when Marx was more careful, this seems to be his view

## Concluding Remarks
- read critique as contestation with other intellectuals
- formulate critique of bourgeois ideology
- moral ideas of a modern capitalist society that support capitalist society and justify the way it works
- not only an array of political and moral ideas that justify our economic arrangements
  - bourgeois society given rise to elaborate discipline of modern economic science
  - bourgeois political economy: Smith, Ricardo develop objective understanding of economic realm
  - must understand bourgeoisie sought to justify its own economic arrangements
  - those economic arrangements understood as a matter of nature
- took up a dedicated study of modern bourgeois economics
  - description of modern economic reality
  - ideological justification for that reality
