+++
title = "Master and Slave, or, The Dialectic of Recognition"
date = 2020-02-13T14:18:16-05:00
series = "HIST 1333"
+++

## Hegel's Phenomenology of Spirit (1807): A *Bildungsroman* of Consciousness or "Geist"

### The Learning Process So Far: Reason is Social
- sensuous-certainty not accurate to its own claims
  - shot through with concepts
- concepts belong to mind
  - in order to be certain of consciousness, withdrawal, confirmation; self-consciousness
- self-consciousness cannot be solipsistic
  - confirm from place of otherness
  - how it manifests itself out there
  - observing own activity
- Hegel's problem with Kant: recognize these walls as mine but I've walled myself in
  - does not permit self-consciousness; no objectivity
  - world that I'm looking at isn't just extension of own mind
- imagine reason as agency
  - implicit in Kant
  - Hegel: Kant doesn't go far enough
  - reason acting in the realm of life
  - reason taking up tools, instrumental reason
    - does not give certainty of consciousness
    - tools are just extensions of me, lose sense of object independence, collapse back into own understanding as agent
  - consuming things: those things vanish into me, lose object independence
- to have consciousness of self requires something special
  - persistence of something independent of me, acknowledge that I am conscious
  - only thing that can do that: recognition by another consciousness

### The Movement from Individual into Social Epistemology
- reason as something special
- once Hegel makes aforementioned argument, leap from individual into social connection

## The Thought Experiment of Primitive Sociality

### What Would be the Most Basic Mode of Social Encounter?
- state-of-nature
  - Rousseau: counterfactual
  - if there were no inherited structures, what would things look like?
  - all normativity
  - state-of-war
- most basic sense of social agreement by which we orient ourselves as humans is missing
- Leviathan: no account of time
  - without rudimentary agreement with others, no standards for truth
  - social epistemology: knowledge requires social trust
- Hegel: consciousness comes into world with brute recognition
  - how do I know I am a self?
  - gain recognition

### The Philosophical Aims of a Social Encounter
- recognition of consciousness by another consciousness
- no standards for mutuality
- radical asymmetry: no other regard for other
- desire for recognition leads to struggle
  - brute encounter without norms

### Consequences of the Struggle for Recognition: Playing out the Experiment
- death
  - does not satisfy need for recognition
- domination and submission
  - *Herr* and *Knecht*
- a philosophical analysis of positions, not an analysis of persons
  - analyze dialectic between master and slave

## The Dialectic of Lordship (*Herrschaft*) and Bondage (*Knechtschaft*)

### What Lordship Achieves: The Failure of Domination
- desire for recognition
- conscious (a) requires recognition by another consciousness (b)
- but (b) consciousness, to be consciousness, must also be recognized as consciousness
- master (a) does not recognize slave's consciousness (b) *as* consciousness
- hence the desire of the master (a) for recognition by an identified *consciousness* (b) must end in frustration

### What Bondage Achieves: Fear of Death and the Promise of Labor
- truth found in position of servitude
- having chosen to submit, experience fear of death
  - shaken to core of not being at all
  - choose instead to live
  - in experience of fear, radical contingency about everything
- realization: mastery does not mean anything
  - absolute master is death
  - other mastery is ultimately empty
  - own servitude is contingent
- slave has opted only to submit
  - "inward turn," experience own negativity
    - work, constantly negate, without achieving any formal satisfaction
    - slave consciousness experiences this radical fear of death
    - engage in negating external world
    - e.g. produce food for master
      - does not enjoy fruits of labor
      - desire held in check
      - constant activity itself without immediate and ultimately unsatisfactory satisfaction of eating
- in physical work, we experience self-realization of our consciousness, "mind of his own"

## Hegel's Lessons from the Master-Slave Dialectic

### The Significance of Work as Self-Expressive Activity
- work significant since it is self-expressive activity
  - educative and self-cultivating
- consciousness begins to realize and become what it is
- misleading: something already inside, just need to deposit outside; mere externalization
- instead, self-realization become what I am by being external to myself
  - anthropogenesis

### The Failure of Domination, or Asymmetrical Power
- domination is self-defeating, irrational
- not adequate or enduring form of sociality
- asymmetrical social relations cannot persist

### The Primacy of the Negative, or, Suffering and the Path to Freedom
- subordinate position is the one that points to freedom
  - promises higher freedom
  - negative is most instructive
- suffering as redemptive, "the meek shall inherit the earth"
  - anthropological? Christology?
  - instability of dominance and intolerability of subordinance
  - humans reject and ultimately get beyond suffering

### From the Critique of Domination to the Critique of Revolution
- if asymmetry does not work, needs rationality and symmetry
- mediation
- power can't be all one-sided
  - system of relation
  - anticipates Hegel's political philosophy
- society needs to exhibit mediation of power
- rational functioning of the whole
- analysis of French Revolution
