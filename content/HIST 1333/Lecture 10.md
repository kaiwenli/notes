+++
title = "Hegel and the State"
date = 2020-02-27T14:10:29-05:00
series = "HIST 1333"
+++

## Hegel's Lectures on Political Philosophy: A Recapitulation

### Reason's Demand for Objective Freedom
- institutions we obey must be worthy of our assent as rational beings
  - not only rationally justifiable, but rationally justified
- not enough to think of self as subjectively free
  - test of rationality something larger
  - social world embodies our rational ideas
- freedom: social world embodying our ideals
- abstract right, or individual morality, insufficient to withstand further going rational scrutiny
- *Sittlichkeit*, "Ethical Life"

### The Contributions of the Family and Civil Society: Immediacy and Mediation
- family: emotion, love, shaped as self
- civil society: economy, object for rational understanding, break free of interiority, work without own consciousness of it to contribute to the universal
  - has not *knowingly* achieved universality

### Family and Civil Society Must Find a Higher, Mediated Unity
- something about civil society that is inadequate
- civil society: not directed at universal intentionally
  - freedom not complete if not willing the universaly consciously
- something lacking about individualism

## Hegel on the Limits of Liberal Individualism

### The Truth of Liberal Individualism
- subjective freedom is necessary if we are to be free
  - express ourselves as autonomous moral agents
- institutions around us must embody our freedom: freedom must be objective
- not enough to be engaging in liberal, individual transactions
- private moral virtue is partly false: you're not moral if you live in immoral world

### The Inadequacy of Liberal Individualism and the Requirement of Objective Freedom
- how does objective freedom furnish content and objectivity to subjective freedom?
- not really a person until recognition as one
- moral conduct remains merely formal
- gains content when realized in social life
- we must not only subscribe to laws that we give to ourselves but subscribe to laws that are recognized as rational by the totality of our social world

## Hegel and the Tradition of Civic Republicanism

### Civic Republicanism as a Broad Trend in Political Theory
- the human being realizes its highest truth only in and through its political life
- accusation of totalitarianism
  - tradition of "positive liberty"
  - Isaiah Berlin: freedom from (negative), shaped to be free, mold character and direct you to proper goals (positive)
  - difficult to overcome this misconception but at this point it's overcome
    - misleading to think Hegel would even warrant statism

### The Aristotelian Background: Political Life as our *Telos*
- Aristotle on human nature: "*Man is by nature a political animal*"
  - state in our political life is theater in which we realizes *telos*
- human has as its final purpose our collective good
  - experience the good life in common with others
  - to be a human being at all is to come out of unformed privacy and to come into political realm, realize common good and thus *telos*
- state helps us realize our humanity in concert with others

### Hegel's Neo-Aristotelian Model of the Political as the Realization of our *Telos*
- realize innermost nature
- whole suite of social institutions, but particularly state, that realize own purposes
- metaphysical requirement for being free as an individual
- the state as the dialectical reconciliation between subjective freedom and objective freedom

## Hegel's Model of the State

### The Components of a Constitutional Monarchy
- best embodiment of freedom available
- Prussian monarch plays no greater role than Queen Elizabeth plays in England today
  - what that will wills is laid out in law
  - thus, monarchy not that irrational

### The Role of the Estates
- see nobility as people who are immune from modern, rapacious, attitudes of merchants class
  - benevolent responsibility since above quest for profit
- analogies to US Senate

### The Significance of Public Opinion
- granting that in modern rational polity, everyone at least potentially contribute to what's rationally defensible laws
- must be that when individuals look at laws, they feel as if their ongoing deliberations grant legitimacy on practices
- but people also have bad opinions and say wrong things about their polity

### The Questions of Criticism, Reform, and Resistance
- how much room does Hegel provide to resist our state?
- rational endorsement: simply finding oneself in political situation, affirm what we've been doing
- Neuhouser
- rational criticism: step back, suspend commitment, doubts about way of doing things in politics
  - endorse
  - or, realize not up to par to rationality
  - what do you do?
    - internal reform: draw upon state's own articulated norms, state doesn't measure up to them, makes sure harmonizes
    - radical criticism: what state should do but does not endorse, people think should be rational norm, an exotic norm
    - no room for this?
    - might withdraw from endorsement entirely and engage in revolution
    - where to get norms if not already in history and society?
    - difficult to imagine norms that are exotic due to historical understanding of human being
- Marx: make use of stated claims and actuality, immanent critiques, lever to overturn own society

## The State in History: Some Concluding Remarks

### The Plurality of Nation-States as Manifestation of Spirit in its Diversity
- state becomes actor, confronting other states
- representation of nations and various spirits, position of plurality
- human diversity
- contribute to collective narrative of humanity

### World History as a Court of Judgement
- court of judgement: contributes something and falls away
- arena of not only life but death
- what they contribute: one chapter in embodiment of freedom
- subsequently defeated, even violently
