+++
title = "From Religion to Absolute Knowing"
date = 2020-02-20T13:30:41-05:00
series = "HIST 1333"
+++

## Hegel's *Phenomenology* as a Vindication of Reason

### The Historical Adventure of *Spirit*
- history of spirit ~= history of philosophy: philosophy is "its own time comprehended in thought"
  - philosophy is an explanation of problems and solutions that are given to us at particular junctions in history
  - task of philosophy to give rational articulation to social forms that we inhabit; make rationality explicit the way we live
- historicism: look at world as bounded in particular epochs
- in 19th century: combined with developmentalism, an idea's emergence and perfection over time
  - not a natural marriage
- spirit, once it has achieved what it wants to be, can engage in retrospective journey and see that each stage contributed to what it is
- absolute knowing as the completion of spirit and its claims? or floor for spirit to evolve further?
  - is absolute knowing really absolute?
- deflationary reading: philosophically respectful
- professor's metaphysical reading: metaphysical grandiose claims historically correct
- closure: night, end of a day

### Hegel's Self-Vindication as a Philosopher
- historicizes his own philosophy and writes it in his philosophy as the culmination of philosophy
- self-assurance or rational arrogance?

### What Sort of Self-Knowledge does Spirit Require?
- self-knowledge must be *rational and philosophical*
- spirit must overcome opacities to the mind
  - overcome religion
  - Left-Hegelians took up this task of philosophical critique of religion
- overcoming retains, so what remains after overcoming religion?

## Hegel's Critique of Religion

### Religion as "Representational Thinking"
- Hegel: trained in theology
- desire to clear away quasi-magical thoughts from religion
- in Protestantism, Catholicism have misinterpreted theology such as through images
  - more rational understanding of what Christianity provides
- Christianity at its best is giving us glimpse of what spirit is, but given to us in representational schemes
  - religion tells us spirit lives in metaphysical elsewhere
  - demystify it: this-worldly, rational form
- thus, fulfillment of Protestantism's goals

### From Belief to Knowledge
- faith true in that faith in spirit, false in that its faith beyond spirit
- overcoming faith: dialectical corrective, "alieness"
  - critique of religion will reclaim content of religion, but as reason's own property
- trying to fulfill Enlightenment critique of religion

### From "Religion" to "Science" (*Wissenschaft*)
- appropriate content of religion into rationality
  - Hegel: there is no beyond which is what Kant thinks
- spirit needed to externalize itself in religion but now needs to re-appropriate itself through rational reflection

## Absolute Knowing and the Self-Satisfaction of Spirit: Process and Recollection

### The Self-Satisfaction of Spirit as a Process and in its Rollection
- process and result
- process: of consummation
- result: spirit steps back and looks at what it has achieved and goes through recollection
  - remembering something involves bringing it back into one self
- no more external negativity
- in stage of recollection, reclaims own past

### The Modified Quotation from Friedrich Schiller, "Die Freundschaft," ("Friendship"), 1782
- as spirit draws back, world seems to mirror its own power and rationality
- metaphysical narcissism: sees itself in its own creation
- "Golgotha": both spirit's passion and resurrection
  - having self-relinquished itself into time to arrive at satisfaction

## Has the Dialectic of *Spirit* Run its Course?

### Why is Absolute Knowing Authoritative?
- deflationary: there could be further manifestations of spirit with same satisfaction
- prof: spirit cannot arrive at absolute knowing if there were negativity remaining

### Absolute Knowing Overcomes but also Incorporates Religion as Truth rather than as Representation
- truth of religion not what it claimed: not god's, but reason's, self-fulfillment
- fulfillment of reason a state of finality
- "the *identity* of identity and non-identity"

## Hegel's Political Philosophy: Introductory Remarks

### A Description of the Political Present, a Normative Construction of What Reason Demands in Political Life?
- actual or ideal?

### A Dialectical Philosophy Requires both of these at Once
- the rational is the actual, and the actual is the rational
- bring these two modes together
