+++
title = "The Authoritarian Reaction"
date = 2020-04-02T01:02:14-07:00
series = "HIST 1333"
+++

## Europe at Mid-Century (1848-1851): From Revolution to Reaction

### The Historical Context
- Age of Revolution: 1789 to mid-18th century
- liberal democratic agitation throughout central Europe
  - general liberal nationalist orientation
  - expanded suffrage
  - written constitutions to check monarchs' power
- revolutions composite events
  - educated, bourgeois group of liberal-minded reformers and nationalists
  - radical, sometimes socialist-minded agitators and crowds
- Germany: the "March" revolution
  - National Assembly drafts constitution
  - Friedrich Wilhelm IV (Prussia) rejects it
  - Assembly falls apart, people flee to US: "the Forty-Eighters"
- France: February Revolution
  - crowd-driven phenomenon
  - monarchy once again dismantled
  - French declare a republic: December 1848
  - new president: Louis Napoleon Bonaparte
  - refuses to leave office and in a coup d'etat becomes First Consul
  - November 1852: declares himself Emperor Napoleon III

### Marx from Cologne to London
- Marx and Engels watched the events from afar
- Marx spends rest of his life in London

## Marx's "The Eighteenth Brumaire of Louis Bonaparte"

### Genesis and Publication of the Essay
- from title, Marx suggests this mid-18th century revolution a bad reincarnation of the original French revolution

### General Remarks on the Essay
- slackening in Marx's earlier confidence in revolution  
  - in contrast to *Manifesto* of imminent revolution
- post-mortem on mid-century revolution
  - rueful, embittered tone
  - from "tragedy" to "farce"
- ironic, negative dialectic
  - revolutionary energy turned sour
  - using Hegelian language: cunning of unreason
  - no longer confident that masses are agent of progress
  - still committed to revolution but a qualified commitment

## The Argument of the Essay

### The General Conception of Revolution as Reprisal of Old Forms
- sometimes, revolution does not introduce wholly new principles to the world
  - a reprisal of old forms
- rejoinder to Hegel (who never really said that history happens twice): tragedy to farce
- revolutions wear costumes of the past
  - past weighs too heavily on the present

### The Betrayal of Bourgeois Revolutionary Ambitions
- 1848 collapses and betrays itself
  - bourgeois opt for Louis Napoleon Bonaparte
- bourgeoisie was the rising force in history
  - state apparatus grows
  - centralization
- Napoleon's nephew makes use of this new strengthened state
  - counter-revolutionary intent of universal suffrage
  - peasants who want fabled Napoleon of the past
  - form of democracy to overturn it
  - parody of what revolution should be
- "lumpen-proletariat": lacks revolutionary consciousness
- new ground for authority: peasants and "lumpen-proletariat"

### An Irony in the History of Revolutions
- modernization and democratization of state, ironically, prepared the terrain for reaction
- state has been centralizing, bigger bureaucracy, modern military
- all these transformations that were supposed to bring bourgeoisie to power disabled them
- state usually instrument of ruling class
  - for the first time, wholly independent power
  - overthrew apparent despotism, but it was burrowed and it overthrew progress
  - an ironic denouement
- bourgeoisie has dug its own grave but not in the way the *Manifesto* has predicted
  - preparing a state machinery that could enable a second Bonaparte to come to power
  - state stands above and over all of civil society

### Proletarian Revolution Would Have to Abandon all Historical Precedents
- end to tradition and precedent: Marx's turn against the logic of determinate negation?
- stunning shift
  - continuities in history end up reintroducing old ghosts of tyranny past
  - a set of social conditions that give rise to a truly revolutionary class
- a genuine communist revolution requires first, a revolution in social conditions
  - come to fruition
  - peasantry will not want communist freedom; they long for (farcical reincarnation of) king
- become truly self-conscious and urban proletariat

## Concluding Remarks

### The Requirement of Thoroughgoing Social Modernization
- betrayal of revolution in mid-century: important lesson
- ultimate requirement: social conditions must be fully modern
- seize power in proper way

### The Requirement of Revolutionary Consciousness
- working class needed to understand themselves as a revolutionary agent in history
  - proletariat needed to be organized into a new political party
  - took up modest role in structuring a new communist movement in Europe
