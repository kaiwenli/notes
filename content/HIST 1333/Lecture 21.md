+++
title = "Commodity Fetishism"
date = 2020-04-17T01:04:57-07:00
series = "HIST 1333"
+++

## Recapitulation: Marx's *Capital* and its Structure

### Capital as Totality: Economy, Ideology, and Form of Life
- economic laws that govern the system of exchange and labor
- ideology: formal set of principles that is used to explain why the system we have is the right/rational/natural one
  - Smith to Ricardo: exercise in ideology (according to Marx), illusion of necessity
- form of life: distinctive way of being human
  - developed in Europe in early modern and modern eras
  - historically specific
  - historically dynamic
  - open to historical change

### From Exchange to Production
- begins with realm of exchange
- Smith: naturally inclined to barter
- metaphors of organism
  - not an invariant realm like physics
- move into realm of labor, capital, exploitation
- exploitation of labor as secret of commodity

### Marx's Dialectical Method of Criticism
- this method exhibits Marx's debt to Hegel
- move from realm of appearance to realm of essence
- Hegel puts skeptical pressure of "sensuous certainty" in the *Phenomenology*
  - claim to immediacy was not in fact self-sufficient
  - push beyond illusion of immediacy
- Marx traces out the same path here
  - surface to depth
- from mere commodities to the social relations that make them possible
- read Marx's *Capital* as the work of capital itself like Hegel's *Geist*
- Marx's own self-understanding of how he inverts Hegel is more or less correct, bringing the dialectic down to earth

## The Fetishism of Commodities

### The Realm of Exchange and its Illusion of Autonomy
- alluded to opening gesture in Hegel's *Phenomenology*
- what Marx wants to do: focus attention on commodity, what is this strange thing
  - its social origin
  - something about the way in which bourgeois society is organized, way we conduct ourselves in our economic life, such that the social origin of commodities is obscured
- losing sight of this is built into our economic conduct

### The Seeming Autonomy of the Commodity and its Actual Social Origins
- turns out that commodity's value refers beyond itself
- Marx's analogy to Hegel's dialectic: from immediacy to mediation, from appearance to essence
- from brute value as an object on toward the greater circuits of capital and labor that lend that commodity value
- from brute immediacy into society
- Marx charts the same path as Hegel's transition into writing about politics, human history
  - if we understand commodity correctly, forced to abandon that illusion of immediacy
  - move into larger field of social relations that make the commodity possible

### The Analogy to Religion
- Marx not saying that we're under a religious spell
  - commodity is not evidence of religion
  - instead, evidence that it's *like* religion, *like* a secular religion
  - is *not* a religion
  - prof: people get overly clever about this passage
- deploys metaphor of fetish
  - treat the commodity in much the same way a "primitive" religious person treats their fetish
  - (Marx is deploying colonial archive of language in a pejorative understanding of non-monotheist religions)
- fetish: a god carved from wood and worshipped
  - object that one worships despite that one has created it oneself
  - origins purely human
- Marx's indulgence in colonial understanding of primitive religion
- Marx is doing this in order to immediately dismantle the arrogance of the modern bourgeois who believes himself to be beyond this primitive understanding of the world
  - bourgeois society exhibits similar error
- analogous to mistake Feuerbach comments on
  - investing human essence in object
  - independent life over us
- Marx is moving beyond Feuerbach
  - to overcome quasi-religious error of commodity fetishism, insufficient to simply criticize
  - criticism to social change
  - what motivated the error in the first place?
  - no amount of reinterpretation can undo commodity fetishism, as that is something we reenact

## The Commodity as "Social Hieroglyphic"

### The Commodity and the Appearance of Value in Exchange
- actual source of value in commodity does not adhere in commodity itself
- insufficient to understand that we've committed cognitive error
  - this would be Feuerbachian
- this error intrinsic to bourgeois life
  - would need to change social structure that gives rise to the error
- "social hieroglyphic"
  - the products of our labor are, in their origin, material receptacles of human labor
  - when exchanging them, we see them as material receptacles of homogeneous human labor
  - objects have become exchangeable
  - whenever we equate as value different products, we equate different kinds of labor expended upon them
  - further obscure origin of a commodity's value
  - further intensify the fetishism of commodities
  - obscure human labor
- decipher the hieroglyphic
  - try to move away from illusion of commodity form and understand deeper social relations that make the commodity what it is, what lends object its value

### Deciphering the Social Hieroglyphic
- labor power
- commodities as exchanged reinforce the sense that differentiated labor power is all of one kind

### The "Scientific" Discovery of Labor as the Source of Value
- bourgeois political economy mystified socially produced value
- left with fetishism
- error of consciousness: air remains unaltered even if you have scientific knowledge of atmosphere's elements
- reflective of Marx's critique of Left-Hegelians: criticism is not sufficient
  - right to develop scientific critique
  - however, not adequate

## Concluding Remarks

### What Does the Analysis of Commodity Fetishism Teach Us?
- illusory realm of apparently independent commodities
- exchange is not a self-sufficient realm
- commodity: expression of human labor
- what's behind realm of appearance

### From Commodity to Society, From Appearance to Essence, From Exchange to Production

### From Exchange to Labor
- labor not only source of value
- labor itself is a commodity
- somewhat provocative sounding
  - seems like it's objectifying human beings
  - but, in a society animated by capital, those who work are treated as commodities
  - they are bought, paid for work
  - in that sense, they are commodities

### Toward an Analysis of Exploitation
- labor a special commodity: produce more value than it costs
- exploitation is a measurable phenomenon using economic algebra
- labor power peculiar: paying for the reproduction of the laborer's life
  - food, rent, literal reproduction
  - laborer produces more value than he costs
  - this is "surplus value"
