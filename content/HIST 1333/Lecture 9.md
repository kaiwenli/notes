+++
title = "Hegel's Philosophy of Right: Morality and Ethical Life"
date = 2020-02-25T14:17:50-05:00
series = "HIST 1333"
+++

## Introductory Remarks: Hegel's Lectures on Political Philosophy

### Hegel in Berlin in Post-Restoration Prussia
- Prussia: torn between restoration and reaction
- newly ambitious and educated bourgeoisie who were eager to reform character of public life

### The Myths of Hegel's Authoritarianism
- apologist for Prussian authoritarianism
- overcoming the shadow of Nazism and re-evaluating Hegel a rationalist
  - by 80s, fairly well discredited
- not an American liberal, that state formed out of rational pact of individuals
  - civic republicanism
- subjective will of individual should be rationally brought into alignment with larger project of our state
  - people should see state as larger embodiment of own needs
  - object as embodiment of own purposes

### The German Admiration for Classical Greece
- aesthetics: Greece embodied harmony of form, grace, that Germany could restore that "noble simplicity and quiet grandeur"
- inspired Hegel
  - no means a romantic
  - deeply admiring of Athenian democracy
  - *polis* a model of what modern polity could be like
  - not nostalgic return; primitive unity can't be captured, but can be dialectically recuperated in modern terms, taking into account rationality and modernity brought on by Enlightenment
  - for Athenians, state is *their* state: create oneself

## Hegel's *Elements of the Philosophy of Right* (*Die Philosophie des Rechtes*), 1821

### Overall Purposes of the Book
- descriptive: don't build up imaginary world, rationalized re-description
- normative: use reason, what polity to construct? what should political life be if it could be rationally defended?
- only responsible way to treat political philosophy: the unity of descriptive and normative aims
- human kind has worked out for itself highest concept of freedom
- look ad political institution and affirm what is reason's work
  - *Idea* refers to freedom
  - should not only exist as philosophical construct
  - see it as something that has surfaced in the temporal and transient
  - concrete exp. of highest ideal

### What Does the Concept of Freedom Require?
- freedom: kind of concept that is recognized by subjective terms and rational institutions
- dialectic works two ways: those objective institutions also shape who we are as subjects so that we will the right things
- state not Leviathan that dominates but an embodiment of individuals

### The Task of a Political Philosophy
- not: impose wholly external and exotic order on the world
- not "ought"
  - rather, "is"
- "ought" as embodied in the "is"
- how did institutions of Hegel's time embody freedom?
  - what is actual merits rationality if it can be shown; if not, not fully rational
  - or, surrender to existent institution, as what is there is only rationality
  - what did Hegel mean?
- affirming what has come historically
  - only sort of thing that can be a candidate for rational scrutiny is something that has developed
  - thus, philosophy always comes too late
- knowledge only comprehends the world after it has taken shape
  - retrospective
  - recognize how the ideal has shaped the real

## Social Life as a Mediated Totality: A Basic Sketch
- analytical dialectic: three moments

### Personal Freedom ("Abstract Right")
- personal freedom: to be free is to realize one's needs without interference
- right to property follows from expressive self
  - manifest one's will in objective things

### Moral Freedom ("Morality")
- autonomous will
- the guiding purpose of the moral will is the realization of the *good*
- remains too abstract; two respects
  - more will does not equal abstract appearance, moral will gets shaped into moral will, subject must be educated. we come to be the social beings we are
  - overcome abstractness through concrete and particular situations
- morality has abstract universality, needs something more concrete and more objective
  - that objectivity manifest in concrete institutions

### Objective Freedom, or Ethical Life (*Sittlichkeit*)
- components of life in common
- the family, civil society, and the state

## The Tripartite Structure of Ethical Life: Family, Civil Society, and the State

### The Family as a Mediated Form
- crucial role, since subjects in its most primitive form feels at home
- dialectical relation between male and female: male (activity and volition) and female (passivity and piety)
  - family structure so that will can first be formed
  - child's upbringing and education

### Civil Society (*bürgerliche Gesellschaft*)
- discovery of civil society
- realm that is necessary for development
  - individual pursue material interests and means
- in the very act of individual pursuing needs, particularity passes over to universality
- aiming at a common realm
- emergent capitalist marketplace
  - codes of justice that mediate claims
  - police
  - corporations: estates, guilds, organize us in economic pursuits
    - cannot be purely atomistic
    - social life in separation

### The State
- mediate between former two, embrace in both something larger and objective
- mediated unity
