+++
title = "Spirit in History"
date = 2020-03-03T13:34:02-05:00
series = "HIST 1333"
+++

## Introductory Remarks: Hegel's Lectures on the Philosophy of History
- culmination of views since beginning of religion
  - question of human suffering calls for some purpose
  - Dante's Inferno: justified theodicy, reflection of divine insight
  - find a meaning in suffering

### Hegel in Berlin, 1818-1831; lectures and posthumous publications
- Hegel belonged to now-obsolete, polymaths of philosophy
  - logic, art, religion, and more

### Three Notorious Features of Hegel's Philosophy of History
- metaphysical teleology: automation to history, organic necessity
  - Spirit's unfolding as predetermined trajectory
  - might look like this (e.g. Aristotelian acorn to tree)
- theodicy
  - Book of Job: why all this suffering?
    - God's more powerful
  - question remains: authors, philosophers offer theodicies
    - Alexander Pope's strong theodicy
  - Voltaire ridiculous above view after Lisbon earthquake
    - *Candide*: optimistic, foolish figure, Dr. Pangloss
    - Panglossian: naive optimism
    - manifest meaningless suffering: theodicy mistaken
    - faith in God and Providence foolish
  - spirit's path from East to West
    - unidirectional
    - prof's view: undeniable and unacceptable Eurocentrism
    - premise from Absolute East and West: reaches its completion in the West and world history simply concludes

### What Can Be Salvaged from Hegel's Philosophy of History
- grounded in simple premise: collective human conduct is purposeful and reflects rationality
  - everything we've done should make sense in context of wanting to become free
  - we made it, we can understand it
  - inherited from Vico: *verum factum* principle
- subject and object as one
- robust teleology: destined to unfold, this is the way things are supposed to be
  - once history has arrived at certain moment, engage in retrospective justification, discern from within that record the workings of reason
  - does not have to mean things were supposed to unfold in a particular way, but rather a growing rationality to that process
  - problem solving of *Geist* as it becomes more and more robust

## The Idea of the "Cunning of Reason"

### World-History as a Dialectical History of Rational Spirit
- world history as the unfolding of our collective reason
- world history as the unfolding of freedom and spirit's "self-consciousness" of its freedom
- Hegel: as a philosopher in university in 1800s his own philosophy of history exemplifies spirit's self-consciousness
  - self-reflexivity
- history's dialectical structure has place for negativity
  - affirms his philosophy of history is an instance of theodicy
  - whatever is, is right?
  - reason makes use of apparent negativity in order to fulfill its higher positive goals
  - "cuning of reason": negativity in the service of reason
  - negativity needs to make sense which necessitates higher view of life to justify war etc.

### What does the Cunning of Reason Justify?
- history as a "slaughter-bench"
  - history as phenomenal world may appear to be irrational but finds its justification
  - sounds arrogant, provoked Heine

### The Question of Means and Ends
- incorporated Kantian philosophy, one must treat humans as end and never means
  - Hegel, as a good student of Kant, knows can't treat humans as means, instrumentalized
  - but spirit seems to use humans as means
  - this is not what he intends
- reason's purposes are our purpose
  - subject and object dialectically the same
  - reason not out there independent of us, it is us
- questions raised in class
  - what is "we" here?
  - who gets to "cash out" and view reason at its culmination? 
  - metaphysical geography

## Concluding Remarks

### The Limits of Retrospective Justification
- justifies far too much?
  - Frankfurt School: Auschwitz major affront to reason, no theodicy possible after Holocaust
  - but far more than just one event
  - Adorno: bombing of peasantry in North Africa, citizens in Vietnam and Cambodia
  - everyday horrors that are not just buzzing along indifferently to human history
  - localized suffering not siloed from macro-level history
- we now believe human history in everydayness contribute to big things
- Dostoevsky ridicules someone like Hegel in *Brothers Karamazov*
- Hegel secularize theodicy: reason's purposes and intent

### The Limits of Hegel's Idealism
- material suffering in humans as fragile
- affirm primacy of spirit over physical being which justifies the unjustifiable
- this error turns us away from Hegel to Marx
  - Marx rejects spirit's sovereignty
