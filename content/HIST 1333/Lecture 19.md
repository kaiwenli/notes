+++
title = "Historicizing Political Economy"
date = 2020-04-09T22:48:11-07:00
series = "HIST 1333"
+++

## Introduction: Marx's Studies in Political Economy at the British Museum

### Marx in the 1850s
- poverty

### From Philosophy to Economics
- *Grundrisse*: Marx's developing economic thinking
- more texts 

### Toward a Critique of Economics
- extraordinary broad: economics far more than highly abstracted and mathematical realms
  - how we produce and reproduce material life
- political economy: way bourgeoisie justifies economic life
  - naturalized structures
- historicized bourgeois political economy
  - dismantling

## Background Comments on Marx's *Grundrisse* (1857-58)

### Historical Background on the Origins of the *Grundrisse*
- Ricardo, Proudhon
- fills two dozen notebooks on various topics
- 1857: introduction to this text

### Hegel's Enduring Influence
- important mentor and interlocutor
- Hegel's *Logic*: terrifying abstraction
- Marx would still keep Marx's *Logic* on hand as a guide into the melancholy science
- but, Marx was developing a certain allergy to Hegel's abstractions
  - expressed impatience with abstraction of Hegel's thinking

### Economics and the Materialist Critique of Hegel
- philosophy comes on the scene only after world has come into being
- to learn anything about material life, examine material reality itself, not own abstractions that cause us to lose focus of that
- guides Marx's thinking in *Grundrisse*

## Marx's *Grundrisse* and the Dialectical Critique of Economics

### The Critique of Robinsonades
- directs ire against Smith, Ricardo
- counter-factual tales of civilization
  - Robinsonades should not be understood as a reaction against an overgrown civilization
  - gesture of nostalgia produced by urban bourgeoisie
- in a society of free competition, people untethered from natural, traditional bonds
  - self-possessed individual
  - wandering laborer who makes way from countryside into city, engages in whatever trade is available
- isolated individual not merely nostalgic, but also the truth of bourgeois life
- thus dialectically both fantasy and reality
- prof's remarks on methodological individualism
  - imagine rational individual, abstraction from surrounding culture and history
  - e.g. prisoner's dilemma, rational choice theory
  - Rawls' veil of ignorance
  - still appear with great power in social sciences and continental philosophy
  - Heidegger's "mineness", own being

### The Critique of Abstraction
- Smith: abstract labor generic category of economic analysis
- abstractions false when they are presumed to be eternal and natural features are economic life
  - historically specific features in particular
- these abstractions are not false because they possess objectivity insofar as they do grab onto features of bourgeois society today
  - abstract labor has risen as historical path
- Smith was trying to capture nature of human economic life
  - did so at particular moment in history
  - blinded by human economic life in general, rather his own time
  - nonetheless right in his description
  - particularly right with regards to ahistorical abstraction of labor in general
  - used that abstraction to grasp nature of economic conduct and value
- Marx: this abstraction has gained reality in Smith's time
  - the abstract term labor, which runs through the writings of political economy, has become truth of bourgeois society
  - not merely an ideological category
  - captures something true about the modern world: Marx uses that category himself
- what we see here is that Marx is entertaining the historical preconditions for his own criticism

### The Critique of Economic Abstraction as Reification or "Forgetting"
- political economists forget how these abstractions come into being
- bourgeois society has torn away all of the obscuring forms of tradition that have prevented us from understanding real economic life
  - recall *Manifesto*: reconsider relations
- most sophisticated form has clarified what's been there all along
  - remnants cleared away
  - insight into where we've come from
- when forgetting historical, reduce economic categories to abstract, quasi-natural things, as if they did not have a past
  - reification
- recall Feuerbach: God an expression of our ideals but forgetting we are the sources of them
- if we get over this reification and this forgetting, need to understand that economy itself is not one isolated, self-regulating realm
  - idea that economy is a kind of natural object took hold in 18th century around time that Newton was describing the physical world as one governed by universal law
  - *Wealth of Nations*: Smith draws explicit analogy between his understanding of economic realm and Newtons' laws
- Marx: economic life is not self-regulating
  - bourgeois society is a recent phenomenon
  - can be replaced
  - thus, no realm that can be learned in abstraction
  - plea that we get over fetishization, something that has independent power against us, when it is part of our shared and remade human world
  - understand economic conduct as part of social reality

### Marx on Historicity and its Dialectic
- to understand economic life, must understand economic life as a totality, as a organic whole
- analogy not to Newtonian world, but to the Darwinian world, a world of organisms
- economic life is not stable
  - human being emerged from ape
  - bourgeois economy emerged out of earlier creature
- "holism": bourgeois economic life forms organic whole with discrete moments
  - shouldn't be misled
  - does not mean that it does not contain within it different traces of other economic forms
  - Marx does not advocated for seamless holism, of wholly self-consistency
  - contaminated by moments from past and troubled by anticipations of the future
  - one specific type of production *predominates* over the rest
  - e.g., slave labor generating wealth out of North America
- "mode of production": predominance, not absolute uniformity
- stadial thinking
  - but talks about it in terms of aesthetics
  - how each epoch succeeds the one before it, but does not foreclose our appreciation of elements of the past
  - Marx is a historical dialectician but not supercessionist
  - past forms of life have not been rendered so invalid that we cannot appreciate what they produced
- historicity and enduring normative claims
  - past persists within the present
  - present can still look at past art, philosophy, find in it certain normative instruction
  - furnish enduring standards, ones that serve as unattainable models in our own time
  - not returning to old forms, but rather to reproduce the truth of those forms but at a higher stage
  - ancient image of human flourishing can still serve as our name
    - not because of nostalgia, but because something in that norm survives for us and we might be able to reproduce its truth at a higher stage
    - Hegel's admiration for Athenian *polis*
  - we don't give up on beauty of the past

## Concluding Remarks

### The *Grundrisse* as a Preparatory Text

### Toward an Economic Analysis of Capitalism as a Total System

### Towards an Ideological Analysis of Bourgeois Political Economy

### Marx's writing of *Das Kapital* in the 1860s
