+++
title = "The Young Hegelians"
date = 2020-03-16T22:35:54-07:00
series = "HIST 1333"
+++

## Recapitulation

### Marx in Berlin: An Awakening to Hegelianism
- Marx's letter to Ruge
- shows how much Marx was already making use of a gesture from Hegel, of immanent critique and determinate negation
  - Hegel's *Philosophy of Right*: denied radical critique
- "ought" is in the "is"

### A Gradual Enhancement of Materialist Themes
- deploying certain gestures from Hegel but pushing against his metaphysics of spirit but towards a more materialistc perspective
- Marx also tries to capture essential lessons of theodicy
- what may have seemed a scandal in Hegel's history: purposes of *Geist* are somehow deeper than human purposes
  - suffering intelligible from spirit's point of view
- Marx: overcome last vestiges of religion
  - theology to anthropology
  - went further than Young Hegelians: critique of religion not enough to set us free
    - critique, overturning society

## The Young Hegelians in Germany
- group of Hegel's students who directed special attention to the question of religion
- critique an especially powerful word

### Critics of Religion
- David Friedrich Strauss: *The Life of Jesus*
  - Jesus was a human being, his life needed to be understood in anthropological terms, stripping away mythology
  - religion needs to be dialectically overcome
  - turn from theology to anthropology: find in Jesus' life certain messages we can use in this world
- Bruno Bauer: *Critique of the History of Revelation*; *The Jewish Question*
- Ludwig Feuerbach: *The Essence of Christianity*

### Other Young or "Left" Hegelians
- Marx and Ruge: leftist journals
- Prussian censors ended them

### The "Young Germany" Movement in Paris
- larger artistic movement
- Heinrich Heine, Ludwig Borne

## Feuerbach's *The Essence of Christianity (Das Wesen des Christentums)* (1841)

### The Idea of Religion
- everything that human beings create is an expression of them
- everything that they can truly know is connected to them
- expressivist theory of the subject: religion needs to be understood in this way too
- when we have an idea of God, it is an expression of us
  - not of individual humanity though, as we are social beings
  - our representation of the world also social
  - thus, expression of us as a species
  - "species being"
- religious concept represent what humans are capable of, insofar as human beings strive for a kind of perfection
  - we are finite: limited in lifespan, knowledge, love
  - thus, not surprising as a species, we can transcend individual limitations with a concept of humanity that do not exhibit that limitation
  - humanity can imagine infinite projects
- we imagine an infinite being that has no restraints
  - independent of us
- concept of religion: grand gesture of projection
  - structure of religion evidence of that
- repeating an old idea: when humans imagine a divinity, they are imagining themselves
  - we forget that we effect that projection

### The Critique of Projection
- imagine divine being as having independent life out there when they're just attributes of us
  - projection, and forgetting of projection
- Feuerbach's history of religion: overcome previous religion by critiquing false projection
  - every religion exempts itself
- for Feuerbach, final mistaken projection is the independent being whose agency is stronger than ours
- arrive at religion of humanity

### Overcoming Projection and Reclaiming Freedom
- we can only experience ourselves as agents if we cease to project onto an autonomous being
- increasingly unfree if we don't do that
- projection and forgetting enhances human unfreedom
  - only by dismantling the concepts of religion and by reclaiming perfections of God as human can we become free
- atheism as the religion of the future

## Marx's "Introduction" to "Contribution to a Critique of Hegel's *Philosophy of Right*" (1843)

### The Critique of Religion
- influence of Feuerbach
- religion as “the fantastic realization of the human being” since “the human being possesses no true reality”
- religion as “opium of the people,” and as “sigh of the oppressed creature”: illusion or expression

### From the Critique of Other-Worldly Religion to the Critique of This-Worldly Human Suffering
- what's been projected and who's projecting it?
- introducing to Feuerbachian critique of religion: critique the society that creates religion
- religion is not just a conceptual structure but an expression of our suffering-being
  - only when we suffer do we develop projection we call religion
- religion is not just a tissue of lies, not just illusion
  - an encrypted protest against a suffering world that gives rise to these illusions
- sociological spin on Feuerbach's critique of religion

### The Discovery of the Proletariat
- material conditions: suffering as manifest in civil society
- looking for something material that corresponds to Feuerbach's universal attributes
  - God as humanity as whole
  - critique of religion must be a form of universal human unfreedom
  - resides in the common experience of labor and oppression of a group in society whose suffering is the universal suffering
- universal transformation of society

## Marx's "These on Feuerbach" (1845)

### Thesis IV
- projection and forgetting: necessary, but not sufficient
- dismantling conditions that made projection necessary in the first place

### Thesis VIII
- criticism not enough
- overcome actual, material conditions
- worldly practice to remedy social conditions

### Thesis XI
- battle cry of militant Marxists
- change

## Concluding Remarks

### Marx's Movement from Hegelianism to Social Theory
- social theory and social practice
- criticism and practice combined

### Social Criticism and the Emergence of Economics
- approves of bourgeois project of gaining political freedom
  - but not enough
- political emancipation v. human emancipation

### The Persistence of Hegelian Themes
