+++
title = "Consideration of Sources"
date = 2019-03-29T14:42:23-04:00
series = "HIST 97I"
+++

## Letters
- are they really private or not?
- relation between correspondants
- scale of correspondences

## Public documents
- who is the audience
- purpose of document?
- what does audience bring as knowledge? what is effect on audience
- *how do we get under the document?*
- what is authority on subject
- formal conventions of this medium
- context
- use of language

## Interviews
- who is talking
- audio/visual for tone
- context of interview:
  - audience and purpose
  - nature of questions
  - *relationship with interviewer*
  - *moment/context*, such as social/linguistic norms

