+++
title = "Pyramids of Giza"
date = 2019-02-05T18:14:06-05:00
series = "HAA 11"
+++

- surrounding the pyramids are cemeteries made of limestone
  - administrative members
- lots of development around the pyramids now

## Simplified Egyptian chronology

- Predynastic Period: ends 3100 BCE
  - Archaic Period
  - southern culture, northern culture
  - southern wins
- Old Kingdom, 2500 BCE, 1st intermediate
- Middle Kingdom, 2000 BCE, 2nd intermediate
- New Kingdom, 1500 BCE, 3rd intermediate
- Late Period

- We're concerned with Old Kingdom
  - Dynasty 4: 2625-2500 BCE
    - Khufu, Khafre, Menkaure

- central field provided the limestone blocks for pyramid then cemetery

## Dynasties

- 0 and 1
  - Abydos, tomb of anonymous ruler
  - command of resources to build vessels
    - international trade
- 2
  - storage magazines
  - staircase
    - complete burial work
  - mudbrick

- 3
  - stone-conceived monument
  - Saqqara: ziggurat-like building
    - simulate king's palace for the afterlife
	- dummy doors
  - Meidum: true pyramid, just lost a lot of cladding
  - Dahshur: "bent pyramid"
    - changed angles
  - Dahshur: red pyramid

Giza

  - had coating/cladding of white limestone
    - blinding for miles

Interior chambers: Khufu

  - subterranean: unfinished
  - King's Chamber
    - flat ceilings of granite

"The pyramids built Egypt"

  - huge nationwide project
  - conscripts and contributions

Mastaba

  - big stones
  - shaft on top of building that leads to sarcophagus
