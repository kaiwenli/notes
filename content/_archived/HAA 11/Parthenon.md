+++
title = "Parthenon"
date = 2019-02-12T05:49:12-05:00
series = "HAA 11"
+++

Symbol for democracy, classical culture

Doric temple

Swollen structure

Decorations: triglyph, metope

Continuous running frieze with narrative