+++
title = "Speyer Cathedral"
date = 2019-02-19T14:00:52-05:00
series = "HAA 11"
+++

SP I: 1024-61
SP II: 1080-1161

construction as continuous redesign

Romanesque: post-classical and pre-gathoic

vaulting: arched ceilings, cloister vaults, barrel vaults, groin and ribbed groin vaults

hall crypt: (partly) underground space of uniform height divided by columns

cushion or shield capital
  - round shape to square shape
  
raised choir

buried eight emperors

axial piers, frontality

rectangular piers have engaged responds terminating beneath a blind arcade

purple and yellow sandstone coursed ashlar

masonry on interior, uncoursed rubble masonry on exterior

dwarf gallery

lombard housing with wall strips, corbel table and dwarf gallery

octogonal cloister vault

arches exert lateral, vertical, and horizontal thrust
  - otherwise cracks because not buttressed