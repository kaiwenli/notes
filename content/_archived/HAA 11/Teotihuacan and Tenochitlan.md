---
title: "Teotihuacan and Tenochitlan"
date: 2019-02-07T09:43:51-05:00
draft: false
---

## "stone age": no metal, wheels, etc
- all human labor

glyphs, concept of a town, on the profile of a building
the city expresses itself through architecture

rich resources: woods for stucco, volcanic glass from obsidian, lime

## microcosm of the cosmos
- measurement and layour
- temple of moon: a minimized mountain
- unit of measurement: buildings measured metrically in the calendar; space and time
  - calibrated to celestial cue

## atetelco
- small temple in middle
- all have true fresco decorations in interior and exterior
- flat, slightly inclined roots that funnel water into cistern

## Palace of Quetzalpapalotl
- mosaic columns: no large blocks ,small units that add up
- carried in low relief
- palette of color

causeway goes up and down; bumpy(?)

tablero: expresses architecture of Teotihuacan

## Vocab
- Teotihuacan: place of the Gods
- Pyramid of the Moon
- Pyramid of the Sun
- Ciudadela: compound in Teotihuacan
- Talud and tablero: facades of Teotihuacan temples
- Tenochitlan: Mexico City
- Templo Mayor: Aztec temple
- Chicomoztoc: Aztec place of origin
- Atlpetl: town
- Calpulli: ward or neighborhood
- Copan: Maya City in present day Honduras
