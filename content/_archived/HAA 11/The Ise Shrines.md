+++
title = "The Ise Shrines"
date = 2019-02-14T14:00:52-05:00
series = "HAA 11"
+++

- 125 shrine buildings
- holiest shrines of Shinto
- associated with the most primitive culture of building
- renewed 62 times
- $500M

- pillars directly dug into ground
- dimensions that is 3x2 big
- 15 meters

- *Hinoki* (Japanese cypress)
- 400 year old trees
- wood itself is extremely durable

- joinery: no nails, precisely fitted
- withstand high humidity and the expanding and contracting of wood

- raised floors: ventilation and protection from rodents and insects
- billets: keep building stable from monsoons 

- adopt rice grainery model

- during 7th century, open air compound for the sun goddess was formalized by the imperial clan into the compound that we now know as the Ise shrine complex
- 125 total deities reflect a political alliance with other clans
- Ise: Chinese influence when it is described as quintessentially Japanese
  - Chinese layout
  - before that, Shinto temples laid out irregularly and in accordance to idiosyncrasies of land
  - ornamentation

## Phenomenology
- how we know what we know
- inner and outer sanctuaries are extremely limited
- link between architecture and photography
  - might fail to capture passage of time
  - particular time of day or season
  - calibrated so much to surroundings
  - solipsistic experience

## Architecture as process
- transporting, procuring wood from central alps (renewal)

## Metabolism in architecture
- thatch(?), roofs that last for 2-3 generations
- buildings that are inhabited then fires in house keep the roof dry, remove pests, etc.
- materials themselves do not dictate metabolism
- decomposition is staged
- pillars not laid directly into ground but stone foundation, which protects from bacteria from soil
- Ise renewed so regularly that can re-take social meaning
  - Japan's renaissance after defeat in WWII
  - oil/OPEC, "last renewal of Ise in history"
  - bubble economy
  - mirror of different anxieties of Japan
    - aging population
    - ecology
    - renewal process to better understand heritage and environment

## Vocabulary
- forked finals, cylindrical billets
- sacred regalia: mirror, sword, sacred jewels
- Sun Goddess Amaterasu (enshrined in Main Shrine of Inner Sanctuary)
- Asuka basin
- Shinto ("Way of the Gods")
- *kami* deity
- *torii* gate
- Japanese cypress (*hinoki*)
- miscanthus reed (*kaya*)
- Katsushika Hokusai (1760-1849)
- *minka* (Japanese farmhouses)
- Yamato clan (the Japanese imperial clan)
- China's Tang Empire (618-907)