+++
title = "Petroleum"
date = 2019-03-10T04:13:14-05:00
series = "SPU 31"
+++

## Global energy consumption
- petroleum and other liquids and natural gas come out on top
- dependency on natural gas growing

## Petroleum and natural gas
- naturally occurring liquid and gaseous hydrocarbons that yield a range of combustible fuels, petrochemicals, lubricants
- mostly form by the same processes
- first: 2000 BC in Sichuan basin as part of salt processing
- oil and natural gas are >97% carbon and hydrogen, hence the name hydrocarbons
- carbon forms strong covalent bonds

## Molecular structure
- homologous series: families of molecules whose members have similar structures and properties but vary by the size of CH groups
  - paraffins: chains and branched-chains
  - napthenes: ring-like structures
    - each carbon shares one bond
  - aromatics: ring-like structures
    - some carbons share double bonds

## Hydrocarbon reservoirs
- most sandstones don't make good hydrocarbon reservoirs
- good conditions: wind and water
  - e.g. beaches

### Limestone reservoir
- can also be porous and are made of fossil
- found in reef systems
  - e.g. Bahamas

### Reduce porosity
- cementation: mineral growth, can fill into pore spaces of rock
- deformation: grains deform and fill in pore spaces
- primary porosity tends to be reduced with burial - decreases with depth
- secondary porosity may be developed by dissolution (mineral decay and growth) and fracturing

### Secondary porosity
- delomitization
- fracturing: producing small but meaningful area of pore space

## Seal
- hydrocarbon seals: rocks with low permeability that impeded the migration of hydrocarbons
- conditions that favor the development of reservoirs prohibit seals
  - impermeability

## Logging
- cores and well logs provide the most direct means of identifying source, reservoir, and seal characteristics
- gamma rays, density, etc.

### Spontaneous potential logs
- measure the difference in electromotive force between the formation and drilling fluids
  - porous sandstones give strong negative readings
  - shales, limestones, and evaporites (low porosity) give strong positive readings

### Resistivity logs
- measure the resistance to the flow of current (opposite of conductivity)
  - shales and porous sandstones with sand water yield low resistivity
  - carbonates, coals, and hydrocarbon bearing formations generally have high resistivity

## Traps
- a configuration of rocks which is able to confine hydrocarbons

### Structural traps
- fold, fault, and more

### Stratigraphic traps
- don't require tectonic activity
- dune

### Spillpoint
- position (3d) where trap is breached
- deepest level within a trap which hydrocarbons can be filled to
- maximum potential source of oil is defined by spillpoint

### Structure contour map
- map of geologic horizon in depth
- dots are wells that have been drilled
- line = depth is the same along it
  - inside = lower

### Isopach map
- map of the thickness of a geologic unit
- characterize reservoir thickness (pay), source potential
- hydrocarbons: thicker is better
- inside a line = thicker

### Uncertainty in maps

### Geophysical imaging techniques
- seismic reflection: off-shore, big boat, sound energy
  - streamer: listening devices behind boat

### Direct hydrocarbon indicators
- presence of natural gas 
- further reduce risks
- caveats: unwanted gases

## Hydrocarbon system elements
- all necessary hydrocarbon elements must be in place at the correct time
- source, reservoir, seal, overburden

## Petroleum digging and production
- enabling tech for the oil and gas industry
- environmental impacts

## Mud-rotary drill rig

### Mud system
- helps cool and lubricate bit
- moves cutting ups borehole
- maintains borehole stability
- balances fluid pressures (primary)

### Stress in the earth
- stress due to weight of overlying rocks and overlying fluids; lithostatic and hydrostatic
- the pressures are separate; for fluid, depends on height
- fluid pressure will be greater than hydrostatic due to presence of a seal (fluids are now partially supporting weight of rocks that lie above the seal)
- to avoid uncontrolled fluids entering the borehole, the weight of the drilling mud must balance the fluid pressure

### Borehole casing
- a series of steel pipes that are forced down a borehole to help maintain borehole integrity and resist formation pressures
- each casing string is smaller than its predecessor
- casing is generally cemented in place

### Cement job
- a plug of cement is lowered down liner, separated from mud by a "spacer"
- amount and type of cement are chosen to ensure that saline fluid and hydrocarbon bearing zones are fully cemented, without leaving excess cement in the borehole
- subsequent drilling generally pulverizes and passes through the shoe at he bottom of the easing string, until another section of casing is set

### Blow-out preventer
- series of valves that help control and monitor fluid flow at borehole
- devices measure pressure of fluids
- series of devices can be deployed to close off flow within (ram) or around (annular) borehole casing string
- shut-off is triggered by operator, using electrical, acoustic, and/or ROVs
- last line of defense against blow-outs

### Deepwater drilling
- Drill ship
  - up to 12k water depth
  - designed for drilling in remote locations
- Semi-submersible drill rigs
  - 8k water depth
  - 30k below mudline

## Onshore production systems

## Offshore production systems
- fixed platform <= 500 m
- compliant tower 500-1500
- floating production system
- floating production, storage, offloading vessel
- tension leg platform
- spar platform

## Calculating oil/gas reserves
- trap volume (m^3): trap width * trap length * reservoir thickness
- oil reserves (barrels) = trap volume * net/gross ratio * porosity * (1 - water cut) * recover factor * 6.89 barrels/m^3

## Factors that influence ultimate recovery
- reservoir porosity and permeability
- reservoir pressures (drive)
- oil density and viscosity (API gravity)
- economics
- production contract terms and politics
- environmental concerns
- others?

## Reservoir pressures
- pressures have a primary influence on production rate and ultimate recoverable reserves
- pressures can be used as a means to infer the size of hydrocarbon columns and other reservoir characteristics

## Primary recovery
- water drive: pushing up into where oil and gas are
- gas cap drive: pushes down
- solution gas driver: "coke bottle" case, maintaining pressure

## Secondary and tertiary recovery
- secondary recovery: process of injecting water or gas to maintain reservoir pressure
- tertiary recovery: process of injecting fluid and gases with the aim of mobilizing hydrocarbons in the reservoir and maintaining pressure
  - often called enhanced oil recovery

## Costs of producing oil and gas
- full cycle cost: make decisions about exploration, field development, and abandonment
- half cycle cost: make decisions about investing in existing fields (e.g. drilling new wells to replace old ones, field expansion)
- lifting cost: make decisions about field operations
  - value varies widely around globe

## Recap: reserve and resource
- reserve: can develop right now
- resource: what could be developed given better technologies

## World oil production
- R/P ratio: 20 years as opposed to coal's hundreds of years
- as such, need to do lots of exploration

## Composition of crude oil
- paraffins = more gasoline

## Environmental Impacts of Petroleum Exploration, Production, Transport, Refining, and Consumption

### Water pollution (local and regional impacts)
- exploration and production activities
  - drilling and production byproducts
  - blowouts
  - spills
  - invasion of sensitive environments
- transport and refining
  - pipeline leaks
  - tanker spills
  - explosions
  - toxic emissions

### Air pollution (regional and global impacts)
- consumption
  - photochemical smog
- greenhouse gas emissions, global warming
  - CO2, CH4

## Exploration and production

### Drilling and production byproducts
- barite and bentonite drilling muds
- oil-based muds
- produced waters (saline, HC contamination)
- co-produced gases (H2S, CO2)
- toxic and radioactive compounds

### Invasion of sensitive environments
- land clearing (soil erosion)
- access routes

### Summary of US requirements for drilling waste disposal

#### Onshore
- most water-based drilling muds are buried on site or sent to a landfill after water and certain contaminants are removed
- oil-based muds are typically recycled

#### Offshore
- baseline requirements: no discharge of free oil or diesel fuel
- acute toxicity must have 96-hour LC50 > 30,000 ppm
- metals concentrations in barite added to mud must not exceed:
  - 1 mg/kg for mercury
  - 3 mg/kg for cadmium
- no discharge of drilling wastes allowed within 3 miles of shore

### Co-produced gases
- CH4 burned on site
- CO2 generally re-injected or released

#### Hydrogen sulfide gas (H2S)
- sour = up to 50% H2S
- highly toxic
- hydrogen sulfide converted to sulfur dioxide which is converted to elemental sulfur

### Site contamination
- natural occurring radioactive compounds can contaminate drilling muds and fluids

- mineral scales inside pipes
- sludges
- contaminated equipments or components
- produced waters

- leading source of toxic compounds or hydrocarbons: improper cement jobs
  - or, improper disposal of drilling muds and saline brines
  - spills of chemicals during storage

### Catastrophes
- blow-outs
- fires
- oil spills

### Borehole pressure measurements
- pressure can be a cause, but not too often
- pumps break
- hit porous rock or fault zone and then mud flows into porous rock or fault zone
  - can't keep pressure high
  - then release fluid into well