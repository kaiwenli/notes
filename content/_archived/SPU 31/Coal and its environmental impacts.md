+++
title = "Coal and its environmental impacts"
date = 2019-01-31T04:13:14-05:00
series = "SPU 31"
+++

## Why does coal have sticking power
- coal is relatively cheap per energy value
  - it is also non-volatile compared to other energy resources
- lots of sticking power: a ton of it
- R/P ratio: the ratio of amount available (e.g. reserve) by the rate at which it is produced
  - coal has hundred year R/P ratios, compared to 10-20 years for natural gas

## What is coal
- fossil fuel: remains of plants/animals
- most dead organic material undergo decomposition
  - takes specific conditions to become coal

## Sedimentary rocks
- igneous rocks can be eroded by wind and water and can become sedimentary rocks
  - they both can be buried deeper into the earth and be exposed to high temps
  - then they become metamorphic rocks, which can go up into the surface or become magma
- sedimentary rocks make only 5% of earth's crust, but **majority of surface rocks are sedimentary**
- contain the **world's fossil energy resources** and more later on to be introduced later

### Classification of sediments and sedimentary rocks
- siliciclastic sediments
  - mud, silt, sand, gravel, created by weathering
  - detrital grains reflect their source, transport mechanisms, and depositional environment
  - classified by the:
    - size of particles
    - sorting of particles
- chemical sediments
  - carbonate, sulfate, phosphate, iron, etc
  - composition reflects chemistry of formation waters
- biological sediments
  - shells, bones, teeth, coal, oil, etc
  - reflect organisms and paleo-environments

- porosity: open spaces; amount of fluids a rock can hold
- permeability: how easy it is for fluids to move through; connectivity

- chemical sedimentary (e.g. salt)
- biological sendiments (e.g. ancient organisms)

## How to into coal deposits
- coal is a biological sedimentary rock
- formation of large coal deposits requires:
  - large organic productivity
  - deposition of organic materials in anoxic environments
  - rapid burial for preservation

- why are deltas good for coal?
  - high organic productivity in swamps and sloughs between channels
  - low rate of water flow and high organic content lead to anoxic conditions
    - the critters are using up the oxygen; not enough oxygen for all organic matter to be decomposed
  - migration of channels and delta lobes deposits sediments on top of organic materials, protecting them from decay and eventually exposing them to high temperatures in the Earth's interior

## Peat resources
- peat is the unconsolidated, partially carbonized remains of land plants. A coal precursor, peat is itself used as an energy resource
- not too much energy content

## Geologic record
- today's geographic distribution of coal deposits are controlled by the locations of continental land masses at times of warm-temperate, humid global climates
- Carboniferous: lots in NA, Africa, China because at the time they were near the equator

## Coal grades
- lignite: between a peat and "conventional" coal
- sub-bituminous coal
- bituminous coal
- anthracite coal

## Thermal maturation
- coal grades are based on their thermal maturity
- increasing coal grade corresponds to a decrease in hydrogen and oxygen content
- higher coal grades have more energy content and lower moisture content

## Accessing coal deposits
- surface mining
  - direct removal of coal from outcrop
  - removal of overburden (strip mining) to access coal
    - scrape off soil, take off rocks and put it in the old strip they just dug out, trucks come in and take the coal, repeat
- subsurface mining
  - room and pillar: yields only 60% of coal but leaves a lot of coal to leave the pillars there (otherwise will collapse)
  - long wall mining: mineshaft collapses behind them; high environmental impact (water flow, ecosystem above)

## Where the coal is at
- Powder River Basin (Montana-Wyoming) is the largest coal reserve in the US
- produces 40% of US coal
- coal are in Tertiary rocks (Fort Union Formation)
- ~1.07 trillion tons
- recoverable coal (resource) ~ 162 billion tons (10:1 stripping ration)
- reserves ~25 billion tons

## Stripping ratio
- it is the thickness of overburden over the thickness of resource

## Coal transportation and use
- most coal is used domestically

## Coal burning electric power plants
- coal is brought by barge/train to a power plant
- pulverize it, remove non-coal fragments
- get coal to the consistency of cooking flour
- blow into furnace
- water system, generate steam, spin turbines
- condense steam back into water

- generating output for Brayton Point Power Station: 1530 MW
  - wind turbine: 2 MW
