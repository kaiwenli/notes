+++
title = "Coal-related environmental concerns"
date = 2019-02-04T04:13:14-05:00
series = "SPU 31"
+++

## Local environmental hazards
- mining accidents and hazards
- surface mining operations
- acid mine drainage

## Regional/global environmental hazards
- ash, SOx (acid rain), NOx
- mercury (Hg), and other heavy metal emissions
- CO2 emissions

## Coal mining safety
- coal mining deaths peaked in early 1900s
- nevertheless very hazardous occupation
- heightened coverage of coal mine accidents
- questions about official reporting and accuracy of totals

### Safety legislation
- 1910: U.S. Bureau of Mines established (investigate accidents, recommend safety procedures)
- 1967: Federal Coal Mine Health and Safety Act
  - had authority
- 1977: Federal Mine Safety and Health Act

## Centralia, PA
- persistent, unstoppable coal fire
- town dropped off the map

## Surface mining operations

### Environmental impacts
- surface land use and access
- acid mine drainage
- efficacy of reclamation: restoration of land surface, vegetation, ecosystems

### US Surface Mining Conrol and Reclamation Act
- established a program for the regulation of surface mining activities and the reclamation of coal-mined lands, under the Department of the Interior
- sets minimum uniform requirements for all coal surface mining on Federal and State lands
- mine operators are required to minimize disturbances and adverse impact on fish, wildlife, and related environmental values
- restoration of land and water resources is ranked as a priority in reclamation planning

## Sulfur
- sulfur present as pyrite (fool's gold) and in organic compounds

## Acid mine drainage
- expose organic compounds in the coal to water
- forms iron sulfate and sulfuric acid
- poses a direct threat to acid intolerant species
- mobilizes toxic metals
- enters water sources and raises pH
  - leads to rapid deposition of iron and other metal-bearing minerals
  - harmful effect on local ecosystems

## Acid run-off from mines
- typically get a large pulse of acidic water immediately after closure
  - when the mine operates water gets welled-out
  - but after closure it rebounds

## Acid run-off treatment
- alkalinity: adding CaCO3, raises pH, precipitates metals
- wetlands: filters metals, raise pH
- anoxic systems: promote growth of bacteria that convert sulfuric acid to a form of sulfur that bonds with metals and precipitates (FeS2)

## 1972 Clean Water Act
- landscape restoration
- the classification of the waste material - is it fill or talus?
  - talus: any mining waste

## Impacts of coal utilization
- acid rain: pH of less than 5
- hydration reaction: SO2 + H2O -> H2SO3 (sulfuric acid)
- oxidizing reaction: H2SO3 + H2O2 -> H2O + H2SO4

## Natural sulfur cycle
- volcanic eruptions

## Sources of sulfur dioxide emissions
- 70% come from fuel combustion

## Impacts of acid rain
- due to wind, source of acid rain != where effects take place
- hazards to acid intolerant species, reduce biodiversity
- leaches nutrients from soil
- deforestation
- decline in agricultural productivity
- haze/smog
- corrosion
- health risks
  - direct threat to intolerant species
  - high acidity mobolizes toxic metals (e.g. mercury, lead, arsenic)

## Reduction of acid rain
- use of low-sulfur fuels
- removal of sulfur from coal by washing
  - metallic sulfur can fall off in processing
- scrubbing of emissions at source
  - dry lime systems
    - yield lime salts, unusable products that must be disposed of in land fills
  - wet lime systems
    - yield gypsum, usable byproduct
    - requires lots of water, capital investment
    - ~90% eficient
- emission dispersal
  - scrubber and baghouse

## Emissions
- falling in US and Europe
- rising in Asia
- cap and trade: set nationwide emissions levels to one-half amount of SO2 in 1980
  - sell vouchers that allow a power plant to emit a certain amount of sulfur
  - less vouchers available over time
  - financial incentive to invest in other energy sources over time

## Acid rain program
- effective

## Trace metal emissions
- mercury
  - methylmercury in fish causes danger for pregnant women

## IGCC
- coal-based integrated gasification combined cycle
- coal dust to gaseous products
  - synthetic natural gas
- benefits include
  - lower emissions (SOX, COX, Hg, particulates)
  - opportunity for Carbon sequestration
  - useful byproducts
- challenges
  - cost efficiency
  - reliability