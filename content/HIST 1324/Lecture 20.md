+++
title = "Foucault/Derrida: The Internal Critique of Structuralism"
date = 2019-04-11T19:55:01-04:00
series = "HIST 1324"
+++

## Introductory Remarks: Structuralism Contested

### Was Foucault a Structuralist?
- even if structuralism not integrated in existential humanis, structuralism itself is a humanism
  - created by a self-important human subjectivity

### Structuralism, Humanism, and Anti-Humanism
- structuralism: anti-humanist

## Derrida on Foucault and Madness

### The Contestatory Reading

#### The Problem of Method
- state of control of mentally ill
- if history to historical objectivity, implies he is on the side of reason
- inned-tension and complicity from the very beginning
- history of a silence?

#### The Problem of Descartes
- problem of labeling 17th century as Age of Reason
- dreaming is the mad: still in touch with sensual reality
- unity of history meaning in an epoch assigns to it a unity, as if one thinker
- lucidity
- is structuralism totalitarian? do all meanings cohere?

### Implications
- too easy to exhibit coherency
  - why would there be just one characteristic

