+++
title = "Existentialism, Humanism, Anti-Humanism"
date = 2019-03-05T21:22:10-05:00
series = "HIST 1324"
+++

## Introductory Remarks: The Twilight of the Subject
- structuralism and post-structuralism both challenge existential humanism

## Sartre's 'Existentialism is a Humanism' (1945-6): A Recapitulation
- human has no higher metaphysical compass point: existence precedes essence
- human condemned to freedom
- subjectivity of individual = point of departure
  - develop theory not only of self-responsibility but social responsibility

## Heidegger's 'Letter on "Humanism"' (1946-7)

### The French View of Heidegger: General Remarks

#### Heidegger as maitre-penseur
  - grew up in environment that was somewhat anti-modernity, anti-urbanism, anti-Semitic
  - retained a certain critical attitude toward the modern world
  - fell out of favor with Nazis
    - was his philosophy damaged by political activity?
  - Celan "ingested" Heidegger's insignia
    - demonstration of how strong Heidegger's philosophy was, even as someone so affected by the Holocaust

#### Questions of translation
- early translations presented Heidegger so that his ideas were compatible with Humanism
  - Dasein: existence
  - Corbin and Koyre: translated to human reality
  - granted it human independence; emphasized human rather than being

### "Letter on 'Humanism'": History of the text
- intervention of the philosopher himself
- Beaufret: how can one give a new sense to "humanism"
  - gave Heidegger an opportunity to intervene in political and philosophical debates in France when his reputation was in trouble
  - Heidegger could explain himself and challenge an unsettling perception that his work dealt nothing with humanism given his political beliefs
  - response in open letter
    - intent on navigating the difficult political terrain in Europe: Marxism and Catholicism

### Chief Arguments of the text

#### preoccupied with one question
- reminder of the centrality of 'Question of Being'
- challenge philosophical assumptions
  - since Plato, Being = invariant ground of things outside of time
  - metaphysical -> temporal, this-worldly, without quasi-theological metaphysical meaning

#### human being is passive in its Being
- not master of meaning, not sun of universe, but passive recipient of Being which discloses itself
- *thrownness*: grounded in nothing but history and time
  - de-centered from the universe
  - caught in flow of history that cannot master
  - sent out without mission or metaphysical purpose
- Being: clearing (like forest clearing, where trees thinned out and light comes through)
  - world is disclosed to us that make it a clearing
  - modern West: oblivion of Being
  - life itself forget Being; humanity lost and in misconstruing Being (e.g. God), human cannot dwell authentically in world

#### the critique of modern metaphysical subjectivism
- humanism, in traditional form, leads to nihilism
- critique technological subjectivism
- Sartre: human being as humanist hero
  - Heidegger's response: he's just dressing up traditional metaphysics with language of existentialism
- rejoinder to governing principle of Sartre's philosophy

#### a different model of the human
- relation of human being to Being
  - thinking, not action
- ontological passivity
- experience its thinking most of all when it turns to language and poetry
- caring for world rather than dominating it
- the modest, the everyday: cohabit with environment
- Heraclitus: pastoral example
- humanism is inhumanity
  - mastery, nihilism

### Larger Points of Comparison

#### Heidegger and Sartre: cultural differences
- Sartrean urbanity, Heideggerian pastoral
- Sartre: living without God, freedom, endorses modernity
- Heidegger: Greek and Roman inheritances, melancholy about what has been forgotten
  - totalities about people: "the German people, the French people, etc"

#### Politics
- activist: politics as existential necessity through existential humanism
  - promoting activism as only alternative to submission to enemy
- passivist: German isn't occupier; turn -isms away
  - concern self with deeper questions about human species
- was Sartre misunderstanding Heidegger?
  - no: understood his philosophy
    - taking it in a different direction thus creating new insights
    - Sartre and Heidegger doing very different things

## (We skip Merleau-Ponty)

## Sartre, Heidegger, and French thought since 1945: from Subjectivism to Structuralism
- waning of existential thought: "popular fashion"
- rise of anti-subjectivism: structuralism and post-structuralism
  - symbolic symbols anterior to human agency
  - primacy of social whole and language
  - shared very general themes with Heidegger: challenge to Sartre
    - more and more prescient and valid

