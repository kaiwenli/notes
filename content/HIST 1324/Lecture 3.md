+++
title = "Durkheim on the Promise and Pathology of Modern Society"
date = 2019-02-05T17:41:59-05:00
series = "HIST 1324"
+++

## Durkheim as a Theorist of Modernity

- the strains of modernity that came with industrialization, specialization, and individualism were tough pills to swallow
- Tocqueville: through modernity people will be torn away from traditional bonds of the past
  - isolated from others
- Comte: French statist
  - threat of societal disintegration can be addressed by strong state
- Spencer: modernity not a threat at all
  - pursuit of self interest -> strengthen interconnection
  - the unintended but happy outcome of individualism
- Tonnies: *Community and Society* (1887)
  - rise of modernity: community and its bonds dissolves and are replaced by frameworks of economic transactions and politics etc
  - modern society afflicted by egoism
    - pathology of individualism
- Durkheim, like other post-industrial thinkers, concerned with these issues
  - genuine solidarity can't be achieved through coercion
  - individual interests only produce superficial connections
  - aware of other theorists and their shortcomings

## The Division of Labor in Society

### Background

- cities springing up
- uprooted laborers from countryside working in factories
- the nature of work itself has changed
- now lived cheek by jowl with people they don't know
- transact with people they don't know
- as economies grow, people specialize in tasks
  - Adam Smith, Karl Marx
  - e.g. Smith's pin-maker analogy: specific people mine ore, smelt ore, etc

- as individuals become specialized, they need social solidarity
  - people become more unique through division of labor
  - that division of labor is also the same phenomenon that gives rise to social interdependency
  - shift in solidarity
    - traditional society: mechanical solidarity
	- modernity: organic solidarity
	- counter-intuitive!

### Mechanical

- social structure: no division of labor
  - society is not separated into different segments
    - every small part of society looks like bigger society
  - any one family can do what society does
- without division of labor, need something to make people feel part of bigger whole
  - need larger, sacred structure
    - oppressive, highly conservative
	- has a kind of independence: individuals partake in it but can't change it
- by virtue of being social beings, we are part of social, symbolic structure that we can't control
- relative independence of collective conscience
  - no objective right/wrong
  - social relativism where our deepest moral instincts are dependent on our local, contingent social conditions

### Organic

- after modernization and division in social roles
- seed for division of labor goes back to sexual differences
  - this is now contested
- over time, society goes through increased division of labor and then increased individualization
  - the individual is a modern creation, modern precipitant of labor
  - under these conditions, it so happens that this individual is harbinger for increased integration
    - lovely, optimistic paradox
- what happens to collective consciousness?
  - no longer need that symbolic thing
  - begins to dissolve

### Abnormalities
- people break away
- different sectors of labor oppose others
- context: lots of militant labor groups
- pathology of class conflict that we must fight against

## Suicide

### Why study this?
- close friend, Hommay, who committed suicide
- prior traditions since 18th century: suicide as problem of moral interest
  - breakdown of traditional social order
  - with rise of urban prole who were chiefly men who were separated from family in countryside
	- long hours
	- married late
	- died young
- Tocqueville: distinctive pathology of social isolation
  - """individualism"""
- last third of 19th century: moralists concerned with decline of the French nation
  - flourishing prostitution
  - population decline
  - degenerate of numbers and moral degeneration
- suicide may seem distinctly individual
  - on the surface, can only study by looking at each case individually
- Durkheim vindicates social realism, belief that societies have their own laws
  - discover social laws to something that seems immune to that sort of method

### Suicide as a Social Fact
- primacy of society to individuals
- looking at 26k cases of suicide
  - has law-like qualities to it
- typology of suicide
  - egoistic: reasons only to him/her; not concerning others
  - altruistic: heroism of a soldier in war
  - anomic: (see section below)
- Protestants live in a greater social isolation in comparison to Catholics and Jews
  - turns to anomic suicide

### Anomic suicide
- individuals untethered from society prone to disconnection
  - suffer from "anomie"—without law
  - insufficient sense of law-like social regulation and integration
- sources of anomie: conjugal/social and economic
  - old bachelors need affective warmth and marriage
  - women who marries too early: fatalistic suicide
    - despair
	- over-elaborate, patriarchal social regulation
  - exaggerated difference to those around them through dislocation and industrialization

### Prescriptions
- "suicidogenic" phenomenon present in end of century due to anti-social trends and utopianism
  - nourishes hatred of social bonds
  - no utopia, just isolation
- call for corporations that would improve integration
- strengthen marriage to prevent suicide
- wives? decrease difference between sexes
  - increase integration of sexes

### Reflections
- presupposes individual tendency
  - two tier model of human condition
  - individual impulses and social conditions
  - Kantian inheritance: dualism
- view social conditions as ameliorative, enhancing us
  - in general, Durkheim ignores the darker sides of social integration
  - greater social regulation might be a different way of social domination
    - what fatalistic suicide implies
