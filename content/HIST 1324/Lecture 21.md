+++
title = "Foucault: Structuralism and the Historicity of the A Priori"
date = 2019-04-16T19:55:01-04:00
series = "HIST 1324"
+++

## Introductory Remarks: 1966 L'Annee structuraliste (Or, "Kick the ball but do it *structurally*!")
- popularity of structuralism

## Foucault and Structuralism: *Les Mots et les choses*: une archeologie des sciences humaines (1966)

### Publication Success in its First Year
- sold better than hot cakes
- prestige of traditional philosophy has declined
- new set of disciplines displacing philosophy
- supercessionism
  - especially Sartre
  
### The Argument

#### The idea of an episteme, or epistemological structure
- episteme: a priori, in a sense, to knowledge
- human sciences possible due to epistemes
- an organizing grid
- why historical a priori?
  - different ones over time
  - archaeologies: earlier strata of epistemes
  
#### The priority of the human sciences
- nature, science: open question
- Foucault priarily interested in human sciences
  - criminology <-> prism
  - psychology <-> practitioners, therapeutics
  - medicine <-> not just knowledge; practice
- middle-level between logic and experience

#### Historicizing the episteme
- not just one episteme
- looks like a structure! what can be known, what classifies world, etc.
- perplexing: episteme not permanent

#### The three epistemes of the modern West
- history: story of temperal unfolding
- no narrative
- series of portraits, juxtaposing different epistemes
- motivate metaphor of archaeology
  - not that one layer influences the next
  - more of just change
- how episteme works, how it construes science
- Renaissance:
  - e.g. walnut = brain
- classical
  - systemic, crystalline rational structure
- the modern age
  - emergence of something over time
  - target of whole book
  - he still tries to make one (or few) texts
  - instantiate the same basic schema and represent whole epoch
    - Derrida

#### The human being at the heart of the modern age
- logic of expression: humanist understanding of self that grows over time
  - expressive subjectivity
- humans a precipitant of episteme
- is human self permanent?
  - what if modern episteme collapses?
  - then the historical human being would collapse as well

## Methods, Problems, Paradoxes

### Methods
- historical epistemology
  - Durkheim: socialized Kantian epistemology
  - Levi-Strauss: every culture has own epistemology (myths)
  - now at Foucault
    - talk about historical a priori
- the method of archaeology
  - the exemplarity question: is every member of an episteme an instance of its logic?
  
### Problems

#### Problems of the Historicity of the Episteme
- erosion destabilizes from the outside
- fine with radical discontinuities
- lack of concern for change and transformations
- is one episteme better than another?
  - no: lack of trans-epistemic character
- comfortable with relativism
  - no progress, no better way of knowing
  
#### Problems of the Social Character of the Episteme
- one of reasons for knowledge: transparency
- other reason: intervene and shape
  - e.g. medicine: representation and improvement
- episteme has double character
- not only regimes of knowledge but also a social intervention
  - but very unclear about intervention
- knowing the world: calling into being one way or another; constituting the world
- risks reversing knowledge and object
  - conventional: object is ground of knowledge
  - Foucault: knowledge becomes ground of object
    - why we know the world constitutes the world
    - full-blown social constructivism

### Paradoxes

#### The Paradox of Humanism and Anti-Humanism
- can human being disappear
- what is status of episteme?
  - episteme = conceptual, discursive
  - isn't that thought the quintessence of humanism?
- there must be
- there is new schema without a knower
- contingency
- humanism vanishes but intelligibility survives

#### The Paradox of Contingency of the Human Sciences
- turn human science from within
- subverting on methodological authority

#### A Concluding Paradox
- prophet of post-humanist age to come
- erasure of modern, humanist episteme
- freeing of constraints = celebration
- if a moment of liberation, then it is from an episteme
- but, can he entertain this: what would it mean to be set free if human is erased