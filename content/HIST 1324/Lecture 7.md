+++
title = "Jean-Paul Sartre and the Origins of Existentialism"
date = 2019-02-19T18:49:18-05:00
series = "HIST 1324"
+++

## Why existentialism in "social thought"?
- politically and socially amplified forms

## Introductory Remarks: The Hollow Years, France from the 1920s to the '30s

### Shifting Political Landscape
- Action Francaise decline due to papal condemnation
- then economic depression, causing socialist resurgence led by Leon Blum through Popular Front
- some militant against democracy; "Hitler over Blum"
- Vichy: collaborationist, quasi-independent

### French Culture in the 1920s
- great philosophers: Sorel to Bergson
- Proust: ISOLT, late flowering of high modernism
- French Surrealism: Breton

### Left-Bank Scene
- high watermark, congregating in cafes
- interdisciplinary
- Sartre: French resistance, flirted with socialism, anti-imperialist
  - public passion and high erudition

## Jean-Paul Sartre, 1905-1980: Life and Thought
- applied phenomenology to Nausea
- member of resistance group and taught philosophy
- quarrel with Heidegger
- existential Marxism

## Existentialism: Origins and Themes

### Existentialism in Europe: A General Overview
- Kierkegaard: returned to Copenhagen after auditing Hegel's courses
  - Hegel: world spirit as reason unfolding through human life
    - show how individual's purpose -> all would flourish
  - what Hegelian dream of world spirit and reason missed was the irreducible fact of finite or particular human
    - ultimately, to be understood in isolation or from broad reason
  - religious human beings as paradigm of finite existence, transcendent relationship with God
  - religion: individual singled out that can't be made rationally intelligible to others
    - bound to commitments that exceed human reason
- Nietzsche: atheist
  - take own power, must set free from higher metaphysical values
  - strong and unique heroic self
- Dostoyevsky: underground man
  - anguished individual who opposes all currents of modern bourgeois life
- Heidegger: systematic thinker
  - authentic human being can live out a proper understanding of being
  - "existential ontology"
- Sartre: amplify anxiety but committing to freedom
- thus, existentialism means many things, takes many forms

### The Problem of the *maitre-penseur* in French Existentialism
- Husserl and Heidegger are "honorary" French existentialists
- phenomenology as weird intruder, "magical thinking"
- but not in France; mainstream

### The meaning of phenomenology
- break away from metaphysical prejudices
- sheer description of essential structures, what appears
  - how humans experience the world
  - what it is about perception that holds together phenomenon
- focus on essences of phenomena
  - block out appearance, incidental subjective facts
  - if we clear them away, get to shared traits to all humans; bracketing
- rational enterprise
- Heidegger: human being plunged into existence
  - basic structure of human existence itself
  - plunge philosophy into concrete, everyday existence
- Sartre's traits with Heidegger:
  - condemned Kantian and bourgeois: try to gobble up the world conceptually, transforming it into mere concepts
  - alimentary(?)
- existentialism as something social: French Hegelianism
  - combine phenomenology with social struggle, struggle between the self and the other

## Sartre's Existential Novel: *Nausea* (1938)

### Journal entries by Roquetine beginning in 1932
- anti-hero in a long tradition of them
  - dissent from surroundings
  - constant complaint about bourgeois sensibilities
- live in "Muckville": inert, loses ability to act

### The encounter with sheer existence
- focuses attention onto nature of human existence and how unsettling it can be
- roots of a chestnut tree, surrealist quality of that encounter

### Critical remarks
- self wants to control world with ideas
- worldly ideas overwhelm us - experience nausea
  - inability to digest experience by reflecting on it
- reduce living to object - crab
  - inaugural moment that turns France to rationalism
  - *cogito ergo sum* dismantled
  - distinguish between passive/inert (in itself) and active/agentic (for itself)
    - inactivity consumes the self, destroys capacity for freedom
- deeply anxious about prserving human freedom

### Creativity as an act of transcendence
- self-transcendence
- symbolism: jazz as creativity
