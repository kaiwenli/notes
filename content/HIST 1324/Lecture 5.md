+++
title = "Durkheim on Metaphysics, Ideology, and Social Solidarity"
date = 2019-02-12T09:48:01-05:00
series = "HIST 1324"
+++

## Crises of the Republic
- Dreyfus Affair: secularism and republican values vs. old values of church and army
- assassinations

## Durkheim and the Ideology of the Republic

### Durkheim and Liberal Socialism
- tinged with Enlightenment ideal; no revolution
- socialism with science, the great religion
- socialism with no historical materialism
- moral constitution: all come together
- L'ecole socialiste
- syndicalism

### Ideological Polarization and Mobilization: Dreyfus Affair
- Brunetiere: individualism must have limits imposed lest it affect social solidarity
  - anti-intellectualism

### "Individualism and the Intellectuals" (1898)
- response to Brunetiere
- Durkheim will want to explain why individualism != sickness, but is the key principle of modern social solidarity
  - his individualism != egoism
  - genuine individualism of Kant, Rousseau
- conservatives: society needs higher authority, e.g. church
- Durkheim: celebrate symbolic abstraction of the individual; religion of humanity
- education has quasi spiritual task

### Laicite and Education
- Durkheim strong advocate of secularism; combat priesthood

## Sociology and Metaphysics

### The Idea of Homo Duplex: "The Dualism of Human Nature and its Social Conditions" (1914)
- Kant: causality comes from human reason
- Durkheim: causality comes from societal forces
- concepts have a social character
- neither aspect of our beings are wholly satisfied; in conflict
- Kant: human reason the higher faculty
- Durkheim: why do we prescribe so much dignity in ideas?
  - owe orderliness to societal structure
  - we are experiencing our obligation to one another

### Dilemmas of Socializing Epistemology
- what does the individual correspond to? what is it that is being represented when people worship the individual?
- representatives of the actual social order. that is why we should worship individualism

### Durkheim's Legacy
- society is a benevolent god
- later on, ambivalent god
- being human participant in higher societal structure
- maybe seen as too old-fashioned and optimistic
