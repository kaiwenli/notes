+++
title = "Existentialism, Freedom, and Marxism"
date = 2019-02-28T22:07:37-05:00
series = "HIST 1324"
+++

## Sartre, Existentialism, and French Politics
- criticized as insufficiently militant
- existentialism as the philosophy for the future

## Sartre's "Existentialism is a Humanism" (1945-6)

### History of the Text
- gave lecture in person
- election soon: hope for a coalition of socialists, social democrats, communists
  - humanism as mediating thing between two extremes

### Sartre's Adversaries: Left and Right
- communists thought of themselves as agents of resistance and those who suffered most
  - existentialism: thought divorced from action
- catholic socialism
  - social solidarity available only at religious level
- thus, lecture was politically strategic

### Chief Arguments of the Text
- human has no metaphysical security
  - abandoned to own condition
  - existence precedes essences
- human self-creation is all we have
- existentialism turns toward society: promotes universal, collective ideal
- existentialism begins with individual but doesn't end there
  - intersubjectivity

## Existentialism and the Question of Marxism

### Political Events and Interpretations
- Sartre understood Marxism's power as a competing ideology
- 40s-60s: decline of organized communism
  - show trials

### Utopianism, Idealism, and Blindness
- choices go beyond US and USSR
- 1947: sustain existentialism of polarizing landscape
- 1952: class requires organization (somewhat communistic)
- French sympathies with communism divided

## Albert Camus (1913-1960): The Loneliness of Moral Dissent
- Algerian
- active in resistance
- novelist

### The Critique of Marxism
- resists Marxist language; history isn't decisive factor
  - rediscover deeper morals in revolution; call for moderation
- break with Sartre: cruel review of *The Rebel* through Jeanson

## Merleau-Ponty (1908-1961): Phenomenology and Politics
- *The Phenomenology of Perception* (1945): emphasizes human embodiment
- from aesthetics to politics to structuralism

### Marxism and Humanism
- similar to Sartre: communism and capitalism are false choices
- 1953: broke away from Sartre
  - skeptical reflections on Marxism
