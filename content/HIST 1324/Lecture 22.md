+++
title = "The Philosophy of May '68: Anti-Humanism and Emancipation"
date = 2019-04-18T19:55:01-04:00
series = "HIST 1324"
+++

## Introduction: The Revolutions of May '68, or, "Sous les pavés, la plage!"
- emancipation from structures, traditions
- post-structuralism: no escape
- ethos of student rebellion and romanticism vs. romanticism is illusory
- anti-humanist message that human can disappear

### The Events
- year of global importance
- Dubcek; Prague Spring, "socialism with a human face"
- French students and labor unions demonstrate
- came to alliance
- Dubcek removed from power

### Interpretations
- what is anti-humanism?
- going back to Sartre and Heidegger
  - Heidegger: humanism is arrogance
- new anti-humanism due to structuralism
  - subject not in charge
  - structure is deeper
- self is a precipitant of structure

## Louis Althusser and Structuralist Marxism

### Biography
- affiliated with far-left
- educator at ENS

### Structuralist Marxism
- young Marx, according to Althusser, was still a humanist (before 1845)
  - Hegelian philosophy
  - geist, spirit
  - collective humanity trying to realize its intentions and become free
- then, scientific theory of society (1845-Kapital)
  - no longer humanist
  - no more humanist illusions
- epistemological rupture
- spinoza denies Cartesian dualism; monist
- Althusser mobilizes Spinoza
  - structuralist reading of Marxism
  - self no more than an instance of a structured whole

### The Idea of the Ideological State Apparatus
- parallels to culture industry
- interpellation or "hailing": call to order by the state
  - individual constituted as political subject
  - complicit

## Jacques Derrida and the Ends of Man
- still waging endless debate over humanism
- claims Heidegger entangled in humanism
- what "we" want to accomplish
- isn't that "we" still humanist?
- to raise question of Being is humanist
- emphasizes Being's proximity
  - if Being is highest and most important, and humans live closest to it... implications?
  - proprietary relationship where human owns Being
- Nietzsche offers possibility of complete irony of own entanglement
  - humorous remedy for sense of failure

## Roland Barthes and the Death of the Author
- semiotics
- anti-humanism to literature
- author not wholly in charge, not final *author*ity
- see meanings proliferate

## Concluding Remarks: What are the Politics of Anti-Humanism?
- for some, decidedly quietist
  - challenge its emancipation
- Foucault, "What is an Author"
- idea that anti-humanism and liberation marred together
  - this is temporary