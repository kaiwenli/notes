+++
title = "Foucault, the Will to Knowledge, and the Invention of the Soul"
date = 2019-08-25T19:18:17-07:00
series = "HIST 1324"
+++

## Introductory Remarks: French Society and Thought in the 1970s and 80s

### The New Politics of the 70s and 80s
- Pompidou dies in office
- oil crisis

### New Themes
- dissolution of left
- rebirth of liberalism
- from revolution to ethics (Levinas)
- the anti-totalitarian moment
- the turn to religion

## Michel Foucault in Later Years
- new interest in power
- new methodology of genealogy
  - interested not specifically in prison, but paradigm of it
- rather than address "universal" concerns of humanity, addresses "specific"
- active in campaign of Soviet dissidence, non-Communist labor union, Solidarity in Poland

## The History of Sexuality, I: The Will to Knowledge (1976)
- Lacan looms over this book
  - direct rejoinder, but much grander

### Argument Against the Repression-Model
- psychoanalysis says humans suffer due to repression
  - affliction from Victorian era is due to repression
  - super-ego hold repression back
- if self can understand repressed desire, then self can become free
  - knowledge can undo authoritarian agency within self
  - knowledge dissolves illegitimate power
  - power works through secrecy
- anticipated by the confessional
- truth, hidden reality, hidden deep within self
- we take pride that we take part in sex-liberated society
  - the more we speak, the more free
  - but we haven't succeeded in leaving Victorian age since we still believe in same premises

### A Counter-Proposal: Knowledge as Power
- making sexuality announce itself: the "putting into discourse" of sex, an "immense verbosity"
  - we talk incessantly about sex since we think it is a truth of self
  - motivates thought that all we need to do is speak about it
  - in doing so, we normalize it, lend it greater definition, makes it available for forms of public power
- admin, discipline, power
- desire is a ruse that power uses to justify exposure
  - power implants what it claims to discover
- what work is done for power by psychoanalysis?
  - be suspicious of this
  - psychoanalysis gain authority over us by claiming to know truth of human beings
- entire framework of modern era says knowledge is opposed to power
  - on the contrary, Foucault says knowledge is a form of power
  - lays claim to knowing a hidden truth; discursive authority

### A New Model of Power
- we think of power as transcendent to the field it operates on
- instead, think of it from immanent, circulates through a social reality
  - capillary power
  - not emanating from any one place
- bio-power and bio-politics: population control, eugenics, sexual normativities (etc.)
  - strong continuity between fascist and liberal regimes
  - genocide and population management pervade fascism and liberal societies

### The Possibility of Resistance
- conventional view:
  - sex is the actual/physical phenomenon
  - sexuality: how a culture imagines sex, the cultural construction
- Foucault tries to invert that relationship
  - no sex without sexuality
- what kind of claim is that?

## Open Questions

### A Genealogy of Psychoanalysis as a Liberal-Emancipatory Model of Knowledge
- genealogy of hermeneutics (interpretation)
- psychoanalysis tells you of hidden self
  - claims to decode hysterical symptoms to get at hidden truth
- belongs to tradition of hermeneutics of suspicion
  - Marx, Nietzsche (will to power), Freud (sex desire)
- enforces distinction between surface and depth, signifier and signified
- picks up where Order of Things left off
- challenge that interpretative art
  - ruse for modern discourse and power
- they claim to be knowledge of what is hidden
- that basic strategy authorizes certain methods of discourse, and therefore social power
- seems to reject idea that speaking of truth emancipates
  - rejects the idea that sex precedes sexuality

### A Postmodern Nominalism and Two Possible Readings
- real human experience is action under a description
  - experience isn't anything unless it's under a description
  - cultural embroidery of sexuality
  - sex conditional upon sexuality
- first reading (extravagant, implausible)
  - sexual reality becomes the *consequence of* sexual discourse: there is no sex "in itself" prior to discursive sexuality
  - theory becomes ultimate reality?
    - turning back on facts of human suffering
- second reading (more plausible)
  - non-discursive reality simply *doesn't count as (isn't known as and descriable as)* experience
  - the non-discursive dimension remains real but discursively inaccessible (or, at least, *inaccessible to us as discursive agents*)
  - epistemic, not metaphysical, anti-realist

### Dissolving the Durkheimian Dualism
- we become human when elevated into symbolic realm, discursive realm
- pushes against dualism
- have we entirely lost the erstwhile liberal-emancipatory and critical function of the real?
  - not entirely
  - momentary glimpse of non-discursive, brute reality
- bodies and pleasures: a non-discursive utopia
  - bodies and pleasures resist modes of discursivity
  - still commit to Kant-Durkheimian dualism?
    - body and pleasure vs discursive beings

### Gender as Performativity: Lessons from Judith Butler
- even embodiment dissolves into discourse
- gender, desire becomes normativity
- no longer to liberate through meaning/knowledge

### The Critical Role of Disclosive Knowledge: A Feminist Objection
- risk: Foucault reaffirmed public and private, which has long supported patrimonial domination and sexual oppression
- 1876: Lapcourt, turn against state intervention
  - liberal feminists permitted to remain silent
  - legal, medical, state intervention an urgent necessity

## Foucault's Final Turn
- "care of the self"

## Concluding Remarks: From Durkheim to Foucault... and Back Again?

### The "Bad" Side of Social Solidarity
  - Durkheim: a Third Republic optimism
  - Foucault: a late-modern pessimism
  - social solidarity in modernity as erasure rather than enhancement of our freedom

### The Genealogy of Humanist Normativity
- Durkheim: the "sacred" norms that make our common life possible: the religion of humanism
- Foucault: post-Nietzschean genealogy: unmasking the religion of humanism and the ideal of the human being

### The End of Dualism
- Durkheim: dualism in organic solidarity: the rise of social bonds and the rise of the individual
- Foucault: monism in (neo-mechanical) solidarity: social bonds and the social constitution of the individual

### The Return of Positivism
- the question of the sources of normativity
- if genealogy is an exhaustive practice that exposes all normativity as power, then what are the aims of criticism?
- Foucault's dilemma: the collapse of normative language into facts of power
	- what we think is humane an elaboration for power
	- but need some confidence in ideals
	- what language of justification exists?
- Comte's return?
- Foucault's emancipatory spirit
- old themes, old problems, remain
