+++
title = "Existentialism, Alterity, Feminism: Beauvoir"
date = 2019-03-12T14:02:22-04:00
series = "HIST 1324"
+++

## The Trajectory of European Feminism: the Three 'Waves'

### Feminist Inheritances: A Universalist-Egalitarian Premise (democracy, suffrage)
- basic rights secured in early republics
- Wollstonecraft: plea for civic emancipation
  - no challenge
  - elevated from deformed condition
  - reflection of cultural improvement, typical of 18th century
- Gouges: image of men != benevolent group
  - men as tyrants
  - premise is still that of an universal egalitarian system of rights
- Woolf: untroubled by the "given" of property

### Simone de Beauvoir and the 'Second Wave'
- de Beauvoir: distinction of being woman
  - self-transcendence
- similar fate to Sartrean existentialism
  - lose salience in 70s

### Post-Existentialism, the challenge to humanist patriarchy, and the 'Third Wave'
- challenge humanism itself
- women need to claim distinctive identity
- embrace feminine difference

## De Beauvoir: Life, Politics, and Publications

### Early Years
- traditional family, went to ENS, met Sartre, taught
- involved in resistance

### Politics
- spoke on TV, radio frequently
- demonstrations
- protested against oppressed people of all categories

### Final Years
- tribute to Sartre
- dies 1986

## *The Second Sex* (1949): Arguments and Themes

### Existentialist Premises
- willing to deploy strong existential distinction between "en soi" and "pour soi"
- dualism
- no such thing as a feminine essence
  - might seem like universalist first wave
  - anything that a woman is is what she becomes
  - Sartrean

### Woman as the Other
- alterity: an universal fact
- male/female: metaphysical structure principle
- not occasionally different from men, but essentially different from men, immanence to male transcendence
- self-reflexive: existentialism itself needs to be interrogated

### Social Construction of Woman
- nothing about woman is eternal
- reflect a situation
  - "situation": charged with existential meaning
  - Sartre: culture and history congeal around us that we transform
  - thus, open to revision
- attentive to all the ways feminine alterity shows up in human experience

### Basic Structure of the Book
- one: facts and myths
  - beliefs that define woman's being
- two: take on surrounding beliefs in own abjection
  - coerced into complicity
- myths of sexism, patriarchy pervade on such a deep level that women are enthralled by freedom

### Conclusion: "Toward Liberation: the Independent Woman"
- what unites men and woman greater than peculiarities
- premise: aligns her with existential humanism of Sartre while also tracing back to universalist Wollstonecraftian principles
- dream of truly reciprocal recognition
  - strongly heteronormative
- all male and female essentially alike

## Concluding Questions
- biological sex v. constructive gender
- are women prone to immanence?
- self-reflexive interrogation of existentialist categories
- immanence as abjection and passivity
  - does this transcendence v. immanence begin to collapse
  - new vision of human being that will endorse immanence in itself
  - registers in some ways with Heidegger
    - human itself a paradigm of domination
	- prof's speculation
