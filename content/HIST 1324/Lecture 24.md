+++
title = "Jacques Lacan: Structuralism, Psychoanalysis, and the Symbolic"
date = 2019-08-25T18:58:38-07:00
series = "HIST 1324"
+++

## Introductory Remarks on French Thought in the Postwar Era: The *Longue Duree*

### Major Patterns: from Sociology to Existentialism, from Structuralism to Poststructuralism
- human ego is illusion, reflected infinitely in a hall of mirrors
- why psychoanalysis?
  - too easy to see clearly defined movements
  - palimpsest: traces remain of previous ones
  - amazingly influential
- psychoanalysis contests primarily of ego, ego constructed out of deeper imaginary codes

## Jacques Lacan

### Biographical Remarks
- in a way, moving back a generation
- born 1901

### Three Broader Streams of Influence

#### French Surrealism
- automatic writing: gain access to unconscious to let it speak freely
- dream-like images

#### French Hegelianism
- Kojeve, drama of self and other
- constitutive recognition as a subject
- twist it into a story about failure; misrecognition
- illusions that establish selfhood

#### French Heideggerianism
- effects we have on world do not come from selfhood
- Lacan challenges humanist self, Cartesian transparency
  - instead, fundamental opacity

### The Specific Inheritance: Structuralism Meets Psychoanalysis

#### Freudian Psychoanalysis
- returning to "original scripture"
- Freud: clinical setting, patients afflicted by hysteria
  - self-like hydraulic mechanism; imbalances; psychic pressure
  - repression, not permitted expression
  - result of blockage is manifestation of symptoms
  - undo that blockage by permitting patience to speak freely, signs of desire, under repression
- living truce: id, ego, super-ego
- operate reasonably between demands of reality and instinct

#### French Revision
- American: fortify ego, Hartmann, ego psychology
- Lacan challenged that: "I is an other"
  - ego not ultimately in charge
  - id is
  - impossibility of self-mastery
- manipulate transference so that patient begin to understand abyss within self

## Key Themes of Lacanian Psychoanalysis

### The Imaginary
- ego
- image; specular
- forms at earliest stages of childhood
- original experience of alienation
  - child sees image of own body
  - at first does not recognize
  - see vs experience
  - misrecognition, ego forged out of that
  - subjectivity challenged
- lack constitutive of all social identity
- core of psyche -> ego -> spawn of something imaginary -> misrecognition -> very core of what we mean by self

### The Symbolic
- super-ego
- structured like a language
- ego, interact with super-ego, which is structured like that
- semantic associations; puns
- language is system of differences
  - if unconscious thinks through languages, it thinks through differences
  - signified inaccessible
- human being born into world of signification that exceeds our capacity to control
- symbols envelop the life of man
- extends Durkheim's symbolic self
  - psyche and social
  - almost theological: language has created us
- little other in subjection to big other
- can't fully bridge ego and super-ego

### The Real
- id 
- truly unrepresentable, unmasterable
- any encounter with the real will be traumatic
- challenges therapeutic of Freudian psychoanalysis
  - can't be integrated into the self
  - unrepresentable, cannot symbolize
  - opacity within self
- the real binds all threads together

## The Politics of Psychoanalysis in France
- Lacanian psychoanalysis challenged authority of bourgeois ego
- what place in social theory?
  - Zizek
- for Foucault, psychoanalysis complicit to bourgeois convention
