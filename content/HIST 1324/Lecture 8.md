+++
title = "Alexandre Kojeve and the Origins of Existential Marxism"
date = 2019-02-21T18:49:18-05:00
series = "HIST 1324"
+++

## France in the Later 1930s: A Short Recapitulation
- unemployment and fall in GDP, though not as bad as USA
- economic recovery by 1938 via Keynesianism
- socialists and radicals united behind Blum
- resurgence of anti-democratic right

## French Intellectuals in the 30s: Religion, Existentialism, Hegelianism

### France in the 1930s and the Rebirth of Religion

#### Neo-Thomism and Existential Catholicism
- fuse modern philosophy and Thomism

#### The Kierkegaard Revival and the Young Hegel
- turned to Hegel's young theological writings
- not the Hegel of world reason
- instead, tinged with religiosity, allied with his greatest rebel, Kierkegaard
  - human finitude, desire, passionate existence
  
### French Existential Hegelianism in the 1930s: "Toward the Concrete"

#### Emergent Theory of Social Being
- Heidegger: social being - neutral/shared social field, the one
  - common horizon of intelligibility
  - lacked intersubjective dimension; face-to-face relation

#### Turn to Hegel for a Theory of Social Being
- self and other: within subjects

#### Origins of the Other: from Levinas to Lacan
- Rimbaud: "I am the other"
- Lacan: unconscience as the other within the self

#### Major Exponents of Hegelianism in France
- direction to the concrete

## Kojeve and French Hegelianism at the Ecole Pratique des Hautes Etudes
- born in Russia
- French Hegelianism center of Parisian intellectual life
- became statesman - Ministry of Economic Affairs, European economic policy
  - Marxism declined
 
### The Hegel Lectures
- theoriest of human desire and passion
  - controversial reading
  - manifests consciousness in realizing its wants
  - not in itself - for itself
  - realize desires in external world
  - human poised between activity and passivity
  - Hegel quote: charged with task overcoming passivity
- deepest desire: have one's own selfhood actualized
  - Hegel's Dialectic of Recognition
  - to be a self cannot be to attribute identity to one self; must have objective reality and be recognized out there in the world
- master and slave
  - contest - very sense of being of self at stake
  - philosophical thought experiment: what it looks like to enter into social relations
  - to be a self is to be recognized as social being
  - why master and slave?
    - everything is at stake: one victorious, one submits
  - master (provisionally) wins because slave recognizes master's consciousness
  - master's view of slave: slave has no agency at all
  - what is unsatisfying: master has gained recognition from a subject that he doesn't recognize as a subject; slave therefore always insufficient, never full-hearted
  - this dialectic does not achieve what master wants
- it is the slave that gains deeper satisfaction
  - he works on material reality
  - manifesting in the concrete
  - slave, despite unfreedom, manifests self in material labor
  
### Ramifications
- labor as original site where humans struggle and achieve agency
- Hegelianized narrative that the prole, in misery, experiences itself as an agent, and that it is the hero of history
- political implication: slave represents future
  - Marxist affirmation of history
  
## Sartre's Philosophy between Existentialism and Hegelianism: Some Introductory Remarks

### *Being and Nothingness*: Basic Premises
- humans in either passivity or action
  - being-in-itself, being-for-itself
- transcend passive state: essence = what you make of yourself by acting in a state of freedom
  - existence in itself: absurd
- human transcendence: negate own passivity
- you can be oneself or deny this choice: bad faith

### Transcendence Transcended: The Problem of the Other
- other looks like an object but from own point of view is subject
  - "crack in my universe"
  - other has same task
  - represents threat
  - retelling Hegel's master and slave
- the "look": way in which each subject reduces other to object
  - deep existential threat to sense of being
- reduce for-itself-ness
- existential drama

## Section notes

### Why does Hegel talk like this?
- examine historical context
- Kantian audience
- 1807: *Phenomenology* first book, didn't get success
- imagine *Phenomenology* as Bible: how consciousness begins
  - not chemically but as a "Bible" moment
  - what would it look like?
    - a dramaticized account
    - how does consciousness come into existence?

### Next generations of Hegel
- Hegel as social philosopher
- Young Hegelians
  - predecessor to Marx: the two classes
- why Hegel?
  - think of social relations beyond Marx due to Russian Revolution

- Hegel and interpretations' implications: we need each other to exist, own consciousness depends on us being together