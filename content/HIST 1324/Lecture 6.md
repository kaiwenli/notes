+++
title = "French Social Thought after Durkheim: The Reproduction of Social Solidarity"
date = 2019-02-14T11:00:52-05:00
series = "HIST 1324"
+++

## Marcel Mauss

## Context of *The Gift*
- shadow of WWI
  - Mauss deployed as interpreter
- events, especially at the end, act as background
- associated with legal scholars with regards to war reparations
  - collective commitment: moral and political question rather than how much money Germany needs to repay
  - what is owed? why is it owed? how could transfer of material good do immaterial things?
- 1923-24: Mauss was engaged with socialist politics
  - bottom-up socialism: unions
  - how might we reimagine the market if it's here to stay?
    - compare and contrast with Russian Revolution

## *The Gift*
- intervening in long-standing debate about primitive economies
  - bartering: once we trade, we go on with our lives
  - one gift -> counter gift; gift never repaid
  - show that earliest exchanges didn't need state intervention
    - might be same in the future
  - did not serve immediate self-interest

- total services of an agonistic type
  - total: filled with religion, social structure, economics, etc.
  - agonistic: involve contest/rivalry; opting out comes at a price

- barter isn't the original contract; it's the gift
  - have spiritual power that reside in things
  - when I give something, I'm giving you a part of myself, you're obligated to return some back

- Marx: theory of alienation: only get wage, separated from work
  - way to critique capitalism
- Mauss: something to return to; problem: we exchange things without acknowledging the immaterial parts of it
  - thus, wage is not sufficient
  - makes case for social insurance
- objects have character beyond instrumental use

### Economic contracts
- why gift?
  - idea of pure gift stands opposed to pure self interest
  - gift mirror of economic contract
  - challenge this distinction between these two
  - approach problem from other direction: challenge the contract

### Shared elements
- selflessness: e.g. rethink taxes
  - exchange in ideas through material transactions
    - not that there are generous gift givers then everyone else
	- myth to think we can divorce who deserves what
- obligations that are open ended
  - transactions are let open

### Cohesion
- gifts as source of obligation
- if debt is never settled, then basis for ongoing relationship
- social cohesion centered around things
  - glue = things that can be exchanged and opportunism to come together to exchange them

## Legacy
- Levi-Strauss: Mauss as precursor to his structuralism
  - reduce social life to object, "world of symbolic relationship"
- anthropology: material culture, trained fieldworkers
- darker implications: bought and stole a lot of artifacts; oppressive/fascistic tendencies
