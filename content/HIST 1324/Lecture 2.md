+++
title = "Durkheim and Society as an Object"
date = 2019-02-02T05:34:30-05:00
series = "HIST 1324"
+++

## France wasn't all taken ahold by modernity

  - e.g. Dreyfus Affair
    - church + state v. secularists (e.g. Zola)
  - l'Action Francaise: right wing

## Positivist

  - Comte: intellectual legacy important to understanding Durkheim
    - important believer in modern sciences
	  - can be used to understand human conduct
	  - believe in universal science
	- hierarchy: math/physics -> bio -> sociology
	  - sociology a secular religion for humanity
	- social physics: humans can be understood as molecules
	- moral statistics: societies exhibit irregularities that might
	  - can study society as its own entity

## Durkheim: Overview

  - like Comte...
    - thinking like mathematician
	- society is external to individual
  - his attitudes toward Comte changes: social facts to social representations
    - symbolic, collective beliefs

## French Educational System

  - dramatically centralized: Paris
    - most of the luminaries are found in one or two of the "grands ecoles"
  - aggregation: to teach in a lycee
  - Durkheim studied under Renouvier and Lachelier

## Durkheim: Life and Work
  - alliance between Jews and progressivism
  - certain kind of renovative Kantian philosophy would be important to Durkheim
  - suicide: how such a personal phenomenon can be viewed through sociological lens, statistics, etc.

## The Rules
  - appeal to objectivity
    - appeal to science
	  - invoke spirit of Descartes
	  - Cartesian: doubt, build up
	    - doubt conventions from religious past
  - human sciences and social science -> united
    - in contrast with Weber: humans are endowed with meaning
	  - hold apart the two; cherish what's distinctive about humans
  - Durkheim, following Comte, follows unity
    - affliction of human sciences: distorted by moral ideas
	  - moral ideas are prejudices
	    - hold them aside
		- world of social facts
	- social facts != aggregation of individual facts
  - idea of social facts
    - facts that bear independence from individual
	- social realist: one ascribes to social independence; hard and fast character you might ascribe to a lectern
	- social functions as if higher, distinct from us
  - major characteristics of the social fact
    - externality: in relation to individuals
	  - a social fact seems to exist in some sort of external space
	  - social facts are constitutive of individuals
	    - e.g. education
		  - educated individual comes second; comes after the whole program
		  - the individual bears the stuff(?) by the system
		  - therefore, the system comes first
		    - rightfully, individuals make it up so system isn't metaphysically independent
	 - things can't be understood if we only focused on individuals
  - constraint: regulative status
    - if you want to know why society has a constraint, see what happens when you try to violate social norm
	- while apparatus of belief and institutions compel us to behave in certain ways
	- stay in narrow channels of the normal
  - neo-Kantian character
