+++
title = "Existentialism, Alterity, Colonial Rebellion: Fanon"
date = 2019-03-14T14:19:19-04:00
series = "HIST 1324"
+++

## Alterity and Hybridity in Post-War French Thought: Themes and Variations

### The Other as a Category of Philosophical and Social Analysis
- Levinas: alterity = metaphysical condition
- Sartre: self-other relations are always conflictual
- de Beauvoir: loses terminal quality; how much can otherness be modified?
  - no longer an invariant; sociocultural category
  - women constituted as the other
- when using the other so foundationally, it becomes hard to address it, undermine it

### 'One is not born but rather becomes a woman': the Other as a Constitutive or a Political Condition
- Fanon: colonial subject as other needs to be dismantled

## The 'Mission Civilizatrice': A Brief Look at French Colonialism in Algeria

### The Early Phase of Colonialism
- justified with Enlightenment: encountered uncivilized people, benefit from learning from French
- rivalry with England
- 1848: Algeria declared integral part of France
- didn't question French staying there but questioned their means

### The Birth of the Algerian Independence Movement
- VE day, May 8, 1945
- birth of FLN and ALN by 1954

### The Algerian War, 1954-1962
- already had great-grandchildren French established there
- pied-noir: jealous of their home
- capture Algerians: tortured, concentration camps
- many pied-noirs fled back to France and formed right-wing bloc

## Alterity and Revolt: Fanon and the Predicament of the Colonial Subject

### Frantz Fanon, Life and Work

#### Biographical: a childhood in Martinique, psychiatry at Lyon and in Algeria, and subsequent radicalization
- born in Martinique in 1925
- slavery abolished in 1848
- quasi-feudal structures of enslavement still present
- psychiatric periods in Algeria that he was eye witness to colonial experience
- died in 1961
- complex legacy: advocated for 'organic' violence

#### Publications: from diagnosis and a theory of alienation to outrage and a theory of violent revolution
- broaden into grand nationalist statements

### Alterity, Hybridity, Commitment, Violence

#### The Experience of the Colonial Other: *Black Skin, White Masks* (1952)
- North African syndrome: trauma of colonial mind affects both mind and body
- not only psychiatric categories but also philosophical terms: Hegelian encounter between master/slave
- negro always preoccupied with self-reflection, can never live up to it
- phenomenological: capture experience
- trauma is historical, hardened into bodily experience
- strong opponent of romanticizing racial essence, black essence; negritude
- colonial subject expresses hybridity: forms of identity through historical interaction
- recognize what is constructed of the colonial subject's identity
- self-creation: refusing to be passive

#### From Diagnosis to Violence: *The Wretched of the Earth* (1961)
- by beginning of 1960s, his radicalism moves from diagnosis to strategizing
- affirm violence as a reclamation of subjectivity
- realist tone in midst of bombing
- Sartre: seeks to construct situation through Hegelianism
  - brutality of structure of violence
  - attempt through philosophy to describe graphic circumstances

## Concluding Remarks

### Intellectuals and Violence
- when to put down the pen and pick up the word?
- Fanon, Sartre: provoked great debate

#### Albert Camus: "Letter to an Algerian Militant" (1955)
- response to Fanon
- pied-noir himself; Algeria belonged to France
- ambivalence (to put it mildly) about Algerian independence
- unsettled by Fanon's recommendation of violence
- moderate it by endorsing the right

#### Hannah Arendt: *On Violence* (1970)
- "glorifies violence for its own sake"
- misrepresentation: Fanon only sees it as a way to undo colonialism
- alternative to non-violence rarely present
  - civil disobedience was an accepted truth in the case of Gandhi and UK
  - that is why it worked

### From Existentialism to Structuralism
- political challenge: universal humanism
- sense that France represented universality
  - revealed to be torture, degradation
- partly false; Eurocentric language
- theoretical challenges: assertion, transcendence seemed impossible
  - does not capture we are all born into structures we did not create
  - they constitute us as social beings
  - to understand human being != complete self-invention but one that embodies a structure of codes
