+++
title = "Foundations of French Structuralist Anthropology"
date = 2019-03-28T15:19:48-04:00
series = "HIST 1324"
+++

## Society and Language: Saussurian Origins of Structuralism

### Saussure's Model of Language: A Brief Review
- *parole* and *langue*
- synchronic and diachronic
- system of signs
  - signifier and signified
  - relation between the two are arbitrary

### Theoretical Implications
- realism: reality *sui generis*
  - Durkheimian dualism
- lack of reference to history
- internal sign arbitrary, entire system exhibits rigidity
- 'general semiology'

### The Rise of Structuralism in Postwar France

#### Favorable review of Levi-Strauss by de Beauvoir
- power of science
- dreams of cybernetics
- math and logic to create a better world
- pivots: "broad humanism"
- kinship with existentialism
  - affirmation of existentialist philosophy

### A Change of Perspective
- incompatibilities with existentialism
- Levi-Strauss' attack on Sartre
- Sartre no longer happy: social sciences replace philosophy
- apply structuralism to human culture

## Claude Levi-Strauss, Introductory Remarks

### The Birth of an Anthropologist
- born in Belgium in 1908
- same generation as existentialists
- conscious heir to Durkheim, Mauss

## The Early Work

### Principles of Structural Anthropology
- culture has a mathematical shape to it
- create model of what human culture is
- anthropology finds underlying concept in culture, makes it explicit, create model
- we operate with finite signs
- culture has a kind of "economy"; finitude of signs and the need to be economical
  - musicians compose on finite tones
  - chemical combinations
- humans don't have infinite power of self-creation
  - rules for combination
  - huge contrast with Sartre

### *The Elementary Structures of Kinship* (1949)
- kinship defines rules for exogamy
  - prohibition of incest as foundation of culture
    - transcend nature into culture
  - Maussian exchange of people on obligatory rules
- kinship key to transcendence: nature to culture
  - consanguinity: humans transcend natural conditions
    - cannot just refer to biology
  - prohibition of incest: link between nature and culture
- human culture is a set of codes
  - all others comprise human [?] as such
  - they don't stem from intention
    - distinction between sacred and profane

### Themes and Polemics
- all human cultures have rich, diverse systems of meaning that are fundamentally same
- general codes and structure that all civilizations share
- all humans are cultural beings
  - myths create an intelligible world for us
  - live by and through those myths
- codes and rules develop of their own accord without rational intention
- Ricoeur: "Kantianism without a transcendental subject"

#### Universalism through Relativism
- fierce critic of Western triumphalism and of Eurocentrism
- anthropology helps to relativize cultural commitments
- appreciate universality of codes through this relativism
- Callois: Levi-Strauss caught in contradiction
  - promoted generous relativism (subject to structuralism) but this science is unknown to people he is studying
- nostalgia as temptation

## Autobiography as Reflexive Ethnography: *Tristes Tropiques* (1955)

### The Burden of Fieldwork: Claude amongst the Nambikwara and Tupi-Kawahib (Indigenous Tribes of Brazilian Amazon)
- reflexive ethnography: learning about others to learning about self

### The Melancholy Task of the Ethnographer

#### "The Writing Lesson"
- analysis of culture may be participation in destruction of it
- precursor: great self-reflection in anthropology
- complicit with colonial destruction

#### "A Little Glass of Rum"
- modernization as cultural erasure
  - rationalizing, sophistication
  - destroy local traditions
  - melancholy, nostalgia, fatalism
  - Rousseau: language as original sin, opened up doors to comparison
