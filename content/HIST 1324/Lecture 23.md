+++
title = "Foucault: Social Order and the Critique of Modern Surveillance"
date = 2019-04-23T19:55:01-04:00
series = "HIST 1324"
+++

## Introductory Remarks: Foucault in the 1970s

### From Structuralism to Genealogy
- genealogy, from Nietzsche
- historicized structuralism
  - to address unsatisfactory: ruptures in epistemes, but how did that happen?
  - simply contrasts them
- became more aware of human sciences as social practices
  - from epistemes, neutral, to discourses
    - knowledge and power composite
  - techniques of social exclusion
- genealogies of power, of reason and truth
  - pervades social reality, constituting social reality
  - brings into being the object of social experience through power
  - Nietzsche understood knowledge not neutral, but social power

## A New Vision of Society: Surveillance, Panopticism, Punishment

### *Surveiller et punir: Naissance de la prison* (Gallimard, 1975)
- understood connections between social theory and social reality
- worried about prison reform, afraid government would not allow activists to change, instead co-opting movement
- publication success

### The Argument: from Spectacle to Surveillance

#### Punishment was a spectcle
- the regicide; Damiens, a domestic servant
- gruesome
- physical body
- spreading ashes to the wind; assert royal power, decimation

#### Surveillance
- less than ten years after Damiens
- temporality becomes new instrument of control
  - it can control time, then reduce human subject, time no longer humanist self, free; can't realize self no more
  - temporality is where self discovery occurs
- timetable, rationalized measurements
- prison is a closed universe, but prisoners are always visible to guards
- no violence to body, but pure vision
- prisoner's soul

#### Panopticon
- new model not just for prison, but for how power works
- power works its effect without direct application of violence
  - structure itself works its effect
  - pure visibility
- the gaze can objectify us
  - but goes further than Sartre
- here though, no other subject
  - no person is even needed to have its effect
  - geometry itself does the work
  - no agency is the source of power

### A New Model: the Disciplinary Society
- not just targeting prison
- all of society is carceral
- replicated in all major public institutions
- sees epistemes as implicated in social practices
  - power-knowledge
  - all human sciences are converging on "knowable man"
    - bringing into object of control
    - object of discipline
- e.g. psychology: normatively rich, how you should be if you are psychologically healthy
  - in those fields, what is the proper way to be?
  - inverts Durkheim, who thought that social solidarity a good thing
    - healthy self-regulating mechanism
    - no bad social solidarity, though hinted at with "fatalistic suicide"
    - social solidarity contrains (dangerous use of that term; see later)
- general inversion of "brute humanitarian sentiments"
  - e.g. prison reform a good thing
  - sanitized, orderly prison life
  - should be as unsettling as the irrational violence Damiens subjected to

### Methodological Considerations

#### A new model of power
- we still think in terms of *ancien regime*
- think of power as systemic, immanent
- overcome Hobbes
- "capillary" power, without center, emmantes them everywhere

#### The productivity of power
- power produces reality
- in what sense did discourse produce the criminal?
- prisms reproduction of criminals; recidivism high
  - factory
- Aristotle: carve joints
  - no joints to carve in social world
  - every classification will be right in a different way
  - e.g. prisons treat criminals in a particular way that define a prisoner as prisoner
    - "looping effect"
    - not just known as, but treated as, a criminal
- nominalism: "criminal" refers to a wide array of definitions
- power produces its object

#### Overcoming Durkheimian dualism
- individual is precipitant of society
- "internal realism"
- erase distinction between the two selves, metaphysical and social, that Durkheim cherished

#### A gesture of defamiliarization: a genealogy of bourgeois society and bourgeois ideology
- new regime should unsettle just as much as old
- don't let naive humanism get to you
- if Durkheim thought rationalization good, Foucault presents different story
  - Weber thought of occidental rationalism of bureaucracy, capitalism
  - rationalism no longer had the least hint of improvement for Foucault

## Concluding Questions

### Pure Description and the Question of Normativity
- sci. genealogy of human sciences
- does he suggest what non-disciplinary society would be like? emancipation?
  - they obstruct the outside... now what
- suspicious of all normative language
- Habermas: Foucault's "crypto-normativity"
  - adopt cool rhetoric who is merely describing

### An ambivalence of style
- anarchy, lyricism in describing power
- fascination with domination?
  - Foucault the ultimate skeptic without recommending an alternative