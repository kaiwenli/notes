+++
title = "From Structuralism to Post-Structuralism"
date = 2019-04-04T19:55:01-04:00
series = "HIST 1324"
+++

## The Birth of Post-Structuralism
- skeptical toward scientific pretensions of structuralism
- not really separate from structuralism

### Post-structuralism Ascendant: or, 'The Philosophy of '68'
- Ferry and Renault: skepticism toward naive humanism
- internal critiques of metaphysics
  - humanist premises with anti-authoritarian effects
  - challenge metaphysics of the self
- language as system of differences
  - furnished opening for post-structuralism
  - instability of meaning
  - always 'deferred'

## Jacques Derrida
- born in Algeria, 1930
- 1960s: interrogating phenomenology
  - rebellious outsider
- main contacts not in philosophy, but in comparative literature

## The Deconstructive Difference

### The Left-Heideggerian Critique of Metaphysics
- Heidegger: destruction of ontology
  - Western thought captive to ideal of stable, complete meaning
  - Forms: source of all truths
- mobilized Nietzschean tasks: destruction of the history of ontology
- we don't understand prejudices we've taken on

### The Critique of Logocentrism: Speech and Writing
- destruction, applies it to metaphysics of language
- speech understood as moment of pure lucidity, all meaning is present and available to us
  - Socratic dialogue
- writing as derivative
- this haunts Saussure as well: phonocentrism
- more: Rousseauian nostalgia
- Heidegger: fetishize particular terms
- Derrida: they all have metaphysical bias: phonocentrism
  - speech as presence, writing as absence
  - this is unstable
- 'archi-writing': primordial writing before differentiation of speech/writing
- can we escape commitment to presence

### Differance and Deconstruction
- *'Differer'*: to differ (be distinguished from); to defer (to postpone); these turned into a gerund (temporal)
  - temporalized: structured temporality be deferral
- *'Differance'*: the process of differentiation, generates meaning but also makes it impossible for it to gain full presence, never arrives at metaphysical truths
  - endless process
  - establishes meaning but defers expectations of a final meaning
  - cannot retain self-grounding stability
  - deferral is the ground
- *Deconstruction*:
  - Heidegger: wrest ourselves free and get proper understanding of Being
  - Derrida: always caught up in traditional going wrong
    - no such overcoming of metaphysics, since its traces are built into all signification
	- metaphysics enables and disables meaning

## The 'Event' of Structure: Derrida reads Levi-Strauss

### The Politics of the Event
- 1966, Johns Hopkins University, "The Language of Criticism and the Sciences of Man" (participants: Roland Barthes, Jacques Lacan, etc.)

### The Argument: Metaphysical 'Trace' in Structuralist Anthropology, or De-Structuring Structure
- destabilizing without alternative: what's the payoff?
  - might be bewildering
  - charges of nihilism
- metaphysical traces in structural anthropology

- structuralism tells us that human structure is set in system of signs
- Derrida asks: do we have confidence that system is giving us truth?
  - system anchored to reality or nature
  - otherwise lattice work of signs, arbitrary
  - needs to be stabilized
  - where's the center?
- Levi-Strauss: nature was ground for culture
  - kinship: natural sign in system of signs
    - universal
    - incest taboo: ground of truth
- final meaning outside of itself
  - first it tells you you're born into system of signs
  - only thing that secures it is outside the structure
  - this is a complication
  - what about sign itself?
- Saussure: thought is presence of a sign
  - these kinds of self-defeating [] evident in history of philosophy as well
    - a succession of attempts to overcome metaphysics
	- impulsive but never truly successful
	- structural anthropology: another attempt
	- L-S: de-centering of Europe
- a certain problem in this humility
  - ethnography still a metaphysics of 'correct' description
  - higher authority than mythical culture
- bricoleur v. engineer
  - engineer holds on to longing presence for objectivity, theological idea, divine vantage on human affairs
  - getting outside system of signs
- dispense with engineer
  - could anthropology establish itself as empirical science?
- what grounds a culture?
  - leave aside nature
  - look at Saussure's preference for synchrony or diachrony
- structuralism can't explain origin of structure since we see meaning as born out of structure
  - can't see what preceded it
  - hand-wavy emergence of structuralism in one fell swoop
  - how this began is outside structure
- what is not named
  - 'Rousseauian' moment in structuralism
  - nostalgia that acts as ground
  - a ground that it, according to its own principles, cannot think about
- we are therefore caught *between* humanist nostalgia for presence/origins and the 'play' of signs 'beyond man and humanism'...

- not nihilist! not destruction
- anti-authoritarian
