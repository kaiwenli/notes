+++
title = "Jean-Paul Sartre and the Tasks of Existential Phenomenology"
date = 2019-02-26T21:43:38-05:00
series = "HIST 1324"
+++

## Introductory Remarks: Jean-Paul Sartre in Occupied Paris

### The Occupation and the Intellectuals
- complicity and resistance

### Sartre at *Aux deux magots* and *Cafe Flore*
- joins army, POW, released

## On Sartre's Existentialism: *Being and Nothingness* (1943)
- resonance of political connotation clear

### Negation and Nothingness
- "In Search of Being": Being not a concept, something far more concrete
- quarreling with neo-Kantianism
  - traditional bourgeois rationalist philosophy sees the task of philosophy as turning everything conceptual
- phenomenology: fasten our attention on existence as existence, concrete as concrete
- nothingness: human being = potential nothingness
  - do people submit to mere being or negate that being and become something different

### The Definition of the Human Being
- capacity of being for-itself
- transcend in-itself
- stark choice: submit or transcend
- the existential challenge of being human

### Anguish, Freedom, and the Absence of a Foundation
- we have capacity to negate passivity
- absence of foundation when "launched out"
  - creates foundation
- nothing like "human nature" or higher metaphysical entities
  - we are absurd and burdened with freedom

## The Phenomenology of the World

### The In-Itself and the For-Itself
- metaphysical drama about human discovery
- ironically, despite being against Cartesianism, Sartre reconfigures those ideas into nihilation/worldliness

### The Analysis of the Slimy
- nightmare to external existence
- "psychoanalysis of things"
- existence sticks to us and then inhibits us
- every attempt at self-realization: congeals into a new reality, which can then inhibit new possibilities
- capitulate? a choice
  - bad faith: choosing not to choose

### Critical and Historical Remarks
- "melting" paintings: Tanguy, Dali
- gender distinctions

## The Challenge of the Social

### Existentialism and the Threat of Solipsism
- Kierkegaard, Heidegger, Dostoyevsky, Nietzsche: solitary individual
- early Sartre: image of society profoundly negative
  - "Hell is other people"
- French Hegelian influence: conflict/dialectic of recognition
  - difficult to confront the other

### The Ontological Challenge of the Other
- not only striving to be myself, but imprint on all beings
  - space bend around self
- the other tries to do the same
  - represents a hemorrhage in my universe
- impossible situation: for-itself is to be completely sovereign, "be the monarch"
  - two kings and one has to die

## The Gaze of the Other

### Being-Seen-by-Another
- other has stolen idea of founding myself
- transcendence transcended

### Shame
- reduce to sheer body
- why keyhole?
  - view other without being faced with a similar reduction

### Being-Seen as an Ontological Structure
- Being-seen a predicament of being human
- possibility of being seen a threat to my possibility
  - the could be
- sense of being watched is the essence of religion
- Foucault reactivates this anxiety

## Ethics, Freedom, Politics

### Freedom and the Situation
- abjection and the challenge of responsibility

### A Call to Action
- develop political understanding of being free
- hope of a resistance and its fulfillment
- later Sartre: naivete
  - too absolute, metaphysical
  - do we always have a choice?
