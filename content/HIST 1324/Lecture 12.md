+++
title = "Alterity and Ethics: Emmanuel Levinas"
date = 2019-03-07T13:40:11-04:00
series = "HIST 1324"
+++

## Emergence of Alterity

### The Question of the Other
- has been there, present in Sartre
- what is it, exactly
  - God, intersubjectivity, gender, race, etc

### Alterity: A Theme and its Variations in Twentieth-Century Thought
- Kierkegaard renaissance: human self v. divine other
  - infinite qualitative distinction blur God and man
- Barth: God as "Absolutely Other"
- de Beauvoir, Fanon, Lacan
- Emmanuel Levinas
  - unresolved problem: classical phenomenology, existentialism, could not reflect on otherness

### The Problem of the Other in Classical Phenomenology (Husserl and Heidegger)

#### Husserl
- other does not appear as equal to myself
  - phenomenology encase within itself; radical solipsism
- only by analogy other is equal
  - other built up through analogues based upon own experience
  - can't ever know "you" (consciousness, etc), so this is how it works
- people found this explanation unsatisfactory

#### Heidegger
- get out of rationalist premises: everydayness and concrete
  - Husserl is too abstracted
- "mineness" as the point of departure
- begins by questioning the questioner
  - highly individualist
- meaningfulness given to us by social commitment
- Dasein: social being, "being with"
- when we are with others we are also lost
- all of us rely on anonymous field of "the One"
  - "Everyone is the other, and no one is himself"
  - Dasein loses itself in social field of the One
- conflictual philosophy: escaped Cartesianism but questions about shared social world
- Dasein modifies relation to the One: authenticity

## Emmanuel Levinas: Biography and Major Publications
- not the One or where hell is other people
- Otherness: origin of social world, ethics
- studied with Husserl and Heidegger
- respected him, but unsettled by Heidegger's political commitments
- more and more finds more holes in Heidegger
- other: pivot by which he can pull himself out of Heideggerian system

## Levinas on Ethics and Alterity

### The Rebellion Against Philosophical Holism
- common belief in philosophical tradition
- Plato: intelligibility through eternal, invariant forms
- Heidegger continues that motif: through Being that world is disclosed
  - still operates with basic insight that world is unified whole, disclosed as a whole through basic organizing principle
- anything other than the disclosed world, by its virtue, not appearing
- external: infinitely other
- other as other: how to get into contact?

### The Encounter with the Other
- religion as model for this
- escaping sameness
- relation to another not a relation of knowing, without totalizing comprehension
  - Heidegger's necessity of understanding collapses
- turn religion horizontal
  - "knowledge cannot take precedence over sociality"
  - other as irreducible to comprehension, like God in religion
  - challenge understanding before knowing

### Ethics as First Philosophy
- overturn epistemology
- must be in relation before reflecting on epistemological categories
- Plato: beyond Forms, idea of the Good: "Good beyond being"
- Descartes: despite solipsism, certitude of the infinite
- thus, ethics has its place in philosophical tradition

### Levinas and Judaism
- Judaism thematizes obligation to other

### Questions for Social Theory
- how to identify anything as other?
  - knowing other requires prior knowledge
  - knowledge would then again precede morality
- ethics vs politics
  - politics: assimilate unknown into sovereign known to all
  - ethics: in context of Cold War
- alterity: everyday jargon, difficulty knowing the other
  - multivalent, provocative
  - can romanticize otherness, but when we do that, is otherness a political consequence? who defines other? do we risk minimizing political/social characters when defining people as others?
