+++
title = "Durkheim and the Rise of the Symbolic"
date = 2019-02-07T16:54:10-05:00
series = "HIST 1324"
+++

## Durkheim in Later Years
- Durkheim had hegemonic role in education at turn of century

### Return to Philosophy
- "talas": those who flocked to Durkheim's secular sociology
- taught general philsophical topics
- pursuing philosophical questions in a new realm through sociology

### Turning to the Sociology of Religion
- religion as a tissue of belief: obligatory set of beliefs and conditions
  - pressure exercised by society on its members
  - sacredness - you're not in control; holds you captive
- social aspect of religion
  - religion derived from sense of social structure
  
## Durkheim's Sociology of Religion: *The Elementary Forms of Religious Life* (1912)

### Presuppositions
- through anthropology, derive insights about religion; "armchair anthropology"
- the simple: social order at its beginnings
- greater simplicity -> better discern universal, religious structures

### Religion
- sacred vs profane
- every society has something like that
  - what counts as sacred in society has a status that becomes the truth of that social order
- religion as what bonds use
  - representation of social bond
- reinforces social bonds
  - sacred rite of collective exuberance
- collective effervescence: ritually reconfirmed sacred bond

## The Social Character of Representation: Themes and Ambiguities

### The Social and the Symbolic
- individual of labor, thought symbolism would wither away
- eternalist vision of the symbolic order
- "man's double": you are an individual but insofar as part of society, you share social aspects, transcend yourself

### Socializing Epistemology
- sociological version of Kant's epistemology
- Durkheim: human intelligibility comes from not reason but from society itself
- science: disbelievers' form of religion

### Ambiguities in the meaning of representation
- all representation have the same basic expressive features, or
- is there something particular about totems
- religion a proto-scienctific explanation of social structure
- clan totemism: projection; primitive sociology

## Political Implications

### Irrational Collective
- prior: group behavior deeply and intrinsically irrational
- large groups in democracies could be moved by irrational symbols, quasi-religious sentiment
- Durkheim: collective as sacred, where human being transcends himself

### Reflexive Moments
- modern democracy: sacred character of the individual
  - somewhat paradoxical