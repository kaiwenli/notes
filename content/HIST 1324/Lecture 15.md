+++
title = "The Origins of French Structuralism"
date = 2019-03-26T16:36:19-04:00
series = "HIST 1324"
+++

## Introductory Remarks: From Subject to Structure

### The Varieties of Structure
- Music and Visual Arts: what art is?
  - expression and internal ideas manifested in sound, etc.
  - emerge from deeper self to surface
  - disillusionment of that idea
  - sound-fields, visual planes
- Western Marxism
  - voluntarism to structuralism
- Psychoanalysis
  - self as precipitant of language
- The Broader Path: mostly French

### The French Political and Cultural Context

#### The Postwar Rise of a Rationalized-Scientific and Meritocratic Civil Service Class
- represents Cartesian spirit in French politics
- displacement of subject and rise of structure
- retirement of Charles de Gaulle
- demise of a charismatic leader
- fading of memory of French resistance
  - Sartre's fading gave way to structuralism
  
#### A Shift in Culture and Economy: *Les trentes glorieuses* (1945-1975)
- miraculous economic growth
- Dosse: "the crisis of the militant intellectual"
  - Sartre's decline: rise of anonymous academics
- science of humanity

## The Linguistic Turn: Variations upon a Theme

### Early Philosophies of Language
- Rousseau: language as vehicle for emotion
- Humboldt: spiritual development; language developed over time

### Heidegger on Language

#### Early Heidegger: language as the basic interpretative framework of life
- world around us has linguistic structure

#### Later Heidegger: language as the "house of Being"
- thereness of the world appears in linguistic way
- poetic relation to the world is something to be cherished

- when we encounter language, language speaks, not us
- language anterior to self

### The Language of Scientific Report: the Vienna Circle of Logical Positivism
- Carnap: language as pre-determining regions of reality

### Ludwig Wittgenstein, 1889-1951
- *Tractus Logico-Philosophicus* (1922): language provides adequate mental image of the world
  - that lies outside language is the mythical
- *Philosophical Investigations* (1953): externalist
  - language's meanings established prior to private ideas
  - understand language as social practice

### Ordinary Language Analysis: the Oxford School, Ryle, Austin, Searle
- Austin: words have performative aspects
- Searle

## The Origins of French Structuralism: Language and Structure

### Ferdinand de Saussure, 1857-1913
- *Cours de Linguistique Generale* (1916): lecture notes compiled by students published posthumously
- after 1960, translations exploded

### Saussure's Linguistic Structuralism
- all linguistic phenomenon as *parole* (speech) and *language* (language-structure)
- if we look at *langue*, can be understood in two different ways
  - diachronic: through time
  - synchronic: time slice

### The Synchronic analysis of the Language-structure
- theory of signs: signifier (sound) and signified (idea)

#### Saussure's dissent from earlier models of language
- representationalism: language as "picture" of the external (extra-linguistic) reality
- expressivism: language as "expressions" (*Ausdrucke*) of internal states or ideas

#### For Saussure, these models are incorrect
- language is not a mere mirror for representation of an extra-linguistic reality
- the signified alone is not the internal birthplace of meaning

#### For Saussure, meaning in language obtains only because of the internal relation of language itself
- meaning: sign holds together
- arbitrary relationship between signifier and signified
- bond seems wholly accidental
- how does meaning get established?

#### The differential structure of all signs in a self-sustaining system
- language as a system of differences
- "in language, there are only differences, *without positive terms*"
- chess pieces
  - what identifies a pawn as a pawn?
  - each piece distinguishes from one another
  - pawn is pawn because of its difference from queen, bishop, etc.
  - no essential thing about "pawn or how it looks that establishes its role in chess
  - differences secure identity

### Broader Philosophical Ramifications of Structuralist Linguistics
- de-emphasize performative character
  - ignore history, intent, uses
  - analogy of symphony: language as a system has autonomy from the way we use it
  - parallels to Durkheim and *sui generis* in society
- language structure is impersonal
  - not fixed by individuals
  - reality of language pre-exists individual mind
- without reference to history
- language holds you captive yet there is arbitrary relation between signifier and signified
  - **internally arbitrary but holistically fixed**
- structures prior to any language user
- toward structuralism: linguistics as model for all sign-systems