+++
title = "Michel Foucault, or Reason and Unreason as a Symbolic System"
date = 2019-04-09T19:55:01-04:00
series = "HIST 1324"
+++

## Introductory Remarks: Varieties of Post-structuralism
- post-structuralism never really one thing

### Derrida's Deconstruction of the Metaphysics of Language
- differance: the dynamic of meaning: each privileged term relies upon the term it excludes (differs, defers), without coming to rest
- no system of meaning is stable

### Foucault's 'Nietzschean' Structuralism: History, Practice, and Power in Social Structure
- masks without a face: a lot of experimentation
- "left-Nietzschean"
- historicizing structuralism: we in historical past different regimes of knowledge that pre-determine how human life is understood
  - abrupt transformations in episteme but record contingencies
- discourses: power-knowledge formations, intervening in reality
  - knowledge as social practice
- interests in exclusion, discipline, surveillance
- genealogies of power
- beyond both structuralism and hermeneutics

## Michel Foucault, Life and Work

### Biographical Remarks
- born into wealthy Catholic family thick with medical practitioners
- philosophy and psychology, logic and unreason, analytic and empirical

### Five Major Streams of Influence

#### Friedrich Nietzsche and the Critique of Nihilism
- spirit of rebellion against bourgeois conformity
- skepticism of bourgeois self and society
- affirmation of unconst, unstructured freedom
  - life as a "work of art"

#### Martin Heidegger and French anti-humanism
- strong suspicion of humanist existentialism
- self needs to be decentered
  - mere precipitant of deeper structures of meaning
- self is contingent in history that can easily vanish
- Ludwig Biswanger (existential psychiatrist, student of Heidegger)

#### French sociology: neo-Kantianism, Durkheim, and post-Durkheimian sociology
- the social a priori, or, the social grounding of epistemology (Durkheim's *Elementary Forms*)
- historicizing the a priori, or, the historical variability of our regimes of knowledge and intelligibility
  - transformations of basic, underlying codes

#### French literary avant-gards: de Sade, Surrealism (Magritte) Maurice Blanchot, Georges Bataille
- limit, or liminal, experiences: the sacred, death, and sexuality

#### French historians and sociologists of science, especially Georges Canguilhem (1904-1955) historian of biology and medicine

## Structuralism and History: The Early Foucault
  
### The Spirit of Structuralism in the Mind of a Savage
- the history of "systems of thought": Foucault as structuralist
- the *episteme*: the systems that structure experience
- epistemic shifts: a history of structure and the contingency of any given structure (e.g., Thomas Kuhn, Ian Hacking)
- the contingency of modernity and its means of ordering human experience
- testing the boundaries: an ideal of freedom elsewhere than structure?
  - no "right" myth
  - once you historicize structure, no structure is the right one
    - new schema could appear
    
### The History of Madness (Paris: Plon, 1961; revised and abridged 1963, 1972)

#### Genesis of the Text
- studies in psychology
- historical sociology? deep structuring logic by which Western reason has gained its definition
  - reminder: meaning defined by difference
  - reason / unreason; only by differing what is mad
  - past systems of differentiation
  - how the mad were defined; defined only in relation to the sane
  - instability of own rationale
 
#### The Argument
- the late medieval and renaissance expulsion: madness as a holy affliction: analogous to leprosy, the "ship of fools"
  - Erasmus: mad person touched by God
  - fool in King Lear: most knowing person
  - wholly innocence of Jesus
- the 'Great Confinement' of the classical age
- a juridical and moral structure rather than a medical establishment, established across Europe in the 17th century
  - founding of Age of Reason, gained its self-definition by placing this in city-center
  - symbolized development of modern institutions
- Descartes
  - entertains madness as radical form of skepticism
  - that moment of radical skepticism "the great exorcism of madness"
- genesis of the modern police state as well as the origin of bourgeois morality (*police* and *politesse*)
  - law and etiquette
- Philippe Pinel (1745-1826) appointed chief physician at Salpetriere (1795): 18th-century therapeutics as colonization of self
  - target individual medical pathology
  - moral to medical/pathological
  - division of sane/insane into the self

### Methodological Lessons and Nietzschean Ideals
- power of imposing a social differentiation
- a description of the history of an exclusion without recapitulating reason's victory?
- anti-psychiatry
- Nietzsche, art, madness