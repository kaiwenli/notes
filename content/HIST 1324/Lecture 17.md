+++
title = "Levi-Strauss and Culture as a Symbolic System"
date = 2019-04-02T16:18:12-04:00
series = "HIST 1324"
+++

## Recap
- French enthusiasm for science
- structure of literature: New Criticism, block out history

## The Inner Logic of Savage Thought: *La Pensee Sauvage* (1962)

### The Significance of the Book and the French Context
- French-Algerian War
- French grandeur: heroic subjectivity challenged in 1962 after de Gaulle and this book
- play on words: the undomesticated flower
  - sauvage != savage
  - brute, undistorted, **wild**
  - thought set free
- anthropology best when exercised in estrangement
  - thus, shared trait with Durkheim
  - dissent: anything that we're doing are not any more differentiated than elsewhere
- universalism

### The Structuralist Analysis of Myth and Totemism
- 'vast system of correspondences': they work through difference
  - e.g. bear tribe: bear differs from eagle
  - no internal homologies: the homologies are in the differences
  - bear tribe different from eagle tribe the way bear != eagle

### A Science of the Concrete
- humans classify things for no external reason
  - against functionalism: too reductive, ignores playful aspect of human mind
  - essence of being human is not functional; for its own sake
- totemism: a certain intellectual payoff
- rules of exogamy: 'a theory of society in the form of a fable'
- myths are 'good for thinking', they explain the world
  - similar to Durkheim: myths are idealized; religion
  - difference is that Durkheim subscribed to theory of social development
    - progressive theory of history
    - all belong to totemic tribes (sports teams, political parties)
    - extolled modern civilization, advanced differences
- L-S: myths resist change
- history to structure, diachrony to synchrony

### Myth and Science: the Bricoleur (symbols) and the Engineer (concepts)
- signs are finite, culture takes on whatever is at-hand
  - economy to create meaning
  - bricolage: everyday handiwork
- bricoleur v. engineer
  - the former exemplifies genius of humanity: myth-making
  - the latter exemplifies distant, conceptualized reasoning, stands outside human world entirely
    - like a god
- myth keeps world intelligible and closed, reduces event to structure
- science is open (through structure what is event shows up as new)
  - represses what makes world unintelligible
- charges of asceticism and intellectualism
- subtle rules: rules as human beings
- not just *sauvage* but *moderne*

### Contra Sartre: The Critique of History
- anthropological dismantling of Sartre's achievement
- Sartre exemplified arrogance of Western reason
  - sovereign importance of history
  - he celebrates Western humanity: human self-creation
  - myth: history moving forward, added significance and subtlety, abandoning other civilizations

## Some Anticipatory Remarks: Structuralism and its Discontents
- distanced from Elementary Structures of Kinship
  - culture not grounded in anything beyond itself
  - lack of self-consciousness, unconsciousness of rules
- determinist, no intentional agency
  - L-S' theory of culture: no sovereign center
  - what causes structure in the first place?